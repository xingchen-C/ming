# Mitch文档转换
#### Description

**框架：springboot+uni+redis(5.0及以上)+nginx+mysql+websocket（转换进度展示）**

# 网址示例

#### 体验地址(仅供参考)：

http://www.mitchconvert.cn/

![image-20240330173500623](md-img/image-20240330173500623.png)

![image-20240330173745988](md-img/image-20240330173745988.png)

![image-20240330173759860](md-img/image-20240330173759860.png)




本项目做了简单的用户登录、注册和保存转换记录的逻辑；如果只想看转换文档的代码，直接看utils包下microsoft包里面的代码。

不管什么格式的文档，核心的文档转换代码只有一句：ConvertFileHandler.buildChainByNoSave(fileBo);其中fileBo是ConvertFileBo实体类的实例对象。

# 工程简介
一、本项目整体的文档转换流程介绍（伪代码）：  
    1、可上传的文档有四大类：words、pdf、excel、ppt（注意这里每一大类可上传的文档后缀可能有多种），
将这四大类构建成一个责任链的调用形式，调用的入口即为抽象类ConvertFileHandler，入参是ConvertFileBo
的实体类，里面包含转换的文档convertFile、要转换的类型chooseType、选择的四大类型之一convertType，
ConvertFileHandler有两个抽象方法：一个是转换完保存到指定的本地路径，另一个是转换为二进制流交给前端（
本项目是选择后一种，因为部署在安装手机上的缘故，手机内存不多，所以才选择这种方式）。第一个链是校验文件
格式、大小，通过校验后才交给所对应的链去处理。
    2、每一个链都会调不通的文档转换工具类，比如ExcelToChain链回去调ExcelToOther工具类进行转换，
以此类推..
    3、每个工具类里面都有四个静态的集合：
    1）allowedConvertType：允许转换的类型-->指的是chooseType
    2）isImageType：转换的类型是否是图片类型（原因：转换后文件数量非单一个（所以需要打成压缩包），转换方式可能跟其他文档不太一样）
    3）isZipType：转换后需要打成压缩包返回的 “允许转换的类型”，不同的工具类转完了以后不单单只有图片类型
可能会产生多个文件，所以用这个来标识。
    4）allowedSourceType：允许上传的文件的格式，比如说words支持上传doc、txt、md等等..
    3、转换步骤：加载配置，去除水印和页数限制；创建文档对象；创建临时保存的目录；设置保存选项（里面有转换进度回调，通过webSocket返回给前端）；
调用文档对象的保存方法执行转换；

二、注意事项：若部署在Linux下时，需要注意该环境下会缺失一些window的字体，导致转换时报错或乱码，
参考地址https://help.fanruan.com/finereport/edition-view-54074-0.html
或进行如下操作：
Unable to load native library: dlopen failed: library "libandroid-spawn.so" not found
（1）在/usr/share/fonts/下创建一个目录存放Windows字体
```
# mkdir /usr/share/fonts/winfonts/
```
（2）将字体上传到创建好的目录并解压
```
# cd /usr/share/fonts/winfonts/
# tar xf Windows_fonts.tar.gz
```
（3）建立字体索引信息，更新字体缓存
```
#安装sudo apt-get install ttf-mscorefonts-installer
mkfontscale
mkfontdir
#安装sudo apt-get install fontconfig
fc-cache -fv
fc-cache -rv 是强制刷新
```
（4）让字体生效
```
# source /etc/profile
```
（5）再次查看字体,如fc-list nofound 则执行sudo apt-get install fontconfig
```
# fc-list  |wc -l
```

# 延伸阅读
如果对您有帮助，希望您能给我一个小心心starts，感激不尽！
