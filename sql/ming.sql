/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80020 (8.0.20)
 Source Host           : localhost:3306
 Source Schema         : ming

 Target Server Type    : MySQL
 Target Server Version : 80020 (8.0.20)
 File Encoding         : 65001

 Date: 23/03/2024 17:27:11
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for convert_microsoft_record
-- ----------------------------
DROP TABLE IF EXISTS `convert_microsoft_record`;
CREATE TABLE `convert_microsoft_record`  (
  `convert_id` bigint NOT NULL AUTO_INCREMENT COMMENT '文档转换记录表主键',
  `user_source_type` int NULL DEFAULT NULL COMMENT '转换者的用户类型：0用户，1游客',
  `convert_uid` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '转换的用户id，登录时是user_id，未登录则是tourist_id',
  `upload_type` varchar(7) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '上传类型',
  `upload_size` varchar(7) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '上传大小',
  `upload_name` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '上传文件的前缀名字，不带.',
  `convert_type` varchar(7) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '转换类型',
  `create_time` datetime NULL DEFAULT NULL COMMENT '记录创建时间',
  `convert_time` datetime NULL DEFAULT NULL COMMENT '开始转换时间',
  `convert_url` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '转换后文件保存的地址url',
  `convert_second` double NULL DEFAULT NULL COMMENT '转换所花费的时间s（开始转换时间-结束转换时间）',
  `convert_status` int NULL DEFAULT NULL COMMENT '转换状态，0初始，1完成，2失败，3已过期',
  `fail_reason` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '转换失败原因',
  PRIMARY KEY (`convert_id`) USING BTREE,
  INDEX `FK_USER_ID`(`user_source_type` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 880 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '文档转换记录表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice`  (
  `notice_id` bigint NOT NULL COMMENT '公告主键',
  `notice_title` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '公告标题',
  `notice_content` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '公告内容',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `valid_end_time` datetime NULL DEFAULT NULL COMMENT '有效截至时间',
  `enabled` tinyint NULL DEFAULT NULL COMMENT '是否启用',
  PRIMARY KEY (`notice_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for sys_problem_feedback
-- ----------------------------
DROP TABLE IF EXISTS `sys_problem_feedback`;
CREATE TABLE `sys_problem_feedback`  (
  `problem_id` bigint NOT NULL AUTO_INCREMENT COMMENT '问题表主键',
  `problem_describe` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '问题描述',
  `anonymous` tinyint NOT NULL COMMENT '是否匿名',
  `user_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '提问者用户id/游客uid',
  `create_time` datetime NULL DEFAULT NULL COMMENT '提问时间',
  `is_read` tinyint NULL DEFAULT NULL COMMENT '是否已读',
  PRIMARY KEY (`problem_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '问题反馈表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for sys_problem_replay
-- ----------------------------
DROP TABLE IF EXISTS `sys_problem_replay`;
CREATE TABLE `sys_problem_replay`  (
  `problem_id` bigint NOT NULL COMMENT '问题表主键',
  `replayer_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '回复者用户id/游客uid',
  `create_time` datetime NULL DEFAULT NULL COMMENT '回复时间',
  `content` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '回复内容',
  `is_read` tinyint NULL DEFAULT NULL COMMENT '是否已读',
  INDEX `problem_id`(`problem_id` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '问题反馈回复表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for sys_tourist
-- ----------------------------
DROP TABLE IF EXISTS `sys_tourist`;
CREATE TABLE `sys_tourist`  (
  `tourist_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '游客uid',
  `tourist_ip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '游客ip',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `last_visit_time` datetime NULL DEFAULT NULL COMMENT '最后访问时间',
  PRIMARY KEY (`tourist_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `user_id` bigint NOT NULL AUTO_INCREMENT COMMENT '用户表主键',
  `user_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `user_sex` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户性别',
  `open_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '微信用户的openid唯一',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '邮箱唯一',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `last_login_time` datetime NULL DEFAULT NULL COMMENT '最后登录时间',
  `last_login_ip` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '最后登录的ip地址',
  `last_login_token` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '最后登录时的token',
  `pass_word` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户密码',
  `status` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户账户状态（0正常，1已禁用，2已注销）',
  `salt` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户密码加密盐',
  `convert_role` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '文档转换角色(n普通用户，v会员，s超级管理员)',
  `avatar` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '头像网络地址',
  `free_convert_num` int(10) UNSIGNED ZEROFILL NULL DEFAULT NULL COMMENT '剩余免费转换次数',
  `spend_convert_num` int(10) UNSIGNED ZEROFILL NULL DEFAULT NULL COMMENT '剩余免费转换次数',
  `login_ip` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '登录ip',
  PRIMARY KEY (`user_id`, `email`) USING BTREE,
  UNIQUE INDEX `UNIQUE_EMAIL`(`email` ASC) USING BTREE COMMENT '邮箱唯一',
  UNIQUE INDEX `PK_USER_ID`(`user_id` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 53 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户表' ROW_FORMAT = DYNAMIC;

SET FOREIGN_KEY_CHECKS = 1;
