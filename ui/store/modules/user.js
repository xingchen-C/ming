import {
	ACCESS_TOKEN,
	PERSON_INFO
} from '@/store/mutation-types'
import storage from '@/utils/storage'
import * as LoginApi from '@/api/login'
import * as UserApi from '@/api/user'
// 登陆成功后执行
const loginSuccess = (commit, {
	data
}) => {
	// 过期时间7天
	const expiryTime = 7 * 86400
	const token = data.lastLoginToken
	// 保存tokne和user信息到缓存
	// storage.set(ACCESS_TOKEN,token , expiryTime);
	storage.set(PERSON_INFO, data, expiryTime)
	// 记录到store全局变量
	commit('SET_TOKEN', token)
	commit('SET_USER', data)
}

export const state = {
	// 用户认证token
	token: '',
	// 用户信息
	personInfo: null
}

export const mutations = {
	SET_TOKEN: (state, value) => {
		state.token = value
	},
	SET_USER: (state, value) => {
		state.personInfo = value
	},
}
export const actions = {

	// 用户登录(普通登录: 输入账号、密码和验证码)
	pwdLogin({
		commit
	}, data) {
		return new Promise((resolve, reject) => {
			LoginApi.pwdLogin(data, {
				custom: {
					catch: true
				}
			}).then(response => {
				const result = response;
				loginSuccess(commit, result)
				resolve(response)
			}).catch(reject)
		})
	},

	// 用户邮箱验证码登录(注册性登录: 输入账号和邮箱验证码，如果账号不存在则自动注册)
	registerLogin({
		commit
	}, data) {
		return new Promise((resolve, reject) => {
			LoginApi.registerLogin(data, {
				custom: {
					catch: true
				}
			}).then(response => {
				const result = response;
				loginSuccess(commit, result)
				resolve(response)
			}).catch(reject)
		})
	},

	// 获取用户信息
	Info({
		commit
	}) {
		return new Promise((resolve, reject) => {
			if (state.personInfo) {
				return resolve(state.personInfo)
			}
			UserApi.getInfo().then(response => {
				if (response != null) {
					const result = response;
					commit('SET_USER', result.data)
					resolve(response)
				}
			}).catch(reject)
		})
	},
	// 刷新用户信息
	// FlushInfo({commit}){
	//  return new Promise((resolve, reject) => {
	//    UserApi.getInfo().then(response => {
	//      const result = response;
	//   loginSuccess(commit, result)
	//      resolve(response)
	//    }).catch(reject)
	//  })
	// },
	// 退出登录
	Logout({
		commit
	}, data) {
		return new Promise((resolve, reject) => {
			LoginApi.logout(data, {
				custom: {
					catch: true
				}
			}).then(response => {
				// storage.remove(ACCESS_TOKEN)
				storage.remove(PERSON_INFO)
				commit('SET_TOKEN', '')
				resolve(response)
			}).catch(reject)
		})
	}

}