import {CONVERTTYPE_LIST,GLOBAL_EXPIRY_TIME} from '@/store/mutation-types'
import storage from '@/utils/storage'
import {getConvertTypeVo} from '@/api/convert/convert.js'

export const  getConvertTypeList=(chooseType)=>{
	//获取可上传的文件类型
	let convertMap = storage.get(CONVERTTYPE_LIST).convertMap;
	if (!convertMap) {
		//缓存的转换类型过期了则重新登录
        getConvertTypeVo().then(result => {
            storage.set(Constants.CONVERTTYPE_LIST, result.data, Constants.GLOBAL_EXPIRY_TIME);
            storage.set(Constants.ALLOW_FILE_SIZE, result.message, Constants.GLOBAL_EXPIRY_TIME);
            convertMap=result.data.convertMap
        }).catch(err => {
            return uni.$u.toast(err.message)
        })
	}
    return convertMap[chooseType]
}