export const ACCESS_TOKEN = 'AccessToken'
// 过期时间7天
export const GLOBAL_EXPIRY_TIME = 7 * 86400
export const PERSON_INFO = 'PersonInfo'
export const PWD_MD5 = 'pd5'
export const CONVERTTYPE_LIST = 'ConvertTypeList'
export const PREFILENAME = "pre"
export const SUFFILENAME = "suf"
export const ALLFILENAME = "all"
//允许上传的文件的大小
export const ALLOW_FILE_SIZE = 'ConvertUploadSize'
//文件下载的前缀路径，nginx通过识别该路径进行获取对于路径的文件
export const FILE_DOWNLOAD_PRE = 'convert-file/'
//====================websocket消息类型=======================
export const CONN_UID="conn-uid";
//连接成功
export const CONN_SUC="conn-suc";
export const CONN_CHECK="conn-check";
export const CONN_BREAK="conn-break";
//转换进度
export const CONVERT_PROGRESS="convert-progress";
export const CONVERT_SUCCESS="convert-success";
export const CONVERT_ERROR="convert-error";
//用户取消转换
export const CONVERT_CHANNEL="convert-channel";
//群公告
export const GROUP_ANNOUNCEMENT="group-announcement";
//已读公告标志
export const READ_ANNOUNCEMENT="read-announcement";
//个人消息弹框提醒
export const PERSON_MSG="person-message";

//用户性别
export const SEX_MAN="1";
export const SEX_WOMAN="2";
//用户角色
export const ROLE_TOURIST="RT";
export const ROLE_ORDINARY="RO";
export const ROLE_VIP="RV";
export const ROLE_SUPER="RS";
//用户状态
export const USER_NORMAL="0";
export const USER_DISABLE="1";
export const USER_CANCELLATION="2";

//用户来源类型
export const SOURCE_USER=0;
export const SOURCE_TOURIST=1;
//转换状态
export const CONVERT_STATUS=['转换中','下载','失败','失效','失效','转换失败重试']
export const CONVERT_STATUS_ICON=['el-icon-loading','el-icon-download','el-icon-error','el-icon-document-delete','el-icon-document-delete','el-icon-warning']