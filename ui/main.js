import App from './App'
import Vue from 'vue'
import uView from 'uni_modules/uview-ui'
import './components/element-ui/element.js'
//引入自定义ali的icon
import './static/customicon/iconfont.css'
//引入并开启websocket挂载
import globalWs from '@/utils/websocket/websocket.js'
Vue.prototype.$globalWs = globalWs
//挂载到全局
// Vue.prototype.$socket = websocket
Vue.use(uView)

Vue.config.productionTip = false

App.mpType = 'app'

import store from '@/store';

const app = new Vue({
  store,
  ...App
})

// 引入请求封装，将app参数传递到配置中
require('@/config/http.interceptor.js')(app)

app.$mount()