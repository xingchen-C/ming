import request from '@/config/request.js';

// 用户登录(账号+密码+随机验证码)
export const pwdLogin = (params) => request.post(`/no-verification-service/login?uuid=${params.uuid}&code=${params.code}`, params, { custom: { auth: false } })
// 用户邮箱注册登录(账号+邮箱验证码)
export const registerLogin = (params) => request.post(`/no-verification-service/register`, params, { custom: { auth: false } })

// 用户退出
export const logout = () => request.get('/person/logout')

// 登录态校验
export const checkLogin = () => request.post('/no-verification-service/checkLogin',null,{ custom: { toast: false } })