import request from '@/config/request.js';

// 获取用户信息
export const getInfo = () => request.post('/person/getPersonInfo',null, { custom: { tourist: true } })
// 修改用户信息
export const alterUserInfo=(params)=> request.post('/person/alterUserInfo',params, { custom: { auth: true } })
//修改密码
export const alterPwd = (params) => request.post(`/person/alterPwd?uuid=${params.uuid}&code=${params.code}`, params, { custom: { auth: true } })
//创建游客
export const createNewTourist = () => request.post(`/no-verification-service/checkOrCreateTourist`,null,{ custom: { tourist: true } })
