import request from '@/config/request.js';

// 获取随机验证码
export const image = () => request.get('/no-verification-service/generate-code', null, { custom: { auth: false, loading: false } })
// 获取邮箱验证码
export const getEmailCode = (params) => request.get(`/no-verification-service/sendEmail-code?email=${params}`, null, { custom: { auth: true} })