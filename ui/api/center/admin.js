import request from '@/config/request.js';

// 获取统计信息
export const statistInfo = () => request.get('/admin/newUserStatist', null,{ custom: { auth: true } })
// 获取用户列表
export const getUserList = (params) => request.post('/admin/getUserList',params, { custom: { auth: true } })
// 管理用户
export const manageUser = (params) => request.post('/admin/manageUser',params, { custom: { auth: true } })
// 发送在线通知公告
export const sendOnlineAnnouncement = (params) => request.post('/admin/sendOnlineAnnouncement',params, { custom: { auth: true } })
//添加用户
export const addUser = (params) => request.post('/admin/addUser',params, { custom: { auth: true } })