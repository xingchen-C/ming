import request from '@/config/request.js';
// 查询问题反馈列表
export const getProblemList=(params)=> request.post('/basic-business/getProblemList',params, { custom: { tourist: true } })
// 提交问题反馈
export const problemFeedback=(params)=> request.post('/basic-business/problemFeedback',params, { custom: { tourist: true } })
// 获取是否有未读回复
export const hasReplay=()=> request.get('/basic-business/hasReplay',null, { custom: { tourist: true } })
// 获取回复列表
export const getProblemReplay=(problemId)=> request.post(`/basic-business/getProblemReplay/${problemId}`,null, { custom: { tourist: true } })
