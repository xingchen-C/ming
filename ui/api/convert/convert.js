import request from '@/config/request.js';

// 提交问题反馈
export const getConvertTypeVo=()=> request.get(`/no-verification-service/getConvertTypeVo`)
// 获取个人转换统计
export const personConversionStatistics=(params)=> request.get(`/microsoft/personConversionStatistics`,null, { custom: { tourist: true} })
export const getConvertRecordList=(params)=> request.post(`/microsoft/getConvertRecordList`,params, { custom: { tourist: true} })

