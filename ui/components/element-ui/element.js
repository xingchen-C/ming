import Vue from 'vue'
import 'element-ui/lib/theme-chalk/index.css'; /*样式文件*/
import {
    // Button,
	Upload,
	Notification,
	Progress,
	Alert,
	Badge,
	Card,
	Image,
	Button,
	Divider,
	Radio,
	Tag,
	Dialog,
	Table,
	TableColumn,
	Tooltip,
	Pagination,
	Select,
	Input,
	Option,
	Loading
}from 'element-ui'
Vue.use(Upload)
Vue.use(Progress)
Vue.use(Alert)
Vue.use(Badge)
Vue.use(Card)
Vue.use(Image)
Vue.use(Button)
Vue.use(Divider)
Vue.use(Radio)
Vue.use(Tag)
Vue.use(Dialog)
Vue.use(Table)
Vue.use(TableColumn)
Vue.use(Tooltip)
Vue.use(Pagination)
Vue.use(Select)
Vue.use(Input)
Vue.use(Option)
Vue.use(Loading)
//应用插件
Vue.prototype.$notify = Notification;