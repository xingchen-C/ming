const { environment } = require('@/config/environment.js')

/**
 * 回调参数
 * @param message 消息内容 JSON格式 {"from": "admin", "to": "admin", message: "message"}
 *
 */
// 初始化封装方法
export default {
  ws: null, //建立的连接
  lockReconnect: false,//是否真正建立连接
  timeoutObj: null,//心跳心跳倒计时
  serverTimeoutObj: null ,//心跳倒计时
  timeoutNum: null,//断开 重连倒计时
  connId:null,//连接ws的用户token
  url:null,
  //发送ws方法
  sendWs: function(data) {
    // console.log(data, '发送的消息')
    this.ws.send(JSON.stringify(data))
  },
  initWebSocket(connId) {
	const that = this;
    // 此处是当前登录信息
    if(connId){
    	console.log("initSocket start by params："+connId)
		// if(connid !=that.connId || !lockReconnect){
		// 	that.connUid=connId
		// 	// that.closeWs()
		// }
		that.connUid=connId
    }
    // let uri = environment.baseWs+connId
    that.url = environment.baseWs+that.connUid;
	console.log("that.url is "+that.url)
    const ws = new WebSocket(that.url)
    ws.onopen = () => {
      console.log('websocket 连接成功：'+connId)
      that.lockReconnect = true
      //开启连接心跳
      that.start()
    }
    ws.onmessage = function onMessage(res) {
      // console.log('接受服务端消息', res)
      // let msgData = res.data
      // if (typeof msgData != 'object') {
      //   const data = msgData.replace(/\ufeff/g, '')
      //   const message = JSON.parse(data)
      //   //服务端消息回调
      //   that.global_callback(message)
      // }
	  console.log('websocket 收到消息'+res.data)
	  uni.$emit('message', res.data)
    }
    ws.onerror = function onError(err) {
      console.log('websocket 连接失败：'+connId)
      that.closeWs();

      console.log('websocket 断开: ' +connId+"，err："+ err, ws.readyState)
      if (ws.readyState !== 0) {
        setTimeout(() => {
          that.initWebSocket(that.url)
        }, 1000)
      }
    }
    ws.onclose = function onClose() {
      console.log('websocket 连接关闭：'+connId)
    }
    this.ws = ws;
  },
  reconnect() {
    const that = this;
	console.log("ws 重连，connId："+that.connId+"，lockReconn："+that.lockReconnect)
    if (that.lockReconnect) {
      return;
    }
    that.lockReconnect = true;
    //没连接上会一直重连，设置延迟避免请求过多
    that.timeoutNum && clearTimeout(that.timeoutNum);
    that.timeoutNum = setTimeout(function () {
      that.initWebSocket(that.url);//新连接
      that.lockReconnect = false;
    }, 5000);
  },
  reset() {
    const that = this;
    //清除时间
    clearTimeout(that.timeoutObj);
    //清除时间
    clearTimeout(that.serverTimeoutObj);
    that.start(); //重启心跳
  },
  start() {
    const that = this;
    that.timeoutObj && clearTimeout(that.timeoutObj);
    that.serverTimeoutObj && clearTimeout(that.serverTimeoutObj);
    that.timeoutObj = setTimeout(function () {
      //这里发送一个心跳，后端收到后，返回一个心跳消息
      if (that.ws.readyState === 1) { //如果连接正常
		let data = { msgType: Constants.CONN_SUC, message: 'heartbeat' }
        that.sendWs(data);
      } else {  //否则重连
        that.reconnect()
      }
      //   serverTimeoutObj = setTimeout(function() {
      //     //超时关闭
      //     ws.close();
      //   }, timeout );
    }, 3000)
  },
  closeWs() {
    const that = this;
    if (that.lockReconnect) {
      that.ws.close()
      that.ws = null
	  that.connId
      that.lockReconnect = false
    }
  },
  global_callback(message) {
    // console.log("callback: "+message);
    const that = this;
    // 此处省略处理 业务逻辑
    if (message.to === "heartbeat") {
      that.reset();
    } else if (message.from === "流程执行结果") {//处理流程
      EventBus.$emit("aMsg", message.message);
    }
  }
}