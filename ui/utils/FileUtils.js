import { PREFILENAME,SUFFILENAME,ALLFILENAME } from '@/store/mutation-types'
//获取文件名称,1前缀不带.，2后缀不带.，3全称
export const getFileName=(file,type)=>{
	if(file==null){
		return uni.$u.toast('文件为空，无法获取文件名称')
	}
	let fileName = file.name
	let fileIndex = fileName.lastIndexOf(".");
	if(type===PREFILENAME){//前缀不带.
		fileName = fileName.substr(0,fileIndex)
	}else if(type===SUFFILENAME){//后缀不带.
		fileName = fileName.substr(fileIndex + 1)
	}
	return fileName;
}


// base64转blob
export const base64ToBlob = (base64Str,convertType) => {
	var bstr = atob([base64Str])
　　var n = bstr.length;
　　var u8arr = new Uint8Array(n);
　　while (n--) {
　　　　u8arr[n] = bstr.charCodeAt(n);
　　}
　　return new Blob([u8arr], { type: convertType });
}

// base64转文件下载
export const base64ToFile = (base64Str,fileName,convertType) => {
	let URL = base64ToBlob(base64Str,convertType);
	var name=fileName+'.'+convertType;
	var reader = new FileReader();
	　　reader.readAsDataURL(URL);
	　　reader.onload = function (e) {
	　　　　// 兼容IE
	　　　　if (window.navigator.msSaveOrOpenBlob) {
	　　　　　　var bstr = atob(e.target.result.split(",")[1]);
	　　　　　　var n = bstr.length;
	　　　　　　var u8arr = new Uint8Array(n);
	　　　　　　while (n--) {
	　　　　　　　　u8arr[n] = bstr.charCodeAt(n);
	　　　　　　}
	　　　　　　var blob = new Blob([u8arr]);
	　　　　　　window.navigator.msSaveOrOpenBlob(blob,name);
	　　　　} else {
	　　　　　　// 转换完成，创建一个a标签用于下载
	　　　　　　const a = document.createElement('a');
	　　　　　　a.download = name; // 这里写你的文件名
	　　　　　　a.href = e.target.result;
	　　　　　　document.body.appendChild(a)
	　　　　　　a.click();
	　　　　　　document.body.removeChild(a)
	　　}
	}
}