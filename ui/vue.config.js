const { environment } = require('./config/environment.js')

module.exports = {
  devServer: {
    port: 1799,
	https : false,
    proxy: {
      '/': {
        target: environment.baseURL,
        ws: true,
        changeOrigin: true,//是否支持跨域
        pathRewrite: {
          '^/': ''
        }
      }
    },
  }
}