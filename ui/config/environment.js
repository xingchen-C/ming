const environment = {
	// 开发环境配置
	development: {
		// 本地部署的后端
		baseURL: 'http://localhost:7777/',
		//如果是http请求用ws，https用wss
		baseWs: `ws://localhost:7777/websocket/?token=`,
		//配置nginx根据文件地址下载文件
		nginxDownload: 'http://localhost:1799/',
		staticImg: '/static/img/tabbar/'
		

	},
	// 生产环境配置
	production: {
		baseURL: 'http://www.mitchconvert.cn/ming',
		//如果是http请求用ws，https用wss
		baseWs: `ws://www.mitchconvert.cn/websocket/?token=`,
		//配置nginx根据文件地址下载文件
		nginxDownload: 'http://www.mitchconvert.cn/',
		// staticImg: 'http://www.mitchconvert.cn/defaultImg/'
		staticImg: '/static/img/tabbar/'
	}
}

module.exports = {
  environment: environment[process.env.NODE_ENV]
}