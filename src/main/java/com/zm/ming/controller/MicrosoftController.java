package com.zm.ming.controller;


import com.zm.ming.domain.bo.basicbusiness.ConvertRecordQuery;
import com.zm.ming.domain.bo.microsoft.ConvertFileBo;
import com.zm.ming.domain.constant.StatusConstants;
import com.zm.ming.domain.result.AjaxResult;
import com.zm.ming.domain.vo.login.LoginInfo;
import com.zm.ming.domain.yml.convert.ConvertYml;
import com.zm.ming.service.IConvertMicrosoftRecordService;
import com.zm.ming.service.ITouristService;
import com.zm.ming.service.MicrosoftService;
import com.zm.ming.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 办公文档互转 前端控制器
 * </p>
 * @author lzm
 */
@Slf4j
@RestController
@RequestMapping("/microsoft")
public class MicrosoftController extends BaseController{
    @Resource
    private IConvertMicrosoftRecordService convertRecordService;
    @Resource
    private MitchUtils mitchUtils;
    @Resource
    private MicrosoftService microsoftService;
    @Resource
    private ITouristService touristService;

    /**
    *文档转换
    * */
    @PostMapping("/convertFileEnqueue")
    public AjaxResult convertFileEnqueue(@Validated ConvertFileBo fileBo, HttpServletRequest request){
        //todo 校验转换权限
        //获取缓存信息并设置到转换记录中
        LoginInfo loginInfo = mitchUtils.getLoginInfo(request);
        return  microsoftService.convertFileEnqueue(fileBo,loginInfo);
    }
    /**
     *获取当前登录用户的文档转换统计数据
     * */
    @GetMapping("/personConversionStatistics")
    public AjaxResult personConversionStatistics(HttpServletRequest request){
        LoginInfo loginInfo = mitchUtils.getLoginInfo(request);
        return convertRecordService.getConversionStatisticsById(loginInfo.getRequestUid());
    }
    /**
     *获取当前用户/游客的文档转换记录
     * */
    @PostMapping("/getConvertRecordList")
    public AjaxResult getConvertRecordList(@RequestBody ConvertRecordQuery recordQuery, HttpServletRequest request){
        LoginInfo loginInfo = mitchUtils.getLoginInfo(request);
        if (!StatusConstants.ROLE_SUPER.equals(loginInfo.getConvertRole()) ){
            recordQuery.setConvertUid(loginInfo.getRequestUid());
        }
        if (recordQuery.getLimit()>20){
            recordQuery.setLimit(20);
        }
        return convertRecordService.getConvertRecordList(recordQuery);
    }
}
