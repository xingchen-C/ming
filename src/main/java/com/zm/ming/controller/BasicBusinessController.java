package com.zm.ming.controller;

import com.zm.ming.domain.bo.basicbusiness.ProblemQuery;
import com.zm.ming.domain.bo.basicbusiness.ProblemReplayQuery;
import com.zm.ming.domain.po.SysProblemFeedback;
import com.zm.ming.domain.po.SysProblemReplay;
import com.zm.ming.domain.result.AjaxResult;
import com.zm.ming.domain.vo.login.LoginInfo;
import com.zm.ming.service.BasicBusinessService;
import com.zm.ming.utils.MitchUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * @author Lzm
 * @description TODO基础业务功能，此处接口要注意：要兼容是否开启必须登录标识，不满足该条件的接口不要往这里写
 * @date 2022/12/5 20:14
 */
@RestController
@RequestMapping("/basic-business")
public class BasicBusinessController {

    @Resource
    private BasicBusinessService basicBusinessService;
    @Resource
    private MitchUtils mitchUtils;

    /**问题列表*/
    @PostMapping("getProblemList")
    public AjaxResult getProblemList(@RequestBody ProblemQuery problemQuery, HttpServletRequest request){
        return basicBusinessService.getProblemList(problemQuery,request);
    }
    /**根据问题id查询问题回复列表*/
    @PostMapping("getProblemReplayList")
    public AjaxResult getProblemReplayList(@RequestBody @Validated ProblemReplayQuery query , HttpServletRequest request){
        return basicBusinessService.getProblemReplayList(query,request);
    }
    /**问题反馈*/
    @PostMapping("problemFeedback")
    public AjaxResult problemFeedback(@RequestBody @Validated SysProblemFeedback problemFeedback, HttpServletRequest request){
        return basicBusinessService.problemFeedback(problemFeedback,request);
    }
    /**问题回复*/
    @PostMapping("problemReplay")
    public AjaxResult problemReplay(@RequestBody @Validated SysProblemReplay problemReplay, HttpServletRequest request){
        return basicBusinessService.problemReplay(problemReplay,request);
    }
    /**获取是否有未读回复的标志*/
    @GetMapping("hasReplay")
    public AjaxResult hasReplay(HttpServletRequest request){
        LoginInfo loginInfo = mitchUtils.getLoginInfo(request);
        return basicBusinessService.hasReplayByUserId(loginInfo.getRequestUid());
    }
    /**根据问题id获取回复列表*/
    @PostMapping("getProblemReplay/{problemId}")
    public AjaxResult getProblemReplay(@PathVariable("problemId") Long problemId, HttpServletRequest request){
        return basicBusinessService.getProblemReplay(problemId,request);
    }
    /**获取启用的公告*/
    @PostMapping("getNoticeList")
    public AjaxResult getNoticeList(){
        return basicBusinessService.getNoticeList();
    }

}
