package com.zm.ming.controller;


import com.zm.ming.domain.bo.user.AlterInfoBo;
import com.zm.ming.domain.bo.user.PassWordBo;
import com.zm.ming.domain.constant.RedisCacheConstant;
import com.zm.ming.domain.result.AjaxResult;
import com.zm.ming.domain.vo.login.LoginInfo;
import com.zm.ming.service.IUserService;
import com.zm.ming.utils.Assert;
import com.zm.ming.utils.MitchUtils;
import com.zm.ming.utils.RedisUtil;
import com.zm.ming.utils.SecurityUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author lzm
 */
@RestController
@RequestMapping("/person")
public class UserController extends BaseController{
    @Resource
    private RedisUtil redisUtil;
    @Resource
    private MitchUtils mitchUtils;
    @Resource
    private IUserService userService;
    /**修改个人信息*/
    @PostMapping("alterUserInfo")
    public AjaxResult alterUserInfo(@RequestBody @Validated AlterInfoBo alterInfoBo, HttpServletRequest request){
        return userService.alterUserInfo(alterInfoBo,request);
    }
    /**修改密码*/
    @PostMapping("alterPwd")
    public AjaxResult alterPwd(@RequestBody @Validated PassWordBo pwdBo, HttpServletRequest request){
        return userService.alterPwd(pwdBo,request);
    }
    /**登出*/
    @GetMapping("/logout")
    public AjaxResult logout(HttpServletRequest request){
        LoginInfo loginInfo = mitchUtils.getLoginInfo(request,RedisCacheConstant.USER_TOKEN);
        if (loginInfo !=null){
            redisUtil.del(mitchUtils.getTokenKey(request,RedisCacheConstant.USER_TOKEN));
        }
        return success();
    }

    /**获取缓存的用户信息*/
    @PostMapping("/getPersonInfo")
    public AjaxResult getPersonInfo(HttpServletRequest request){
        logger.info("person getPersonInfo start。。。");
        LoginInfo loginInfo = mitchUtils.getLoginInfo(request);
        logger.info("person getPersonInfo end，loginInfo："+loginInfo);
        loginInfo.setUserId(null);
        return  AjaxResult.success(loginInfo);
    }


}
