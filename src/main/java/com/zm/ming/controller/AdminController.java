package com.zm.ming.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zm.ming.domain.bo.basicbusiness.NoticeQuery;
import com.zm.ming.domain.bo.user.ManageBo;
import com.zm.ming.domain.bo.user.QueryListBo;
import com.zm.ming.domain.constant.Constants;
import com.zm.ming.domain.constant.RedisCacheConstant;
import com.zm.ming.domain.constant.StatusConstants;
import com.zm.ming.domain.po.SysNotice;
import com.zm.ming.domain.po.user.SysUser;
import com.zm.ming.domain.result.AjaxResult;
import com.zm.ming.domain.vo.login.LoginInfo;
import com.zm.ming.service.BasicBusinessService;
import com.zm.ming.service.IUserService;
import com.zm.ming.utils.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.socket.WebSocketHandler;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @author Lzm
 * @description TODO管理员请求控制器
 * @date 2022/12/10 16:27
 */
@RestController
@RequestMapping("/admin")
public class AdminController extends BaseController{
    @Resource
    private MitchUtils mitchUtils;
    @Resource
    private RedisUtil redisUtil;
    @Resource
    private IUserService userService;
    @Resource
    private BasicBusinessService businessService;

    /**用户管理-获取用户列表*/
    @PostMapping("getUserList")

    public AjaxResult getUserList(@RequestBody QueryListBo query){
        //开启分页
        PageHelper.startPage(query.getPage(),query.getLimit());
        //得到对应的分页对象
        PageInfo<LoginInfo> pageInfo;
        if (query.isOnline()){
            //说明查询的是redis在线的用户
            List<LoginInfo> byPrefixKey = mitchUtils.getByPrefixKey(RedisCacheConstant.USER_TOKEN, query, LoginInfo.class);
            pageInfo =new PageInfo<>(byPrefixKey);
        }else {
            //查数据库的用户
            pageInfo= new PageInfo<>(userService.getUserList(query));
        }
        return AjaxResult.success(pageInfo);
    }
    /**用户管理-管理用户,此方式只能修改用户状态和用户角色，不修改用户信息*/
    @PostMapping("manageUser")
    public AjaxResult manageUser(@RequestBody ManageBo bo){
        if (bo.isDoUnOnline()){
            Assert.isTrue(StringUtils.isBlank(bo.getUserToken()),"强制用户下线时，该用户的凭证不能为空");
            Assert.isTrue(redisUtil.hasKey(RedisCacheConstant.USER_TOKEN+bo.getUserToken()),"用户不在线，无法强退");
            redisUtil.del(RedisCacheConstant.USER_TOKEN+bo.getUserToken());
        }
        SysUser dbUser = userService.getById(bo.getUserId());
        Assert.notNull(dbUser,"用户不存在");
        Assert.isTrue(!StatusConstants.ROLE_SUPER.equals(dbUser.getConvertRole()),"无法修改管理员的个人信息");
        dbUser.setStatus(bo.getStatus());
        dbUser.setConvertRole(bo.getConvertRole());
        userService.updateById(dbUser);
        return success();
    }
    /**用户管理-管理员方式添加用户*/
    @PostMapping("addUser")
    public AjaxResult addUser(@RequestBody LoginInfo newUser){
        Assert.isTrue(StringUtils.isNotBlank(newUser.getEmail())&&InfoUtils.isEmail(newUser.getEmail()),"邮箱格式不正确");
        SysUser email = userService.getOne(new QueryWrapper<SysUser>().eq("email", newUser.getEmail()));
        Assert.isTrue(email==null,"用户已存在");
        SysUser sysUser = new SysUser();
        BeanUtils.copyProperties(newUser,sysUser);
        sysUser.setSalt(SecurityUtils.getSalt(Constants.SALT_PWD));
        sysUser.setCreateTime(new Date());
        sysUser.setUserId(null);
        userService.save(sysUser);
        return success();
    }
    /**用户管理-访问量统计*/
    @GetMapping("newUserStatist")
    public AjaxResult newUserStatist(){
        return userService.newUserStatist();
    }

    /**公告管理-获取公告列表*/
    @PostMapping("getManageNoticeList")
    public AjaxResult getManageNoticeList(@RequestBody NoticeQuery query){
        return businessService.getManageNoticeList(query);
    }
    /**公告管理-添加公告*/
    @PostMapping("addNotice")
    public AjaxResult addNotice(@RequestBody @Validated SysNotice notice){
         return toAjax(businessService.addNotice(notice));
    }
    /**公告管理-更新公告*/
    @PostMapping("updateNotice")
    public AjaxResult updateNotice(@RequestBody @Validated SysNotice notice){
        Assert.notNull(notice.getNoticeId(),"未知公告");
        return toAjax(businessService.updateNotice(notice));
    }
}
