package com.zm.ming.controller.nologin;

import com.zm.ming.controller.BaseController;
import com.zm.ming.domain.constant.RedisCacheConstant;
import com.zm.ming.domain.result.AjaxResult;
import com.zm.ming.utils.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.common.exception.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author Lzm
 * @description TODO本想部署在微信H5，后因限制就没做了，保留了这个类
 * @date 2022/11/20 13:32
 */
@Slf4j
@RestController
@RequestMapping("/no-verification-wechat")
public class NoVerificationWeChatController extends BaseController {
    @Resource
    private WxMpService wxMpService;
    @Resource
    private RedisUtil redisUtil;
    /**
     * 获取调用微信api的access_token,加密返回
     * */
    @PostMapping("/forgotPwd")
    public AjaxResult getAccessToken(){
        String accessToken=null;
        try {
            if (redisUtil.hasKey(RedisCacheConstant.WX_ACCESS_TOKEN)) {
                accessToken=(String) redisUtil.get(RedisCacheConstant.WX_ACCESS_TOKEN);
            }else {
                accessToken= wxMpService.getAccessToken();
                //todo 加密access_token

                redisUtil.set(RedisCacheConstant.WX_ACCESS_TOKEN,accessToken,RedisCacheConstant.WX_ACCESS_TOKEN_EXPIRES_IN);
            }
        } catch (WxErrorException e) {
            log.error("获取微信access_token失败："+e.getMessage());
            return error("获取微信信息失败");
        }
        return successData(accessToken);
    }
}
