package com.zm.ming.controller.nologin;

import cn.hutool.core.date.DateUtil;
import com.zm.ming.controller.BaseController;
import com.zm.ming.domain.bo.login.LoginBo;
import com.zm.ming.domain.bo.user.ForgotPwdBo;
import com.zm.ming.domain.bo.user.RegisterBo;
import com.zm.ming.domain.constant.Constants;
import com.zm.ming.domain.constant.MicrosoftConstants;
import com.zm.ming.domain.constant.RedisCacheConstant;
import com.zm.ming.domain.constant.StatusConstants;
import com.zm.ming.domain.po.user.SysTourist;
import com.zm.ming.domain.result.AjaxResult;
import com.zm.ming.domain.vo.login.LoginInfo;
import com.zm.ming.domain.vo.microsoft.ConvertTypeVo;
import com.zm.ming.domain.vo.microsoft.OneStatistics;
import com.zm.ming.domain.yml.convert.ConvertYml;
import com.zm.ming.service.CurrencyService;
import com.zm.ming.service.IConvertMicrosoftRecordService;
import com.zm.ming.service.ITouristService;
import com.zm.ming.service.IUserService;
import com.zm.ming.utils.*;
import com.zm.ming.utils.UUID;
import com.zm.ming.utils.microsoft.ExcelToOther;
import com.zm.ming.utils.microsoft.PdfToOther;
import com.zm.ming.utils.microsoft.PptxToOther;
import com.zm.ming.utils.microsoft.WordsToOther;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * @author ZM
 * @date 2022/11/5 16:12
 */
@RestController
@RequestMapping("/no-verification-service")
public class NoVerificationServiceController extends BaseController {
    @Resource
    private RedisUtil redisUtil;
    @Resource
    private CurrencyService currencyService;
    @Resource
    private EmailUtil emailUtil;
    @Resource
    private IUserService userService;
    @Resource
    private MitchUtils mitchUtils;
    @Resource
    private ConvertYml convertYml;
    @Resource
    private IConvertMicrosoftRecordService recordService;
    @Resource
    private ITouristService touristService;
    /**
     * 用户登录
     * */
    @PostMapping("/login")
    public AjaxResult login(@RequestBody @Validated LoginBo loginBo,HttpServletRequest request){
        return userService.login(loginBo,request);
    }
    /**获取缓存的用户信息*/
    @PostMapping("/checkLogin")
    public AjaxResult checkLogin(HttpServletRequest request){
        LoginInfo loginInfo = mitchUtils.getLoginInfo(request);
        if (loginInfo==null){
            return error("user is no login");
        }
        return  AjaxResult.success(loginInfo);
    }
    /**
     * 忘记密码
     * */
    @PostMapping("/forgotPwd")
    public AjaxResult forgotPwd(@RequestBody @Validated ForgotPwdBo forgotPwdBo,HttpServletRequest request){
        return userService.forgotPwd(forgotPwdBo,request);
    }

    /**
     * 用户注册
     * */
    @PostMapping("/register")
    public AjaxResult register(@RequestBody @Validated RegisterBo registerBo,HttpServletRequest request){
        return userService.register(registerBo,request);
    }

    /**生成随机码*/
    @GetMapping("/generate-code")
    public AjaxResult generateRandomCode(){
        return  currencyService.generateRandomCode();
    }

    /**发送邮箱验证码*/
    @GetMapping("/sendEmail-code")
    public AjaxResult sendEmailCode(String email,HttpServletRequest request){
        String ipAddress = SecurityUtils.getIpAddress(request);
        String sendLockKey=RedisCacheConstant.SEND_EMAIL_MAX_LOCK+email;
        Assert.isTrue(!redisUtil.hasKey(sendLockKey),"抱歉，当日发送已达上限");
        Assert.isTrue(InfoUtils.isEmail(email),"请输入正确的邮箱");
        //先看缓存有没有
        if (!redisUtil.hasKey(RedisCacheConstant.EMAIL_CODE+email)) {
            //生成一个四位真随机数
            String random =String.valueOf(UUID.randomNum(4));
            //构造发送对象信息
            SimpleMailMessage emailMessage = new SimpleMailMessage();
            emailMessage.setTo(email);
            emailMessage.setSubject(EmailUtil.CODE_SUBJECT);
            emailMessage.setText(String.format(EmailUtil.CODE_MSG+DateUtil.now(),random));
            emailUtil.sendSimpleEmail(emailMessage);
            redisUtil.set(RedisCacheConstant.EMAIL_CODE+email,random,5*Constants.MINUTE);
        }else {
            return error("已发送，请勿重复操作");
        }
        String sendValidTrial=RedisCacheConstant.SEND_EMAIL_VALID_TRIAL+ipAddress;
        redisUtil.decrByCreate(sendValidTrial,RedisCacheConstant.SEND_EMAIL_MAX,12*Constants.HOURS);
        if((int)redisUtil.get(sendValidTrial)<1){
            redisUtil.set(sendLockKey,0,12*Constants.HOURS);
        }
        return  success("已发送");
    }

    /**根据选择的文档类型获取可转换的类型*/
    @GetMapping("/getConvertTypeVo")
    public AjaxResult getConvertTypeVo(HttpServletRequest request){
        //根据角色获取可上传文件的最大大小
        LoginInfo loginInfo = mitchUtils.getLoginInfo(request);
        String uploadFileSizeMsg=null;
        String convertRole=null;
        if (loginInfo!=null){
            if (StatusConstants.ROLE_SUPER.equals(loginInfo.getConvertRole())){
                convertRole=StatusConstants.ROLE_SUPER;
            }else if (!StatusConstants.ROLE_ORDINARY.equals(loginInfo.getConvertRole()) || loginInfo.getSpendConvertNum()>0){
                convertRole=StatusConstants.ROLE_VIP;
            }
        }else {
            convertRole=StatusConstants.ROLE_ORDINARY;
        }
        uploadFileSizeMsg =String.valueOf( convertYml.getRole_size().get(convertRole));

        ConvertTypeVo convertTypeVo = recordService.getConvertTypeVo();

        return  AjaxResult.success(uploadFileSizeMsg,convertTypeVo);
    }
    /**
     * 创建游客，返回游客id
     * */
    @PostMapping("/checkOrCreateTourist")
    public AjaxResult checkOrCreateTourist(HttpServletRequest request){
        Assert.isTrue(!convertYml.getMust_login(),"当前业务未开启游客模式，请登录/注册使用，感谢理解~");
        String touristIp = SecurityUtils.getIpAddress(request);
        String touristId = SecurityUtils.getTouristToken(request);
        logger.info("checkOrCreateTourist get touristId："+touristId);
        LoginInfo loginInfo = touristService.checkOrCreateTourist(touristId, touristIp);
        return success(loginInfo.getUserSourceType()+"_"+loginInfo.getRequestUid());
    }
}
