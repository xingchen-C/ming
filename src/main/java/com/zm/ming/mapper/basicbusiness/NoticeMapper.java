package com.zm.ming.mapper.basicbusiness;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zm.ming.domain.bo.basicbusiness.NoticeQuery;
import com.zm.ming.domain.po.SysNotice;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author Lzm
 * @description TODO
 * @date 2022/11/24 21:06
 */
@Mapper
public interface NoticeMapper extends BaseMapper<SysNotice> {

    List<SysNotice> getNoticeList(NoticeQuery query);
}
