package com.zm.ming.mapper.basicbusiness;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zm.ming.domain.po.SysProblemReplay;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author Lzm
 * @description TODO
 * @date 2022/11/24 21:06
 */
@Mapper
public interface ProblemReplayMapper extends BaseMapper<SysProblemReplay> {

}
