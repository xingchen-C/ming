package com.zm.ming.mapper.basicbusiness;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zm.ming.domain.bo.basicbusiness.ProblemQuery;
import com.zm.ming.domain.po.SysProblemFeedback;
import com.zm.ming.domain.po.SysProblemReplay;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author Lzm
 * @description TODO
 * @date 2022/11/24 21:06
 */
@Mapper
public interface ProblemFeedbackMapper extends BaseMapper<SysProblemFeedback> {
    List<SysProblemFeedback> queryProblemFeedback(ProblemQuery problemQuery);

    List<SysProblemReplay> getProblemReplayByPids(@Param("pIds") List<Long> problemIds);

    int hasReplayByUserId(@Param("userId") String userId);

    int updateIsReadByPidAndUid(@Param("problemId") Long problemId,@Param("userId") String userId);
}
