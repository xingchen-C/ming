package com.zm.ming.mapper.microsoft;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zm.ming.domain.bo.basicbusiness.ConvertRecordQuery;
import com.zm.ming.domain.po.ConvertMicrosoftRecord;
import com.zm.ming.domain.vo.echart.WordCloud;
import com.zm.ming.domain.vo.microsoft.OneStatistics;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author Lzm
 * @description TODO
 * @date 2022/11/14 11:22
 */
@Mapper
public interface ConvertMicrosoftRecordMapper extends BaseMapper<ConvertMicrosoftRecord> {
    /**
     * 获取转换记录列表
     * */
    List<ConvertMicrosoftRecord> queryConvertRecordList(ConvertRecordQuery recordQuery);

    /**
     * 获取统计数据
    * */
    OneStatistics getConversionStatisticsByUid(@Param("convertUid") String convertUid);

    Long insertReturnId(ConvertMicrosoftRecord record);

    List<WordCloud> countUploadType();

    int updateExpiredConvertStatus(@Param("recordIds") List<Long> recordIds);
    int updateExpiredFailConvertStatus(@Param("recordIds") List<Long> recordIds);
}
