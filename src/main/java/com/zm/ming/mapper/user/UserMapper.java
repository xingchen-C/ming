package com.zm.ming.mapper.user;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zm.ming.domain.bo.user.QueryListBo;
import com.zm.ming.domain.bo.user.QueryUserBo;
import com.zm.ming.domain.po.user.SysUser;
import com.zm.ming.domain.vo.basicbusiness.NewUserStatist;
import com.zm.ming.domain.vo.login.LoginInfo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;


/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author lzm
 */
@Mapper
public interface UserMapper extends BaseMapper<SysUser> {
    SysUser queryUserById(QueryUserBo queryUserBo);

    List<LoginInfo> getUserList(QueryListBo queryListBo);

    NewUserStatist getUserStatistics();
}
