package com.zm.ming.mapper.user;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zm.ming.domain.bo.basicbusiness.NoticeQuery;
import com.zm.ming.domain.po.SysNotice;
import com.zm.ming.domain.po.user.SysTourist;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author Lzm
 * @description TODO
 * @date 2022/11/24 21:06
 */
@Mapper
public interface TouristMapper extends BaseMapper<SysTourist> {
    int countTouristByIpOneDay(@Param("touristIp") String touristIp,@Param("hitDay") Integer hitDay);
}
