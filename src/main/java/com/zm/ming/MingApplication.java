package com.zm.ming;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author lzm
 * 项目启动入口
 * 过滤器使用@WebFilter注解就需要使用ServletComponentScan让其生效
 * ...
 */
@SpringBootApplication
@ServletComponentScan(basePackages = "com.zm.ming.verify.filter")
@MapperScan(basePackages = "com.zm.ming.mapper.*")//mapper包扫描
@EnableScheduling//开启定时任务
public class MingApplication {
    public static void main(String[] args) {
        SpringApplication.run(MingApplication.class, args);
    }
}
