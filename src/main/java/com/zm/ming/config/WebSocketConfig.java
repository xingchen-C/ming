package com.zm.ming.config;

import com.zm.ming.handler.websocket.DefaultWebSocketHandler;
import com.zm.ming.handler.websocket.WebSocketImpl;
import com.zm.ming.handler.websocket.WebSocketInterface;
import com.zm.ming.verify.interceptor.WebSocketInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;
import org.springframework.web.socket.server.standard.ServerEndpointExporter;

import javax.annotation.Resource;
import java.net.http.WebSocket;

/**
 * @author Lzm
 * @description TODO
 * @date 2022/12/7 22:25
 */
@Configuration
@EnableWebSocket
public class WebSocketConfig implements WebSocketConfigurer {
    @Resource
    private DefaultWebSocketHandler defaultWebSocketHandler;
//    @Bean
//    public ServerEndpointExporter serverEndpointExporter() {
//        return new ServerEndpointExporter();
//    }
//    @Bean
//    public DefaultWebSocketHandler webSocketHandler() {
//        return new DefaultWebSocketHandler();
//    }
    @Bean
    public WebSocketInterceptor webSocketInterceptor(){
        return new  WebSocketInterceptor();
    }
    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        //websocket连接需要token权限
        registry.addHandler(defaultWebSocketHandler,"websocket/")//”websocket/“是ws连接的前缀路径
                .addInterceptors(webSocketInterceptor())
                .setAllowedOrigins("*");
    }
}