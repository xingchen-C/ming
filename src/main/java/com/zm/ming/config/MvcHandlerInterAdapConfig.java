package com.zm.ming.config;
import com.zm.ming.domain.yml.convert.ConvertYml;
import com.zm.ming.verify.interceptor.AdminInterceptor;
import com.zm.ming.verify.interceptor.CodeValidationInterceptor;
import com.zm.ming.verify.interceptor.NoLoginInterceptor;
import com.zm.ming.verify.interceptor.TouristModeInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.Resource;

/**
 * @author lzm
 */
@Configuration
public class MvcHandlerInterAdapConfig implements WebMvcConfigurer {
    /*配置拦截器配置类的相关配置：
        继承WebMvcConfigurer里面各常用方法介绍
        解决跨域问题 public void addCorsMappings(CorsRegistry registry) ;
        添加拦截器 void addInterceptors(InterceptorRegistry registry);
        这里配置视图解析器 void configureViewResolvers(ViewResolverRegistry registry);
        配置内容裁决的一些选项 void configureContentNegotiation(ContentNegotiationConfigurer configurer);
        视图跳转控制器 void addViewControllers(ViewControllerRegistry registry);
        静态资源处理 void addResourceHandlers(ResourceHandlerRegistry registry);
        默认静态资源处理器 void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer);
    */
    @Resource
    private ConvertYml convertYml;
    /**将方法的返回值交给ioc维护（实例化）*/
    @Bean
    public NoLoginInterceptor noLoginInterceptor(){
        return new  NoLoginInterceptor();
    }
    @Bean
    public TouristModeInterceptor touristModeInterceptor(){
        return new  TouristModeInterceptor();
    }
    @Bean
    public CodeValidationInterceptor codeValidationInterceptor(){
        return new  CodeValidationInterceptor();
    }
    @Bean
    public AdminInterceptor adminInterceptor(){
        return new  AdminInterceptor();
    }
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 拦截所有路径   多个拦截器组成一个拦截器链
        // 注册自定义两个拦截器
        // addPathPatterns  用来添加拦截规则，/** 表示拦截所有请求  /*/表示拦截目录
        // excludePatterns  排除掉不被拦截的请求路径
        //拦截器的拦截顺序，是按照Web配置文件中注入拦截器的顺序执行的

        //需要登录的地址/**即所有都要登录，排除excludePathPatterns配置的地址
        InterceptorRegistration mustLogin = registry.addInterceptor(noLoginInterceptor())
                .addPathPatterns(InterceptorUrl.ROOT_URL);
        //排除favicon.ico网页图标，在nginx中设置返回
        mustLogin.excludePathPatterns(InterceptorUrl.NO_PERMIT_URL);
        //todo 注意！转换若配置免登录，则排除下列接口路径，请注意新加接口的布局情况
        if (!convertYml.getMust_login()){
            //noLoginInterceptor排除TOURIST_URL路径
            mustLogin.excludePathPatterns(InterceptorUrl.TOURIST_URL);

            //免登录情况下，对游客路径接口增加游客拦截器
            InterceptorRegistration touristRegistry = registry.addInterceptor(touristModeInterceptor());
            touristRegistry.addPathPatterns(InterceptorUrl.TOURIST_URL);
        }
        //登录、注册需要验证码
        registry.addInterceptor(codeValidationInterceptor())
                .addPathPatterns(InterceptorUrl.CODE_URL);
        //管理页面需要管理员权限
        registry.addInterceptor(adminInterceptor())
                .addPathPatterns(InterceptorUrl.ADMIN_URL);
    }
}
