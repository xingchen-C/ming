package com.zm.ming.config;

/**
 * @author Lzm
 * @description TODO 拦截器地址集合
 * 用户类型等级可以访问自身以及向下的权限，例如用户可以访问游客的接口，但是游客不能访问用户的接口
 * 管理员>用户>游客>其它
 * admin>user>tourist>other
 * @date 2024/3/17 14:26
 */
public interface InterceptorUrl {
    String ROOT_URL="/**";
    String [] ADMIN_URL=new String[]{
            "/admin/**"
    };
    String [] CODE_URL=new String[]{
            "/no-verification-service/login",
            "/person/alterPwd"
    };
    //开启游客模式，游客模式的接口，所使用到的用户id均应该使用requiredId
    String [] TOURIST_URL=new String[]{
            "/microsoft/**",
            "/basic-business/**",
            "/person/getPersonInfo"
    };
    //免任何权限的接口
    String [] NO_PERMIT_URL=new String[]{
            "/no-verification-service/*",
            "/images/**",
            "/favicon.ico"
    };
}
