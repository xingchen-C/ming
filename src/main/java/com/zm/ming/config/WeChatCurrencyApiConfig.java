package com.zm.ming.config;

import me.chanjar.weixin.mp.api.WxMpConfigStorage;
import me.chanjar.weixin.mp.api.WxMpInMemoryConfigStorage;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.api.impl.WxMpServiceImpl;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

/**
 * @author Lzm
 * @description TODO
 * @date 2022/11/20 14:13
 */
@Configuration
//@PropertySource(
//        value={"classpath:wxProperties.properties"},
//        ignoreResourceNotFound = true)
//@DependsOn("propertyPlaceholderConfigurer")
public class WeChatCurrencyApiConfig {
    //直接获取资源文件中的配置的值
    @Value("${we-chat.app-id}")
    private String appId;
    @Value("${we-chat.app-secret}")
    private String appSecret;

    @Value("${we-chat.token}")
    private String token;//Token

    @Value("${we-chat.aes-key}")
    private String aesKey;//有就填，没有就不填

    @Value("${we-chat.merchant-no}")
    private String merchantNo;//商户号

    @Value("${we-chat.merchant-secret}")
    private String merchantSecret;//商户秘钥

    @Value("${we-chat.notify-url}")
    private String notifyUrl;//支付后台通知接口地址

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
        PropertySourcesPlaceholderConfigurer ppc = new PropertySourcesPlaceholderConfigurer();
        ppc.setIgnoreUnresolvablePlaceholders(true);
        return ppc;
    }

    @Bean
    public WxMpConfigStorage wxMpConfigStorage() {
        WxMpInMemoryConfigStorage configStorage = new WxMpInMemoryConfigStorage();
        configStorage.setAppId(this.appId);
        configStorage.setSecret(this.appSecret);
        configStorage.setToken(this.token);
        configStorage.setAesKey(this.aesKey);
        configStorage.setPartnerId(this.merchantNo);
        configStorage.setPartnerKey(this.merchantSecret);
        configStorage.setNotifyURL(this.notifyUrl);
        return configStorage;
    }

    @Bean
    public WxMpService wxMpService() {
        WxMpService wxMpService = new WxMpServiceImpl();
        wxMpService.setWxMpConfigStorage(wxMpConfigStorage());
        return wxMpService;
    }

}
