package com.zm.ming.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zm.ming.domain.bo.basicbusiness.ConvertRecordQuery;
import com.zm.ming.domain.bo.microsoft.ConvertFileBo;
import com.zm.ming.domain.po.ConvertMicrosoftRecord;
import com.zm.ming.domain.result.AjaxResult;
import com.zm.ming.domain.vo.echart.WordCloud;
import com.zm.ming.domain.vo.login.LoginInfo;
import com.zm.ming.domain.vo.microsoft.ConvertTypeVo;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Lzm
 * @description TODO
 * @date 2022/11/14 11:19
 */
public interface IConvertMicrosoftRecordService extends IService<ConvertMicrosoftRecord> {

    /**
     * 统计指定用户的转换数据
     * */
    AjaxResult getConversionStatisticsById(String convertUid);

    /**
     * 对缓存的词云可上传数自增1（用于用户上传文件进行转换时使用的方法）
     * todo 若业务缓存操作比较多，不适合放这里，应该新建一个专门操作业务缓存的类型
     * */
    boolean incrementWordCloud(String uploadType);
    /**
     * 统计可上传的文档类型记录数，返回词云对象列表
     * */
    List<WordCloud> countDbUploadType();
    /**
     * 获取所有可转换的类型
     * */
    Set<String> getAllUploadType();


    /**
     * 统计转换返回对象
     * */
    ConvertTypeVo getConvertTypeVo();

    AjaxResult getConvertRecordList(ConvertRecordQuery recordQuery);
    /**
     * 删除过期的转换记录文档，并标记过期状态，返回删除失败的数据
     * */
    void  delExpiredConvertRecord();
}
