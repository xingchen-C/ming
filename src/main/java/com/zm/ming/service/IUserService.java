package com.zm.ming.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.zm.ming.domain.bo.login.LoginBo;
import com.zm.ming.domain.bo.user.*;
import com.zm.ming.domain.po.user.SysUser;
import com.zm.ming.domain.result.AjaxResult;
import com.zm.ming.domain.vo.login.LoginInfo;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author lzm

 */
public interface IUserService extends IService<SysUser> {

    /**
     * 注册用户
     * @param registerBo 用户信息注册入参
     * @return 结果
     * */
    AjaxResult register(RegisterBo registerBo, HttpServletRequest request);

    AjaxResult login(LoginBo loginBo,HttpServletRequest request);

    AjaxResult alterUserInfo(AlterInfoBo alterInfoBo, HttpServletRequest request);

    AjaxResult forgotPwd(ForgotPwdBo forgotPwdBo,HttpServletRequest request);

    AjaxResult alterPwd(PassWordBo pwdBo, HttpServletRequest request);

    List<LoginInfo> getUserList(QueryListBo queryListBo);

    AjaxResult newUserStatist();
}
