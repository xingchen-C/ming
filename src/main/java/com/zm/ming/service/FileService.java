package com.zm.ming.service;

import org.springframework.web.multipart.MultipartFile;

/**
 * @author Lzm
 * @description TODO
 * @date 2024/2/28 22:02
 */
public interface FileService {
    public String uploadFile(MultipartFile multipartFile);
}
