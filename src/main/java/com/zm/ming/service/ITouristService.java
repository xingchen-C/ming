package com.zm.ming.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.zm.ming.domain.bo.login.LoginBo;
import com.zm.ming.domain.bo.user.*;
import com.zm.ming.domain.po.user.SysTourist;
import com.zm.ming.domain.po.user.SysUser;
import com.zm.ming.domain.result.AjaxResult;
import com.zm.ming.domain.vo.login.LoginInfo;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author lzm

 */
public interface ITouristService{

    LoginInfo checkOrCreateTourist(String touristId,String touristIp);

    LoginInfo checkTourist(String touristId,String touristIp);
}
