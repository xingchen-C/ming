package com.zm.ming.service;

import com.zm.ming.domain.bo.basicbusiness.NoticeQuery;
import com.zm.ming.domain.bo.basicbusiness.ProblemQuery;
import com.zm.ming.domain.bo.basicbusiness.ProblemReplayQuery;
import com.zm.ming.domain.bo.microsoft.ConvertFileBo;
import com.zm.ming.domain.po.SysNotice;
import com.zm.ming.domain.po.SysProblemFeedback;
import com.zm.ming.domain.po.SysProblemReplay;
import com.zm.ming.domain.result.AjaxResult;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Lzm
 * @description TODO
 * @date 2022/12/5 20:15
 */
public interface BasicBusinessService {
    AjaxResult problemFeedback(SysProblemFeedback problemFeedback, HttpServletRequest request);

    AjaxResult getProblemList(ProblemQuery problemQuery, HttpServletRequest request);

    AjaxResult problemReplay(SysProblemReplay problemReplay, HttpServletRequest request);

    AjaxResult getProblemReplayList(ProblemReplayQuery query, HttpServletRequest request);

    AjaxResult hasReplayByUserId(String userId);

    AjaxResult getProblemReplay(Long problemId, HttpServletRequest request);

    AjaxResult getNoticeList();

    int addNotice(SysNotice notice);

    int updateNotice(SysNotice notice);

    AjaxResult getManageNoticeList(NoticeQuery query);
}
