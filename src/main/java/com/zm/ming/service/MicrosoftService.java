package com.zm.ming.service;

import com.zm.ming.domain.bo.microsoft.ConvertFileBo;
import com.zm.ming.domain.result.AjaxResult;
import com.zm.ming.domain.vo.login.LoginInfo;

/**
 * @author Lzm
 * @description TODO
 * @date 2024/2/23 21:01
 */
public interface MicrosoftService {
    /**
     * 文件转换入队
     * */
    AjaxResult convertFileEnqueue(ConvertFileBo fileBo, LoginInfo loginInfo);

    boolean convertFileCustomer(ConvertFileBo convertFileBo);
}
