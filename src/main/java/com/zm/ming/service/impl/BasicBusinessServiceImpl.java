package com.zm.ming.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zm.ming.domain.bo.basicbusiness.NoticeQuery;
import com.zm.ming.domain.bo.basicbusiness.ProblemQuery;
import com.zm.ming.domain.bo.basicbusiness.ProblemReplayQuery;
import com.zm.ming.domain.constant.Constants;
import com.zm.ming.domain.constant.RedisCacheConstant;
import com.zm.ming.domain.constant.StatusConstants;
import com.zm.ming.domain.po.SysNotice;
import com.zm.ming.domain.po.SysProblemFeedback;
import com.zm.ming.domain.po.SysProblemReplay;
import com.zm.ming.domain.result.AjaxResult;
import com.zm.ming.domain.vo.login.LoginInfo;
import com.zm.ming.mapper.basicbusiness.NoticeMapper;
import com.zm.ming.mapper.basicbusiness.ProblemFeedbackMapper;
import com.zm.ming.mapper.basicbusiness.ProblemReplayMapper;
import com.zm.ming.service.BasicBusinessService;
import com.zm.ming.service.IConvertMicrosoftRecordService;
import com.zm.ming.utils.*;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * @author Lzm
 * @description TODO
 * @date 2022/12/5 20:16
 */
@Service
public class BasicBusinessServiceImpl  implements BasicBusinessService {
    @Resource
    private ProblemFeedbackMapper problemFeedbackMapper;
    @Resource
    private ProblemReplayMapper problemReplayMapper;
    @Resource
    private RedisUtil redisUtil;
    @Resource
    private MitchUtils mitchUtils;
    @Resource
    private IConvertMicrosoftRecordService convertRecordService;
    @Resource
    private NoticeMapper noticeMapper;

    @Override
    public AjaxResult getProblemList(ProblemQuery query, HttpServletRequest request) {
        LoginInfo loginInfo = mitchUtils.getLoginInfo(request);
        if (!StatusConstants.ROLE_SUPER.equals(loginInfo.getConvertRole())) {
            //非管理员只能查自己
            query.setUserId(loginInfo.getRequestUid());
        }
        //开启分页
        PageHelper.startPage(query.getPage(),query.getLimit());
        //得到对应的分页对象
        PageInfo<SysProblemFeedback> pageInfo=new PageInfo<>(problemFeedbackMapper.queryProblemFeedback(query));
        return AjaxResult.success(pageInfo);
    }
    @Override
    public AjaxResult getProblemReplayList(ProblemReplayQuery query, HttpServletRequest request) {
        LoginInfo loginInfo = mitchUtils.getLoginInfo(request);
        SysProblemFeedback problem = problemFeedbackMapper.selectById(query.getProblemId());
        Assert.notNull(problem,"找不到这个问题~");
        //非管理员、提问者不能获取
        if ( !StatusConstants.ROLE_SUPER.equals(loginInfo.getConvertRole())||
                !Objects.equals(problem.getUserId(), loginInfo.getUserId())){
            return AjaxResult.error("不能查看与本人无关的回复信息");
        }
        //开启分页
        PageHelper.startPage(query.getPage(),query.getLimit());
        //得到对应的分页对象
        PageInfo<SysProblemReplay> pageInfo=new PageInfo<>(problemReplayMapper.selectList(new QueryWrapper<SysProblemReplay>()
                .eq("problem_id", query.getProblemId())));
        return AjaxResult.success(pageInfo);
    }

    @Override
    public AjaxResult hasReplayByUserId(String userId) {
        int newReplay=problemFeedbackMapper.hasReplayByUserId(userId);
        return newReplay>0?AjaxResult.success(true):AjaxResult.success(false);
    }

    @Override
    public AjaxResult getProblemReplay(Long problemId, HttpServletRequest request) {
        LoginInfo loginInfo = mitchUtils.getLoginInfo(request);
        int newReplay = problemFeedbackMapper.hasReplayByUserId(loginInfo.getRequestUid());
        if (newReplay>0){
            //更新为已读
            problemFeedbackMapper.updateIsReadByPidAndUid(problemId, loginInfo.getRequestUid());
        }
        List<SysProblemReplay> replayList = problemReplayMapper.selectList(
                new QueryWrapper<SysProblemReplay>().eq("problem_id", problemId));
        return AjaxResult.success(replayList);
    }

    @Override
    public AjaxResult getNoticeList() {
        return AjaxResult.success(noticeMapper.selectList(new QueryWrapper<SysNotice>().eq("enabled", true)));
    }

    @Override
    public int addNotice(SysNotice notice) {
        notice.setCreateTime(new Date());
        return noticeMapper.insert(notice);
    }

    @Override
    public int updateNotice(SysNotice notice) {
        return noticeMapper.updateById(notice);
    }

    @Override
    public AjaxResult getManageNoticeList(NoticeQuery query) {
        //开启分页
        PageHelper.startPage(query.getPage(),query.getLimit());
        //得到对应的分页对象
        PageInfo<SysNotice> pageInfo=new PageInfo<>(noticeMapper.getNoticeList(query));
        return AjaxResult.success(pageInfo);
    }

    @Override
    public AjaxResult problemFeedback(SysProblemFeedback problemFeedback,HttpServletRequest request) {
        LoginInfo loginInfo = mitchUtils.getLoginInfo(request);
        Assert.isTrue(!redisUtil.hasKey(RedisCacheConstant.ISN_COMMIT_PROBLEM_KEY+ loginInfo.getRequestUid()),"感谢您真诚的反馈，请隔一段时间再来提交~");
        //判断是否有选择匿名，如果没有，则加上反馈者的用户id
        if (!problemFeedback.isAnonymous()){
            problemFeedback.setUserId(loginInfo.getRequestUid());
        }
        problemFeedback.setCreateTime(new Date());
        //未读
        problemFeedback.setRead(false);
        problemFeedbackMapper.insert(problemFeedback);
        redisUtil.set(RedisCacheConstant.ISN_COMMIT_PROBLEM_KEY+ loginInfo.getRequestUid(),0,6* Constants.HOURS);
        return AjaxResult.success("非常感谢您的反馈~作者会继续努力的！");
    }

    @Override
    public AjaxResult problemReplay(SysProblemReplay problemReplay, HttpServletRequest request) {
        Assert.notNull(problemReplay.getProblemId(),"请问要回复那个问题呢？");
        SysProblemFeedback problemFeedback = problemFeedbackMapper.selectById(problemReplay.getProblemId());
        Assert.notNull(problemFeedback,"无法回复未知的提问");
        LoginInfo infoVo = mitchUtils.getLoginInfo(request);
        if (!StatusConstants.ROLE_SUPER.equals(infoVo.getConvertRole())){
            //非超级管理员只能回复自己提问的问题
            Assert.isTrue(Objects.equals(problemFeedback.getUserId(), infoVo.getRequestUid()),"无法回复不是本人提交的问题");
        }
        problemReplay.setRead(false);
        problemReplay.setReplayerId(infoVo.getRequestUid());
        problemReplay.setCreateTime(new Date());
        problemReplayMapper.insert(problemReplay);
        return AjaxResult.success();
    }

}
