package com.zm.ming.service.impl;

import cn.hutool.core.io.FileUtil;
import com.zm.ming.domain.bo.microsoft.ConvertFileBo;
import com.zm.ming.domain.bo.microsoft.ConvertResult;
import com.zm.ming.domain.constant.Constants;
import com.zm.ming.domain.constant.StatusConstants;
import com.zm.ming.domain.po.ConvertMicrosoftRecord;
import com.zm.ming.domain.result.AjaxResult;
import com.zm.ming.domain.vo.login.LoginInfo;
import com.zm.ming.domain.yml.convert.ConvertYml;
import com.zm.ming.handler.ConvertFileHandler;
import com.zm.ming.handler.websocket.WebSocketImpl;
import com.zm.ming.handler.websocket.WebSocketInterface;
import com.zm.ming.mapper.microsoft.ConvertMicrosoftRecordMapper;
import com.zm.ming.mapper.user.UserMapper;
//import com.zm.ming.mq.ConvertSender;
import com.zm.ming.mq.redisMq.RedisConvertSender;
import com.zm.ming.mq.redisMq.RedisConvertSender;
import com.zm.ming.service.IConvertMicrosoftRecordService;
import com.zm.ming.service.MicrosoftService;
import com.zm.ming.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import javax.annotation.Resource;
import java.io.File;
import java.util.Date;

/**
 * @author Lzm
 * @description TODO
 * @date 2024/2/23 21:01
 */
@Service
@Slf4j
public class MicrosoftServiceImpl implements MicrosoftService {
    @Resource
    private ConvertMicrosoftRecordMapper  convertRecordMapper;
    @Resource
    private IConvertMicrosoftRecordService  microsoftRecordService;
    @Resource
    private UserMapper userMapper;
    @Resource
    private RedisUtil redisUtil;
    @Resource
    private PlatformTransactionManager transactionManager;
//    @Resource
//    private ConvertSender convertSender;
    @Resource
    private RedisConvertSender convertSender;
    @Resource
    private ConvertYml convertYml;
    @Resource
    private WebSocketImpl webSocket;

    @Override
    public AjaxResult convertFileEnqueue(ConvertFileBo fileBo, LoginInfo loginInfo) {

        String convertRole = loginInfo.getConvertRole();
        //todo 校验转换权限
        if (convertYml.getMust_spend()){
            if (StatusConstants.ROLE_ORDINARY.equals(convertRole)||
                    StatusConstants.ROLE_TOURIST.equals(convertRole)){
                //普通用户和游客校验剩余次数
                boolean hasConvertNum=loginInfo.getFreeConvertNum() + loginInfo.getSpendConvertNum()>0;
                Assert.isTrue(hasConvertNum,"抱歉，当前转换次数已达限制！请充值使用");
            }
        }
        Long roleMaxSize = convertYml.getRole_size().get(convertRole);
        Assert.isTrue(fileBo.getConvertFile().getSize() <=FileUtils.m2bL(roleMaxSize),"上传的文件不得超过："+FileUtils.b2mS(roleMaxSize));
        //todo 除了文件校验，为了安全还需要对转换责任链中所涉及到的校验在此处提前进行校验

        log.info("convertFileEnqueue convert file start by requestUid："+loginInfo.getRequestUid()+"，fileBo："+fileBo);
        //先新增转换记录
        ConvertMicrosoftRecord record=new ConvertMicrosoftRecord();
        //生成转换id
//        record.setConvertId(UUID.fastUUID().toString(true));
        record.setConvertUid(loginInfo.getRequestUid());
        record.setUserSourceType(loginInfo.getUserSourceType());
        record.setUploadName(FileUtils.getFilePrefix(fileBo.getConvertFile()));
        record.setUploadSize(FileUtils.getFileSize(fileBo.getConvertFile()));
        record.setConvertType(fileBo.getConvertType());
        record.setUploadType(FileUtils.getFileType(fileBo.getConvertFile()));
        record.setCreateTime(new Date());
        record.setConvertStatus(StatusConstants.CONVERT_INIT);
        //新增，插入后会往record设置新增记录的recordId
        convertRecordMapper.insertReturnId(record);

        /** 入队前设置参数 **/
        fileBo.setConvertUid(loginInfo.getRequestUid());
        fileBo.setUserId(loginInfo.getUserId());
        fileBo.setConvertId(record.getConvertId());
        fileBo.setUseType(Constants.USE_CONVERT_P2P);
        //获取配置文件中设置保存在本地的系统前缀路径
        fileBo.setPreSavePath(convertYml.getSave_path());
        //fileBo保存在内存之中，此处convertFile以先转为临时文件路径方式保存（时间换空间）
        String tempFileName = FileUtils.multipartFileToFile(fileBo.getConvertFile(),convertYml.getTemp_path());
        fileBo.setTempLocalFileName(tempFileName);
        fileBo.setConvertFile(null);
        fileBo.setFailRetryNum(convertYml.getFail_retry_num());
        //生产者转换消息入队
        //rabbitmq方式
//        convertSender.acceptConvertRequest(fileBo);
        //redisMq方式
        convertSender.sendMessage(fileBo);
        //自增缓存上传类型数
        microsoftRecordService.incrementWordCloud(record.getUploadType());
        log.info("convertFileEnqueue convert file end by requestUid："+loginInfo.getRequestUid()+"，fileBo："+fileBo);
        return AjaxResult.success(record);
    }

    @Override
    public boolean convertFileCustomer(ConvertFileBo convertFileBo) {
        boolean result=false;
        ConvertMicrosoftRecord dbRecord = convertRecordMapper.selectById(convertFileBo.getConvertId());
        if (dbRecord == null){
            log.error(convertFileBo.getConvertId()+" convertFileCustomer consume convert fail by no db record，"+convertFileBo);
            return true;
        }
        Date startTime = new Date();
        dbRecord.setConvertTime(startTime);
        dbRecord.setConvertStatus(StatusConstants.CONVERT_OK);
        String tempFilePath=convertYml.getTemp_path()+convertFileBo.getTempLocalFileName();
        try {
            //开始转换
            log.info(String.format("convert_record_id start【%d】-> %s to %s",convertFileBo.getConvertId(),
                    dbRecord.getUploadType(),dbRecord.getConvertType()));
            convertFileBo.setConvertFile(FileUtils.fileToMultipartFile(tempFilePath));
            ConvertResult cr = ConvertFileHandler.buildConvertFileChain(convertFileBo);
            dbRecord.setConvertUrl(cr.getPathResult());
            dbRecord.setConvertSecond(cr.getConsumingSecond());
            result=true;
        }catch (Exception e){
            log.error(String.format("convert_record_id【%d】 convert exception！",convertFileBo.getConvertId()),e);
            String errMsg=e.getMessage()==null?"":e.getMessage();
            errMsg=errMsg.length()>128? errMsg.substring(0,127):errMsg;
            //失败重试次数-1
            convertFileBo.setFailRetryNum(convertFileBo.getFailRetryNum()-1);
            if (convertFileBo.getFailRetryNum()>0){
                //非第一次重试失败不更新数据库状态
                if (convertYml.getFail_retry_num()-1 != convertFileBo.getFailRetryNum()){
                    errMsg="转换失败，正在重试中...";
                    WebSocketUtil.sendConvertErr(webSocket,errMsg,convertFileBo.getConvertUid());
                    return false;
                }else{
                    //说明第一次失败重试，需要更新数据库状态
                    dbRecord.setConvertStatus(StatusConstants.CONVERT_RETRY);
                    dbRecord.setFailReason(errMsg);
                }
            }else {
                //说明重试次数不足了，设置状态为失败
                dbRecord.setConvertStatus(StatusConstants.CONVERT_FAIL);
                dbRecord.setFailReason(errMsg);
                WebSocketUtil.sendConvertErr(webSocket,errMsg,convertFileBo.getConvertUid());
                FileUtil.del(tempFilePath);
            }
        }finally {
            log.info(String.format("convert_record_id【%d】，temp path【%s】",convertFileBo.getConvertId(),tempFilePath));
            if (!result && convertFileBo.getFailRetryNum()>0){
                convertFileBo.setConvertFile(null);
                log.warn("convertFileCustomer finally retry enqueue，convert_recordId："+convertFileBo.getConvertId()+" retry num："+convertFileBo.getFailRetryNum());
                //失败且有重试次数则重新入队
                log.info("convertFileCustomer finally convertFileBo："+convertFileBo);
                convertSender.sendMessage(convertFileBo);
            }
        }
        int i = convertRecordMapper.updateById(dbRecord);
        return result && i>0;
    }
}
