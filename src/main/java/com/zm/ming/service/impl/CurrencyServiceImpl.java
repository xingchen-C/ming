package com.zm.ming.service.impl;

import com.zm.ming.domain.constant.Constants;
import com.zm.ming.domain.constant.RedisCacheConstant;
import com.zm.ming.domain.result.AjaxResult;
import com.zm.ming.domain.vo.currency.VerificationCodeVo;
import com.zm.ming.service.CurrencyService;
import com.zm.ming.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.IOException;

/**
 * @author ZM
 * @date 2022/11/8 14:28
 * 通用的服务实现类
 */
@Slf4j
@Service
public class CurrencyServiceImpl implements CurrencyService {
    @Resource
    private RedisUtil redisUtil;

    @Override
    public AjaxResult generateRandomCode() {
        //生成随机码
        String code = SecurityUtils.getSalt(Constants.SALT_RANDOM_CODE);
        String graphic=null;
        try {
             graphic = PictureUtils.createGraphicByChar(code);
        } catch (IOException e) {
            log.info("根据随机码创建图形验证码失败："+e.getMessage());
            return AjaxResult.error("创建图形验证码失败");
        }
        //生成随机码的key
        String uuid = UUID.fastUUID().toString(true);
        //将随机码code生成图形随机码，并转为base64
        redisUtil.set(RedisCacheConstant.GRAPHIC_CODE + uuid,code,5* Constants.MINUTE);
        VerificationCodeVo codeVo=new VerificationCodeVo();
        codeVo.setUuid(uuid);
        codeVo.setGraphic(graphic);
        return AjaxResult.success(codeVo);
    }
}
