package com.zm.ming.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zm.ming.domain.bo.login.LoginBo;
import com.zm.ming.domain.bo.user.*;
import com.zm.ming.domain.constant.Constants;
import com.zm.ming.domain.constant.RedisCacheConstant;
import com.zm.ming.domain.constant.StatusConstants;
import com.zm.ming.domain.po.user.SysUser;
import com.zm.ming.domain.result.AjaxResult;
import com.zm.ming.domain.result.HttpConstant;
import com.zm.ming.domain.vo.basicbusiness.NewUserStatist;
import com.zm.ming.domain.vo.login.LoginInfo;
import com.zm.ming.handler.websocket.WebSocketImpl;
import com.zm.ming.mapper.microsoft.ConvertMicrosoftRecordMapper;
import com.zm.ming.mapper.user.UserMapper;


import com.zm.ming.service.IUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zm.ming.utils.*;
import com.zm.ming.utils.UUID;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author lzm
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, SysUser> implements IUserService {
    @Resource
    private RedisUtil redisUtil;
    @Resource
    private MitchUtils mitchUtils;
    @Resource
    private UserMapper userMapper;
    @Resource
    private ConvertMicrosoftRecordMapper convertMicrosoftRecordMapper;
    @Resource
    private WebSocketImpl webSocket;
    @Value("${mitch.convert.free_num}")
    private Integer freeConvertNum;
    @Value("${mitch.sys.headImg.path}")
    private String  preHeadImgPath;

    @Override
    public AjaxResult register(RegisterBo registerBo,HttpServletRequest request) {
        //参数校验
        mitchUtils.checkEmailCode(registerBo.getEmail(),registerBo.getEmailCode());
        String ipAddress = SecurityUtils.getIpAddress(request);
        //查询用户
        SysUser dbUser = userMapper.selectOne(new QueryWrapper<SysUser>().eq("email", registerBo.getEmail()));
        SysUser user=null;
        String msg;
        if (dbUser==null){
            //说明是新用户，则进行新增用户
            user= new SysUser();
            user.setEmail(registerBo.getEmail());
            user.setCreateTime(new Date());
            user.setStatus(StatusConstants.USER_NORMAL);
            user.setConvertRole(StatusConstants.ROLE_ORDINARY);
            //设置初始密码盐
            user.setSalt(SecurityUtils.getSalt(Constants.SALT_PWD));
            user.setFreeConvertNum(freeConvertNum);
            user.setSpendConvertNum(0);
            msg="欢迎加入我们~";
        }else {
            checkUserStatus(dbUser);
            //说明不是新用户则进行登录
            user=dbUser;
            msg="欢迎回来~";
        }
        //设置用户最后的登录信息
        user.setLastLoginTime(new Date());
        user.setLastLoginIp(ipAddress);
        user.setLastLoginToken(UUID.fastUUID().toString(true));
        //不存在即保存，存在即更新
        boolean b = saveOrUpdate(user);
        Assert.isTrue(b,"更新用户信息繁忙");
        //注册成功后自动登录，并缓存用户信息
        return AjaxResult.success(msg,mitchUtils.setLoginInfo(user));
    }
    @Override
    public AjaxResult login(LoginBo loginBo,HttpServletRequest request) {
        //登录校验
        Assert.isTrue(!redisUtil.hasKey(RedisCacheConstant.PWD_ERR_LOCK+loginBo.getLoginId()),
                "密码错误次数已达上限，限制登录 "+RedisCacheConstant.PWD_ERR_LOCK_TIME+"分钟");
        String ipAddress = SecurityUtils.getIpAddress(request);
        //查询数据库
        SysUser dbUser = getDbUserByEmail(loginBo.getLoginId());
        Assert.isTrue(StringUtils.isNotBlank(dbUser.getPassWord()),"您暂未设置密码，请使用获取邮箱验证码的方式进行登录，可在登录后进密码的设置");
        String loginPwd = SecurityUtils.encryptPwdByMd5AndSalt(loginBo.getPassWord(), dbUser.getSalt());
        //用userId作为key创建试错缓存
        String validTrial=RedisCacheConstant.PWD_VALID_TRIAL+dbUser.getUserId();
        if (!dbUser.getPassWord().equals(loginPwd)){
            //有则自减1，没有则新增这个key
            redisUtil.decrByCreate(validTrial, RedisCacheConstant.PWD_NUM_OF_VALID_TRIAL,
                    RedisCacheConstant.PWD_ERR_LOCK_TIME*Constants.MINUTE);
            //剩余试错次数
            int validTrailNum =(int) redisUtil.get(validTrial);
            if (validTrailNum<1){
                //说明错误已达上限，设置限制登录的key并返回。这里设置两个key是防止用另一个账号来登录。0是占位符，没有意义
                redisUtil.set(RedisCacheConstant.PWD_ERR_LOCK+dbUser.getEmail(),0,
                        RedisCacheConstant.PWD_ERR_LOCK_TIME*Constants.MINUTE);
                redisUtil.del(validTrial);
                return login(loginBo,request);
            }
            return AjaxResult.error("密码错误,剩余次数："+validTrailNum);
        }
        redisUtil.del(validTrial);
        LoginInfo loginInfo =null;
        //删除最后登录的token缓存，登录两次则较早那个token会导致强退
        redisUtil.hashDel(RedisCacheConstant.USER_TOKEN,dbUser.getLastLoginToken());
        //校验通过设置最后登录信息
        dbUser.setLastLoginTime(new Date());
        dbUser.setLastLoginIp(ipAddress);
        dbUser.setLastLoginToken(UUID.fastUUID().toString(true));
        updateById(dbUser);
        //登录成功后设置缓存
        loginInfo =mitchUtils.setLoginInfo(dbUser);
        return AjaxResult.success("欢迎回来~", loginInfo);
    }

    @Override
    public AjaxResult alterUserInfo(AlterInfoBo alterInfoBo, HttpServletRequest request) {
        LoginInfo loginInfo = mitchUtils.getLoginInfo(request);
        SysUser dbUser = getDbUserByEmail(loginInfo.getEmail());
        Assert.isTrue(Objects.equals(dbUser.getUserId(), loginInfo.getUserId()),"无法修改非本人的信息");
        //判断用户是否有修改头像
        if (alterInfoBo.getAvatarFile()!=null&&!alterInfoBo.getAvatarFile().isEmpty()){
            //todo 有则进行头像上传。。。

        }
        dbUser.setUserName(alterInfoBo.getUserName());
        dbUser.setUserSex(alterInfoBo.getUserSex());
        dbUser.setUpdateTime(new Date());
        updateById(dbUser);
        //更新缓存的用户信息
        return AjaxResult.success("修改成功~",mitchUtils.setLoginInfo(dbUser));
    }
    @Override
    public AjaxResult forgotPwd(ForgotPwdBo forgotPwdBo, HttpServletRequest request) {
        mitchUtils.checkEmailCode(forgotPwdBo.getEmail(),forgotPwdBo.getEmailCode());
        SysUser dbUser = getDbUserByEmail(forgotPwdBo.getEmail());
        //生成新的加密盐
        String newSalt = SecurityUtils.getSalt(Constants.SALT_PWD);
        String newPwd = SecurityUtils.encryptPwdByMd5AndSalt(newSalt, forgotPwdBo.getPassWord());
        dbUser.setSalt(newSalt);
        dbUser.setPassWord(newPwd);
        dbUser.setUpdateTime(new Date());
        dbUser.setLastLoginTime(new Date());
        dbUser.setLastLoginIp(SecurityUtils.getIpAddress(request));
        dbUser.setLastLoginToken(UUID.fastUUID().toString(true));
        updateById(dbUser);
        //登录成功后设置缓存
        return AjaxResult.success("欢迎回来~",mitchUtils.setLoginInfo(dbUser));
    }

    @Override
    public AjaxResult alterPwd(PassWordBo pwdBo, HttpServletRequest request) {
        LoginInfo loginInfo = mitchUtils.getLoginInfo(request);
        Assert.isTrue(pwdBo.isSamePwd(),"两次密码输入不一致！");
        SysUser dbUser = getDbUserByEmail(loginInfo.getEmail());
        Assert.isTrue(Objects.equals(dbUser.getUserId(), loginInfo.getUserId()),"无法修改非本人的信息");
        //说明用户已经设置过密码了，否则说明用户还没设置密码
        if (StringUtils.isNotBlank(dbUser.getPassWord())){
            //登录校验
            Assert.isTrue(StringUtils.isNotBlank(pwdBo.getOldPassWord()),"请输入旧密码");
            Assert.isTrue(!redisUtil.hasKey(RedisCacheConstant.PWD_ERR_LOCK+ loginInfo.getEmail()),
                    AjaxResult.error(HttpConstant.UNAUTHORIZED,"旧密码错误次数已达上限，限制操作与登录 "+RedisCacheConstant.PWD_ERR_LOCK_TIME+"分钟"));
            //对旧密码进行盐加密然后比较数据库密码
            String oldPwd = SecurityUtils.encryptPwdByMd5AndSalt(pwdBo.getOldPassWord(), dbUser.getSalt());
            //用userId作为key创建试错缓存
            String validTrial=RedisCacheConstant.PWD_VALID_TRIAL+dbUser.getUserId();
            if (!dbUser.getPassWord().equals(oldPwd)){
                //有则自减1，没有则新增这个key
                redisUtil.decrByCreate(validTrial, RedisCacheConstant.PWD_NUM_OF_VALID_TRIAL,
                        RedisCacheConstant.PWD_ERR_LOCK_TIME*Constants.MINUTE);
                //剩余试错次数
                int validTrailNum =(int) redisUtil.get(validTrial);
                if (validTrailNum<1){
                    //说明错误已达上限，设置限制登录的key并返回。这里设置两个key是防止用另一个账号来登录。0是占位符，没有意义
//                redisUtil.set(RedisCacheConstant.PWD_ERR_LOCK+dbUser.getOpenId(),0,
//                        RedisCacheConstant.PWD_ERR_LOCK_TIME*Constants.MINUTE);
                    redisUtil.set(RedisCacheConstant.PWD_ERR_LOCK+dbUser.getEmail(),0,
                            RedisCacheConstant.PWD_ERR_LOCK_TIME*Constants.MINUTE);
                    redisUtil.del(validTrial);
                    return alterPwd(pwdBo,request);
                }
                return AjaxResult.error("旧密码错误,剩余次数："+validTrailNum);
            }
            //密码一致，删掉错误剩余数
            redisUtil.del(validTrial);
        }
        //生成新的加密盐
        String newSalt = SecurityUtils.getSalt(Constants.SALT_PWD);
        String newPwd = SecurityUtils.encryptPwdByMd5AndSalt(pwdBo.getPassWord(),newSalt);
        dbUser.setSalt(newSalt);
        dbUser.setPassWord(newPwd);
        dbUser.setUpdateTime(new Date());
        updateById(dbUser);
        return AjaxResult.success("修改成功");
    }

    @Override
    public List<LoginInfo> getUserList(QueryListBo queryListBo) {
        List<LoginInfo> loginInfos =userMapper.getUserList(queryListBo);
        if (loginInfos ==null){
            loginInfos = Collections.emptyList();
        }
        return loginInfos;
    }

    @Override
    public AjaxResult newUserStatist() {
        NewUserStatist result =userMapper.getUserStatistics();

        List<String> sevenStrList = Arrays.stream(result.getSevenDayStr().split(",")).toList();
        List<String> sixStrList = Arrays.stream(result.getSixMonthStr().split(",")).toList();
        //把string类型转成int类型
        List<Integer> sevenDay = sevenStrList.stream().map(Integer::parseInt).collect(Collectors.toList());
        List<Integer> sixMonth = sixStrList.stream().map(Integer::parseInt).collect(Collectors.toList());

        result.setSevenDay(sevenDay);
        result.setSixMonth(sixMonth);
        result.setOnlineNum(webSocket.getConnectionCount());
        result.setStatistics(convertMicrosoftRecordMapper.getConversionStatisticsByUid(null));

        return AjaxResult.success("统计完成",result);
    }

    private SysUser getDbUserByEmail(String email){
        SysUser dbUser = getOne(new QueryWrapper<SysUser>().eq("email", email));
        checkUserStatus(dbUser);
        return dbUser;
    }
    private void checkUserStatus(SysUser user){
        Assert.notNull(user,"用户不存在");
        Assert.isTrue(StatusConstants.USER_NORMAL.equals(user.getStatus()),"该用户状态异常");
    }

}
