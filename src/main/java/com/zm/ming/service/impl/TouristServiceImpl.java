package com.zm.ming.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.zm.ming.domain.constant.RedisCacheConstant;
import com.zm.ming.domain.constant.StatusConstants;
import com.zm.ming.domain.po.user.SysTourist;

import com.zm.ming.domain.vo.login.LoginInfo;
import com.zm.ming.domain.yml.convert.ConvertYml;
import com.zm.ming.domain.yml.convert.TouristYml;
import com.zm.ming.mapper.user.TouristMapper;
import com.zm.ming.mapper.user.UserMapper;
import com.zm.ming.service.ITouristService;
import com.zm.ming.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *extends ServiceImpl<TouristMapper, SysTourist> implements ITouristService
 * @author lzm
 */
@Service
@Slf4j
public class TouristServiceImpl implements ITouristService{
    @Resource
    private TouristMapper touristMapper;
    @Resource
    private TouristYml touristYml;
    @Resource
    private MitchUtils mitchUtils;
    @Resource
    private RedisUtil redisUtil;
    @Resource
    private ConvertYml convertYml;


    /**
     * 创建游客信息
     * */
    @Override
    public LoginInfo checkOrCreateTourist(String touristId,String touristIp) {
        LoginInfo loginInfo=null;
        if (StringUtils.isNotBlank(touristId)){
            loginInfo= mitchUtils.getLoginInfo(touristId, RedisCacheConstant.TOURIST_TOKEN);
            log.info("checkOrCreateTourist，原touristId："+touristId+" loginInfo："+loginInfo);
            if (loginInfo==null){
                SysTourist tourist = touristMapper.selectById(touristId);
                if (tourist!=null){
                    //设置游客缓存信息
                    return mitchUtils.setTouristLoginInfo(tourist.getTouristId(),touristIp);
                }
            }else {
                return loginInfo;
            }
        }
        //说明游客id不合法，创建游客id
        int touristNum=touristMapper.countTouristByIpOneDay(touristIp,touristYml.getNewT_hit_day());
        Assert.isTrue(touristNum<touristYml.getNewT_hit_num(),
                "很抱歉，"+touristYml.getNewT_hit_day()+"天最多只能创建"+touristYml.getNewT_hit_num()+"个游客。请选择登录/注册继续使用~");
        SysTourist tourist = new SysTourist();
        Date date = new Date();
        tourist.setCreateTime(date);
        tourist.setTouristIp(touristIp);
        tourist.setLastVisitTime(date);
        touristMapper.insert(tourist);
        return mitchUtils.setTouristLoginInfo(tourist.getTouristId(),touristIp);
    }

    @Override
    public LoginInfo checkTourist(String touristId,String touristIp) {
        log.info("touristId："+touristId+"，touristIp："+touristIp);
        Assert.isTrue(!StringUtils.isAnyBlank(touristId,touristIp),"游客信息为空，请刷新/更换网络环境后重试，若仍未解决，您可进行登录/注册再使用！");
        SysTourist tourist = touristMapper.selectById(touristId);
        Assert.notNull(tourist,"游客ID不合法!");
        tourist.setLastVisitTime(new Date());
        if (!touristIp.equals(tourist.getTouristIp())){
            tourist.setTouristIp(touristIp);
        }
        touristMapper.updateById(tourist);
        return mitchUtils.setTouristLoginInfo(tourist.getTouristId(),touristIp);
    }
}
