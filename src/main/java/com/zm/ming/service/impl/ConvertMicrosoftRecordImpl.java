package com.zm.ming.service.impl;
import cn.hutool.core.collection.CollectionUtil;
import com.aspose.cad.internal.K.F;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zm.ming.domain.bo.basicbusiness.ConvertRecordQuery;
import com.zm.ming.domain.constant.Constants;
import com.zm.ming.domain.constant.RedisCacheConstant;
import com.zm.ming.domain.constant.StatusConstants;
import com.zm.ming.domain.po.ConvertMicrosoftRecord;
import com.zm.ming.domain.po.SysProblemReplay;
import com.zm.ming.domain.result.AjaxResult;
import com.zm.ming.domain.vo.echart.WordCloud;
import com.zm.ming.domain.vo.login.LoginInfo;
import com.zm.ming.domain.vo.microsoft.ConvertTypeVo;
import com.zm.ming.domain.vo.microsoft.OneStatistics;
import com.zm.ming.domain.yml.convert.ConvertYml;
import com.zm.ming.mapper.microsoft.ConvertMicrosoftRecordMapper;
import com.zm.ming.service.IConvertMicrosoftRecordService;
import com.zm.ming.utils.Assert;
import com.zm.ming.utils.RedisUtil;
import com.zm.ming.utils.microsoft.ExcelToOther;
import com.zm.ming.utils.microsoft.PdfToOther;
import com.zm.ming.utils.microsoft.PptxToOther;
import com.zm.ming.utils.microsoft.WordsToOther;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.File;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Lzm
 * @description TODO
 * @date 2022/11/14 11:20
 */
@Service
@Slf4j
public class ConvertMicrosoftRecordImpl extends ServiceImpl<ConvertMicrosoftRecordMapper, ConvertMicrosoftRecord> implements IConvertMicrosoftRecordService {
    @Resource
    private ConvertMicrosoftRecordMapper recordMapper;
    @Resource
    private ConvertYml convertYml;
    @Resource
    private RedisUtil redisUtil;
    @Override
    public AjaxResult getConversionStatisticsById(String convertUid) {
        Assert.isTrue(StringUtils.isNotBlank(convertUid),"用户信息不存在");
        OneStatistics statistics=recordMapper.getConversionStatisticsByUid(convertUid);
        return AjaxResult.success(statistics);
    }

    @Override
    public boolean incrementWordCloud(String uploadType) {
        if (StringUtils.isBlank(uploadType)){
            log.error("IncrementWordCloud false by uploadType isBlank");
            return false;
        }
        List<WordCloud> wordClouds= (List<WordCloud>)redisUtil.hashGet(RedisCacheConstant.CONVERT_TYPE_KEY, RedisCacheConstant.CONVERT_TYPE_VW);
        if (wordClouds ==null || wordClouds.size()==0){
            log.error("IncrementWordCloud false by redis wordCloudList isEmpty");
            return false;
        }
        for (WordCloud wordCloud : wordClouds) {
            if (uploadType.equals(wordCloud.getName())){
                wordCloud.incrementValue();
                break;
            }
        }
        if (!redisUtil.hashSet(RedisCacheConstant.CONVERT_TYPE_KEY, RedisCacheConstant.CONVERT_TYPE_VW, wordClouds)){
            log.error("IncrementWordCloud false by redis set error");
            return false;
        }
        log.info("IncrementWordCloud true");
         return true;
    }

    @Override
    public List<WordCloud> countDbUploadType() {
        List<WordCloud> wordClouds = null;
        //先获取缓存，有则返回，无则查库
        wordClouds= (List<WordCloud>)redisUtil.hashGet(RedisCacheConstant.CONVERT_TYPE_KEY, RedisCacheConstant.CONVERT_TYPE_VW);
        if (wordClouds == null || wordClouds.size() ==0){
            //缓存没命中，则查数据库
            wordClouds=recordMapper.countUploadType();
            if (wordClouds ==null || wordClouds.size()==0){
                //说明数据库无转换记录，则new一个列表，避免为null
                wordClouds=new ArrayList<>(1);
            }
            Set<String> dbWcNameSet = wordClouds.stream().map(WordCloud::getName).collect(Collectors.toSet());
            //获取当前系统允许转换的类型列表
            Set<String> allUploadType = getAllUploadType();
            for (String ct : allUploadType) {
                if (!dbWcNameSet.contains(ct)){
                    WordCloud wordCloud = new WordCloud();
                    wordCloud.setName(ct);
                    //设置1避免没有显示到词云中
                    wordCloud.setValue(1);
                    wordClouds.add(wordCloud);
                }
            }
            //设置到缓存中
            redisUtil.hashSet(RedisCacheConstant.CONVERT_TYPE_KEY,RedisCacheConstant.CONVERT_TYPE_VW,
                    wordClouds);
        }

        return wordClouds;
    }

    @Override
    public Set<String> getAllUploadType() {
        Set<String> allUploadType=null;
        if (redisUtil.hHasKey(RedisCacheConstant.CONVERT_TYPE_KEY,RedisCacheConstant.CONVERT_TYPE_VU)) {
            allUploadType =(Set<String>)redisUtil.hashGet(RedisCacheConstant.CONVERT_TYPE_KEY,RedisCacheConstant.CONVERT_TYPE_VU);
        }else{
            //设置所有的可上传类型，set去重
            allUploadType=new HashSet<>();
            allUploadType.addAll(WordsToOther.allowedSourceType);
            allUploadType.addAll(PdfToOther.allowedSourceType);
            allUploadType.addAll(ExcelToOther.allowedSourceType);
            allUploadType.addAll(PptxToOther.allowedSourceType);
            redisUtil.hashSet(RedisCacheConstant.CONVERT_TYPE_KEY,RedisCacheConstant.CONVERT_TYPE_VU,
                    allUploadType);
        }
        return allUploadType;
    }

    @Override
    public ConvertTypeVo getConvertTypeVo() {
        //获取转换类型映射
        ConvertTypeVo convertType=new ConvertTypeVo();;
        Map<String,Set<String>> convertMap=null;
        Set<String> allUploadType = getAllUploadType();
        List<WordCloud> convertTypeWordCloudList=countDbUploadType();//获取统计转换数据（词云数据对象）
        if (redisUtil.hHasKey(RedisCacheConstant.CONVERT_TYPE_KEY,RedisCacheConstant.CONVERT_TYPE_VM)) {
            convertMap=(Map<String,Set<String>>)redisUtil.hashGet(RedisCacheConstant.CONVERT_TYPE_KEY,RedisCacheConstant.CONVERT_TYPE_VM);
        }else{
            //根据可上传类型获取对应的可转换类型，set去重
            convertMap =new HashMap<>();
            for (String uploadType : allUploadType) {
                Set<String>convertTpe=new HashSet<>();
                if (WordsToOther.allowedSourceType.contains(uploadType)){
                    convertTpe.addAll(WordsToOther.allowedConvertType);
                }
                if (PdfToOther.allowedSourceType.contains(uploadType)){
                    convertTpe.addAll(PdfToOther.allowedConvertType);
                }
                if (ExcelToOther.allowedSourceType.contains(uploadType)){
                    convertTpe.addAll(ExcelToOther.allowedConvertType);
                }
                if (PptxToOther.allowedSourceType.contains(uploadType)){
                    convertTpe.addAll(PptxToOther.allowedConvertType);
                }
                convertMap.put(uploadType,convertTpe);
            }
            //设置到缓存中
            redisUtil.hashSet(RedisCacheConstant.CONVERT_TYPE_KEY,RedisCacheConstant.CONVERT_TYPE_VM,
                    convertMap);
        }
        //设置到对象里
        convertType.setAllUploadType(allUploadType);
        convertType.setConvertMap(convertMap);
        convertType.setCountUploadTypeList(convertTypeWordCloudList);
        return convertType;
    }

    @Override
    public AjaxResult getConvertRecordList(ConvertRecordQuery recordQuery) {
        //开启分页
        PageHelper.startPage(recordQuery.getPage(),recordQuery.getLimit());
        //得到对应的分页对象
        PageInfo<ConvertMicrosoftRecord> pageInfo=new PageInfo<>(recordMapper.queryConvertRecordList(recordQuery));
        return AjaxResult.success(pageInfo);
    }

    @Override
    public void delExpiredConvertRecord() {
        Date now =new Date();
        Calendar calendar = Calendar.getInstance(); //得到日历
        calendar.setTime(now);//把当前时间赋给日历
        calendar.add(convertYml.getSave_date_unit(), convertYml.getSave_date_num());
        //获取过期时间点
        Date expiredTime = calendar.getTime();
        //todo 查询过期的数据。数据量大的时候考虑分批处理
        List<ConvertMicrosoftRecord> convertMicrosoftRecords = recordMapper.selectList(new QueryWrapper<ConvertMicrosoftRecord>()
                .eq("convert_status",StatusConstants.CONVERT_OK).le("convert_time", expiredTime).or().le("create_time", expiredTime));
        if (CollectionUtil.isEmpty(convertMicrosoftRecords)){
            log.info("当前无待删除的过期文档数据");
            return;
        }
        List<Long> expiredIds=new ArrayList<>(convertMicrosoftRecords.size());
        List<Long> expiredFailIds=new ArrayList<>();
        for (ConvertMicrosoftRecord record : convertMicrosoftRecords) {
            if (StringUtils.isNotBlank(record.getConvertUrl())){
                File expiredFile=new File(convertYml.getSave_path()+record.getConvertUrl());
                boolean isDel=false;
                if (expiredFile.exists()){
                    isDel = expiredFile.delete();
                    log.info("convertId："+record.getConvertId()+" 文件删除结果："+isDel);
                }else {
                    log.warn("convertId："+record.getConvertId()+" 文件不存在！路径："+convertYml.getSave_path()+record.getConvertUrl());
                    isDel = true;
                }
                if (isDel){
                    expiredIds.add(record.getConvertId());
                }else {
                    expiredFailIds.add(record.getConvertId());
                }
            }
        }
        //置空，免占内存
        convertMicrosoftRecords=null;
        //更新删除结果状态
        if (expiredIds.size()>0){
            int e = recordMapper.updateExpiredConvertStatus(expiredIds);
            log.info("expiredIds size："+expiredIds.size()+"，update result："+e);
        }
        if (expiredFailIds.size()>0) {
            int f = recordMapper.updateExpiredFailConvertStatus(expiredFailIds);
            log.info("expiredFailIds size：" + expiredIds.size() + "，update result：" + f);
        }
    }
}
