package com.zm.ming.service;

import com.zm.ming.domain.result.AjaxResult;

/**
 * @author ZM
 * @date 2022/11/8 14:27
 */
public interface CurrencyService {
    /**
     * 生成随机验证码
     * @return 生成结果
     * */
    AjaxResult generateRandomCode();
}
