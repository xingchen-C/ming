package com.zm.ming.handler.convert_chain;

import com.zm.ming.domain.bo.microsoft.ConvertFileBo;
import com.zm.ming.domain.bo.microsoft.ConvertResult;
import com.zm.ming.domain.constant.Constants;
import com.zm.ming.domain.constant.MicrosoftConstants;
import com.zm.ming.handler.ConvertFileHandler;
import com.zm.ming.utils.Assert;
import com.zm.ming.utils.FileUtils;
import com.zm.ming.utils.UUID;
import com.zm.ming.utils.microsoft.ExcelToOther;
import com.zm.ming.utils.microsoft.PdfToOther;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.Resource;


/**
 * @description excel转其它文档链
 * @author lzm
 * @date 2022/11/10 22:42
 */
public class ExcelToChain extends ConvertFileHandler {
    @Override
    public ConvertResult convertFileByUseType(ConvertFileBo convertBo) {
        if (ExcelToOther.allowedSourceType.contains(convertBo.getUploadType())&&
        ExcelToOther.allowedConvertType.contains(convertBo.getConvertType())) {
            ConvertResult cr = new ConvertResult();
            //判断选择转换方式是否符合当前处理链处理的类型
            if (Constants.USE_CONVERT_M2B.equals(convertBo.getUseType())) {
                cr.setBytesResult(ExcelToOther.convertTypeBytes(convertBo.getConvertFile(), convertBo.getConvertType(),convertBo.getConvertUid()));
            } else {
                cr.setPathResult(ExcelToOther.convertType(convertBo.getConvertFile(), convertBo.getPreSavePath(), convertBo.getConvertType(),convertBo.getConvertUid()));
            }
            cr.setWsNoticeId(convertBo.getConvertUid());
            return cr;
        }
        //不符合当前链所处理的类型则交给下一个链处理
        Assert.isTrue(nextDeal!=null,super.getFailReason());
        return nextDeal.convertFileByUseType(convertBo);
    }
}
