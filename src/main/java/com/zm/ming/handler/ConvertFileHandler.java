package com.zm.ming.handler;

import cn.hutool.core.date.StopWatch;
import com.zm.ming.domain.bo.microsoft.ConvertFileBo;
import com.zm.ming.domain.bo.microsoft.ConvertResult;
import com.zm.ming.handler.convert_chain.*;
import com.zm.ming.utils.MitchUtils;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.Instant;

/**
 * @author ZM
 * @date 2022/9/28 21:23
 * 构建文档转换责任链
 */
@Component
@Data
@Slf4j
public abstract class ConvertFileHandler<T> {
    private final String failReason="当前所选项未能满足可转换的类型";

    /**责任链，下一个处理者*/
    protected ConvertFileHandler<T> nextDeal=null;
    /**
     * 责任链，下一个链接节点
     */
    protected  void next(ConvertFileHandler handler) {
        this.nextDeal = handler;
    }

    /**
     * 处理逻辑的抽象方法
     * @param convertFileBo 请求入参实体
     * @return 处理结果
     * */
    public abstract ConvertResult convertFileByUseType(ConvertFileBo convertFileBo);

    public static class Builder<T> {
        private ConvertFileHandler<T> head;
        private ConvertFileHandler<T> tail;

        public Builder<T> addDealHandler(ConvertFileHandler handler) {
            if (this.head == null) {
                this.head = handler;
            } else {
                this.tail.next(handler);
            }
            this.tail=handler;
            return this;
        }
        public ConvertFileHandler build() {
            return this.head;
        }
    }

    /**
     * 构建文档处理责任链,该责任链调用的都会把转换后的文件保存到本地并返回文件的路径
     * @param convertFileBo 转换对象
     * @return ConvertResult
     * */
    public static  ConvertResult buildConvertFileChain(ConvertFileBo convertFileBo){
        //计算耗时
        StopWatch stopWatch = MitchUtils.startStopWatch();
        //开始转换
        log.info(String.format("buildConvertFileChain convert_record_id【%d】convert start",convertFileBo.getConvertId()));
        ConvertResult convertResult = buildHandler().convertFileByUseType(convertFileBo);
        double consumingSeconds = MitchUtils.getSecond(stopWatch);;
        convertResult.setConsumingSecond(consumingSeconds);
        log.info(String.format("buildConvertFileChain convert_record_id【%d】convert end，consuming second：【%f】",convertFileBo.getConvertId(),consumingSeconds));

        return convertResult;
    }

    private static ConvertFileHandler buildHandler(){
        ConvertFileHandler.Builder<Object> builder= new ConvertFileHandler.Builder<>();
        return builder
                //构建链，第一个是校验文件的链，校验通过才能进入后面处理文档的链
//                .addDealHandler(new CheckFileSizeChain())//文件大小前置校验不再走此处，而是在入队前就进行校验
                .addDealHandler(new WordsToChain())
                .addDealHandler(new PptxToChain())
                .addDealHandler(new ExcelToChain())
                .addDealHandler(new PdfToChain())
                .build();
    }
}
