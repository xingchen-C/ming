package com.zm.ming.handler.websocket;

import com.zm.ming.domain.result.WebsocketResult;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;
import java.util.Set;

/**
 * @author Lzm
 * @description TODO
 * @date 2024/4/9 23:46
 */

public interface WebSocketInterface {
    /**
     * 会话开始回调
     *
     * @param session 会话
     */
    void handleOpen(WebSocketSession session);

    /**
     * 会话结束回调
     *
     * @param session 会话
     */
    void handleClose(WebSocketSession session);

    /**
     * 处理消息
     *
     * @param session 会话
     * @param message 接收的消息
     */
    void handleMessage(WebSocketSession session, String message);
    /**
     * 发送消息
     *
     * @param session 当前会话
     * @param wsResult 要发送的消息
     */
    void sendMessage(WebSocketSession session, WebsocketResult wsResult);
    /**
     * 发送消息
     *
     * @param userId 当前会话
     * @param wsResult 要发送的消息
     */
    void sendMessage(String userId, WebsocketResult wsResult);
    /**
     * 发送消息
     *
     * @param session 当前会话
     * @param message 要发送的消息
     */
    void sendMessage(WebSocketSession session, String message);

    /**
     * 发送消息
     *
     * @param userId  用户id
     * @param message 要发送的消息
     */
    void sendMessage(String userId, TextMessage message);

    /**
     * 发送消息
     *
     * @param userId  用户id
     * @param message 要发送的消息
     */
    void sendMessage(String userId, String message);

    /**
     * 发送消息
     *
     * @param session 当前会话
     * @param message 要发送的消息
     */
    void sendMessage(WebSocketSession session, TextMessage message);

    /**
     * 广播
     *
     * @param message 字符串消息
     */
    void broadCast(String message);

    /**
     * 广播
     *
     * @param message 文本消息
     */
    void broadCast(TextMessage message);

    /**
     * 处理会话异常
     *
     * @param session 会话
     * @param error   异常
     */
    void handleError(WebSocketSession session, Throwable error);

    /**
     * 获得所有的 websocket 会话
     *
     * @return 所有 websocket 会话
     */
    Set<WebSocketSession> getSessions();

    /**
     * 得到当前连接数
     *
     * @return 连接数
     */
    int getConnectionCount();
}
