package com.zm.ming.handler.websocket;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.zm.ming.domain.constant.WebsocketConstants;
import com.zm.ming.domain.result.WebsocketResult;
import com.zm.ming.utils.MitchUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author Lzm
 * @description TODO
 * @date 2024/4/9 23:46
 */
@Slf4j
@Service
public class WebSocketImpl implements WebSocketInterface{
    @Resource
    private MitchUtils mitchUtils;
    /**
     * 在线连接数（线程安全）
     */
    private final AtomicInteger connCount = new AtomicInteger(0);
    /**
     * 线程安全的无序集合（存储会话）
     * todo 后续考虑存在redis中
     */
    private final CopyOnWriteArraySet<WebSocketSession> sessions = new CopyOnWriteArraySet<>();

    @Override
    public void handleOpen(WebSocketSession session) {
        sessions.add(session);
        int count = connCount.incrementAndGet();
        log.info("ws a conn opened，session info：{}，current online count：{}",session,count);
        WebsocketResult wr = new WebsocketResult();
        wr.setMessage("当前在线人数为："+count);
        wr.setMsgType(WebsocketConstants.CONN_SUC);
        sendMessage(session,wr);
    }

    @Override
    public void handleClose(WebSocketSession session) {
        sessions.remove(session);
        int count = connCount.decrementAndGet();
        log.info("ws a conn closed，session info：{}，current online count：{}",session,count);
    }

    @Override
    public void handleMessage(WebSocketSession session, String message) {
        // 只处理前端传来的文本消息，并且直接丢弃了客户端传来的消息
        log.info("ws received a message，session info：{}，message：{}",session,message);
        JSONObject clientMsgJson= JSONObject.parseObject(message);
        WebsocketResult wr = JSONObject.toJavaObject(clientMsgJson, WebsocketResult.class);
        log.info("msg to wsr："+wr);
        //根据消息类型处理不同业务逻辑
        if (wr!=null){
            switch (wr.getMsgType()){

            }
        }
    }

    @Override
    public void sendMessage(WebSocketSession session, WebsocketResult wsResult){
        this.sendMessage(session, JSON.toJSONString(wsResult));
    }

    @Override
    public void sendMessage(String token, WebsocketResult wsResult){
        this.sendMessage(token,JSON.toJSONString(wsResult));
    }

    @Override
    public void sendMessage(WebSocketSession session, String message){
        this.sendMessage(session, new TextMessage(message));
    }

    @Override
    public void sendMessage(String token, TextMessage message){
        Optional<WebSocketSession> userSession = sessions.stream().filter(session -> {
            if (!session.isOpen()) {
                return false;
            }
            if (StringUtils.isBlank(token)){
                return false;
            }
            return mitchUtils.getLoginInfo(token)!=null;
        }).findFirst();
        if (userSession.isPresent()) {
            try {
                userSession.get().sendMessage(message);
            } catch (IOException e) {
                log.error("ws sendMessage fail，session info：{}，token{}，message{}",userSession.get(),token,message,e);
            }
        }
    }

    @Override
    public void sendMessage(String token, String message){
        this.sendMessage(token, new TextMessage(message));
    }

    @Override
    public void sendMessage(WebSocketSession session, TextMessage message){
        try {
            session.sendMessage(message);
        } catch (IOException e) {
            log.error("ws sendMessage fail，session info：{}，message：{}，error：{}",session,message,e);
        }
    }

    @Override
    public void broadCast(String message){
        for (WebSocketSession session : sessions) {
            if (!session.isOpen()) {
                continue;
            }
            this.sendMessage(session, message);
        }
    }

    @Override
    public void broadCast(TextMessage message){
        for (WebSocketSession session : sessions) {
            if (!session.isOpen()) {
                continue;
            }
            try {
                session.sendMessage(message);
            } catch (IOException e) {
                log.error("ws broadCast fail，session info：{}，message{}，error：{}",session,message,e);
            }
        }
    }

    @Override
    public Set<WebSocketSession> getSessions() {
        return sessions;
    }

    @Override
    public void handleError(WebSocketSession session, Throwable error) {
        log.error("websocket处理会话异常：session id：{}，error：{}",session.getId(),error);
    }


    @Override
    public int getConnectionCount() {
        int num = connCount.get();
        log.info("当前在线人数："+num);
        return num;
    }
}
