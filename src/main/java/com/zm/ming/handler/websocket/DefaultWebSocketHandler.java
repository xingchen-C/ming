package com.zm.ming.handler.websocket;

import org.springframework.stereotype.Component;
import org.springframework.web.socket.*;

import javax.annotation.Resource;

/**
 * @author Lzm
 * @description TODO
 * @date 2024/4/9 23:35
 */
@Component
public class DefaultWebSocketHandler implements WebSocketHandler {
    @Resource
    private WebSocketImpl webSocket;

    /**
     * 建立连接
     *
     * @param session WebSocketSession
     */
    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        webSocket.handleOpen(session);
    }
    /**
     * 接收消息
     *
     * @param session WebSocketSession
     * @param message 消息
     */
    @Override
    public void handleMessage(WebSocketSession session, WebSocketMessage<?> message) throws Exception {
        if (message instanceof TextMessage textMessage) {
            webSocket.handleMessage(session, textMessage.getPayload());
        }
    }
    /**
     * 发生错误
     *
     * @param session   WebSocketSession
     * @param throwable 异常
     */
    @Override
    public void handleTransportError(WebSocketSession session, Throwable throwable) throws Exception {
        webSocket.handleError(session, throwable);
    }
    /**
     * 关闭连接
     *
     * @param session     WebSocketSession
     * @param closeStatus 关闭状态
     */
    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus closeStatus) throws Exception {
        webSocket.handleClose(session);
    }
    /**
     * 是否支持发送部分消息
     *
     * @return false
     */
    @Override
    public boolean supportsPartialMessages() {
        return true;
    }
}
