package com.zm.ming.exception;

import com.zm.ming.domain.result.AjaxResult;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Lzm
 * @description TODO自定义异常
 * @date 2022/11/22 9:42
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomException extends RuntimeException{
    private AjaxResult ajaxResult;
}
