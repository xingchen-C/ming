package com.zm.ming.exception;

import com.zm.ming.domain.result.AjaxResult;
import com.zm.ming.domain.result.HttpConstant;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.mail.MailException;
import org.springframework.validation.BindException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;

/**
 * @author ZM
 * @date 2022/11/2 15:07
 * 全局异常处理器
 */
@NotNull
@RestControllerAdvice
public class GlobalExceptionHandler extends RuntimeException{
    private static final Logger log = LoggerFactory.getLogger(GlobalExceptionHandler.class);
    /**
     * 自定义验证异常
     */
    @ExceptionHandler(BindException.class)
    public AjaxResult handleBindException(BindException e){
        log.error(e.getMessage(), e);
        String message = e.getAllErrors().get(0).getDefaultMessage();
        return AjaxResult.error(message);
    }
    /**
     * 请求方式不支持
     */
    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public AjaxResult handleHttpRequestMethodNotSupported(HttpRequestMethodNotSupportedException e,
                                                          HttpServletRequest request){
        String requestURI = request.getRequestURI();
        log.error("请求地址'{}',不支持'{}'请求", requestURI, e.getMethod());
        return AjaxResult.error(HttpConstant.BAD_METHOD,e.getMessage());
    }
    /**
     * org.springframework.util.Assert校验异常、基础业务异常
     */
    @ExceptionHandler(IllegalArgumentException.class)
    public Object handleAssertException(IllegalArgumentException e)
    {
        log.error("Assert校验异常："+e.getMessage());
        String message = e.getMessage();
        return AjaxResult.error(message);
    }

    /**
     * 拦截未知的运行时异常
     */
    @ExceptionHandler(RuntimeException.class)
    public AjaxResult handleRuntimeException(RuntimeException e, HttpServletRequest request)
    {
        String requestURI = request.getRequestURI();
        log.error("请求地址'{}',发生未知异常.", requestURI, e);
        return AjaxResult.error(HttpConstant.ERROR,"操作失败");
    }
    /**
     * 系统异常
     */
    @ExceptionHandler(Exception.class)
    public AjaxResult handleException(Exception e, HttpServletRequest request)
    {
        String requestURI = request.getRequestURI();
        log.error("请求地址'{}',发生系统异常.", requestURI, e);
        return AjaxResult.error(HttpConstant.ERROR,"系统异常");
    }

    /**邮件异常*/
    @ExceptionHandler(MailException.class)
    public AjaxResult handleMailException(MailException e)
    {
        log.error("邮件发送异常，原因："+ e.getMessage());
        return AjaxResult.error(HttpConstant.ERROR,"邮件发送失败！");
    }

    /**自定义异常*/
    @ExceptionHandler(CustomException.class)
    public AjaxResult handleCustomException(CustomException e)
    {
        AjaxResult errResult = e.getAjaxResult();
        log.error("自定义异常，原因："+ errResult.get(AjaxResult.MSG_TAG));
        if (errResult.get(AjaxResult.CODE_TAG)==null){
            errResult.replace(AjaxResult.CODE_TAG,HttpConstant.ERROR);
        }
        return errResult;
    }
}
