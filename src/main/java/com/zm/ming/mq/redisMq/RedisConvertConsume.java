package com.zm.ming.mq.redisMq;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.zm.ming.domain.bo.microsoft.ConvertFileBo;
import com.zm.ming.domain.yml.convert.ConvertRedisStreamParams;
import com.zm.ming.service.MicrosoftService;
import com.zm.ming.utils.BeanUtils;
import com.zm.ming.utils.RedisUtil;
import com.zm.ming.utils.microsoft.AsposeUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.redis.connection.stream.ObjectRecord;
import org.springframework.data.redis.connection.stream.RecordId;
import org.springframework.data.redis.stream.StreamListener;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author Lzm
 * @description TODO
 * @date 2024/3/24 17:36
 */
@Component
@Slf4j
public class RedisConvertConsume implements StreamListener<String, ObjectRecord<String, String>> {
    @Resource
    private MicrosoftService microsoftService;
    @Resource
    private RedisUtil redisUtil;
    @Resource
    private ConvertRedisStreamParams streamParams;

    @Override
    public void onMessage(ObjectRecord<String, String> message) {
        RecordId id = message.getId();
        String convertMsg = message.getValue();
        if (StringUtils.isBlank(convertMsg)){
            log.error(String.format("recordId【%s】convert-consume fail，convertMsg is blank",id));
        }else {
            if (convertMsg.contains("\\")){
                convertMsg=convertMsg.replaceAll("\\\\","");
            }
            convertMsg=convertMsg.substring(1,convertMsg.length()-1);
            log.info("getValue str："+convertMsg);
            ConvertFileBo convertBo = JSON.parseObject(convertMsg).toJavaObject(ConvertFileBo.class);
            log.info("to bean："+convertBo);
            log.info(String.format("convert-consume by redis is start...recordId:%s，convertId:%d",id.getValue(),convertBo.getConvertId()));
            microsoftService.convertFileCustomer(convertBo);
        }
        log.info("convert-consume by redis is end...recordId:"+id+"，convertMsg："+convertMsg);
    }
}
