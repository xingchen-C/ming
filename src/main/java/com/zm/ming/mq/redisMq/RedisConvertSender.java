package com.zm.ming.mq.redisMq;

import cn.hutool.core.bean.BeanUtil;
import com.alibaba.fastjson.JSON;
import com.zm.ming.domain.bo.microsoft.ConvertFileBo;
//import com.zm.ming.utils.BeanUtils;
import com.zm.ming.domain.yml.convert.ConvertRedisStreamParams;
import com.zm.ming.utils.RedisUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.connection.stream.ObjectRecord;
import org.springframework.data.redis.connection.stream.RecordId;
import org.springframework.data.redis.connection.stream.StreamRecords;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Lzm
 * @description TODO
 * @date 2024/3/24 17:35
 */
@Component
@RequiredArgsConstructor
@Slf4j
public class RedisConvertSender {
    @Resource
    private RedisUtil redisUtil;
    @Resource
    private ConvertRedisStreamParams streamParams;

    public void sendMessage(ConvertFileBo fileBo) {
//        ObjectRecord<String, ConvertFileBo> record = StreamRecords.newRecord()
//                .in(streamParams.getConvert_stream())
//                .ofObject(fileBo)
//                .withId(RecordId.autoGenerate());
//        RecordId recordId = redisUtil.addConvertMessage(record);
        String convertJsonStr = JSON.toJSONString(fileBo);
        log.info("convertJsonStr："+convertJsonStr);

        RecordId recordId = redisUtil.addConvertMessage(streamParams.getConvert_stream(),convertJsonStr);
//        RecordId recordId = redisUtil.sendStreamMsg(streamParams.getConvert_stream(), fileBo);
        log.info("转换队列发送消息，接收参数为："+fileBo.toString());
        log.info("Message sent to RedisConvertSender，with RecordId: " + recordId);
    }
}
