package com.zm.ming.mq.redisMq;

import lombok.extern.slf4j.Slf4j;
import org.springframework.util.ErrorHandler;

/**
 * @author Lzm
 * @description TODO
 * @date 2024/3/25 20:41
 */
@Slf4j
public class ConsumeErrorHandler implements ErrorHandler {
    @Override
    public void handleError(Throwable throwable) {
        log.error("队列消费时出现异常！",throwable);
    }
}
