package com.zm.ming.verify.filter;

import com.zm.ming.utils.RedisUtil;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.http.server.reactive.ServerHttpRequest;
import reactor.core.publisher.Flux;
import javax.annotation.Resource;
import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.nio.CharBuffer;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.atomic.AtomicReference;
/**
 * * 两者的本质区别：拦截器（Interceptor）是基于Java的反射机制，而过滤器（Filter）是基于函数回调。
 *  * 从灵活性上说拦截器功能更强大些，Filter能做的事情，都能做，而且可以在请求前，请求后执行，比较灵活。
 *  * Filter主要是针对URL地址做一个编码的事情、过滤掉没用的参数、安全校验（比较泛的，比如登录不登录之类），太细的话，还是建议用interceptor。
 *  * 不过还是根据不同情况选择合适的。
 *  * 注意，这里可以使用@WebFilter注解或配置类的方式使得该过滤器生效
 *  * 而@WebFilter这个注解是Servlet3.0的规范，并不是Spring boot提供的。除了这个注解以外，我们还需在配置类中加另外一个注解：@ServletComponetScan，指定扫描的包。*/
/**
 * @author ZM
 * @date 2022/11/6 13:27
 * 过滤器解决乱码和请求url过滤
 */
@WebFilter(urlPatterns = "/**",filterName = "GarbledCodeAndUrlFilter")
public class GarbledCodeAndUrlFilter implements Filter {
    private static final String encoding="UTF-8";

    /**
     * filter对象创建的时候调用（服务器启动时创建，因为可能一个filter对应多个servlet,启动时先把filter准备好，访问资源就可以进行过滤了）
     * 这里面有个filterConfig:是当前filter对象的配置信息，和servletConfig差不多是获取名称、参数
     */
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    /**当匹配到mapping时调用doFilter
     * filterChain：过滤器链对象，内部维护着各个filter的索引，并且知道所有filter的顺序，是根据mapping的顺序来执行的！！！
     * */
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain) throws IOException, ServletException {
        // 设置字符编码链锁
        servletRequest.setCharacterEncoding(encoding);
        servletRequest.setCharacterEncoding(encoding);
        //url过滤
        HttpServletRequest request= (HttpServletRequest) servletRequest;
        String path = request.getServletPath();
        String[] exclusionsUrls = { ".js", ".gif", ".jpg", ".png", ".css", ".ico", ".html" };
        for (String str : exclusionsUrls) {
            if (path.contains(str)) {
                // 传递给下一个filter
                chain.doFilter(servletRequest, servletResponse);
                return;
            }
        }
        chain.doFilter(servletRequest,servletResponse);
        // 使用HttpServletRequestWrapper的包装类对请求进行加工
//        chain.doFilter(new XssHttpServletRequestWrapper(request), servletResponse);
    }
    /**
     * 当filter对象销毁的时候调用（服务器关闭时），和ServletContext的生命周期是一样的
     * */
    @Override
    public void destroy() {

    }
    /**
     * 从请求体中获取body的表单内容
     * */
    private String resolveBodyFromRequest(ServerHttpRequest serverHttpRequest)
    {
        // 获取请求体
        Flux<DataBuffer> body = serverHttpRequest.getBody();
        AtomicReference<String> bodyRef = new AtomicReference<>();
        body.subscribe(buffer -> {
            CharBuffer charBuffer = StandardCharsets.UTF_8.decode(buffer.asByteBuffer());
            DataBufferUtils.release(buffer);
            bodyRef.set(charBuffer.toString());
        });
        return bodyRef.get();
    }
}
