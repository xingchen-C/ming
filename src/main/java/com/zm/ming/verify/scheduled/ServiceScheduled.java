package com.zm.ming.verify.scheduled;

import com.zm.ming.domain.yml.convert.ConvertYml;
import com.zm.ming.service.IConvertMicrosoftRecordService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author Lzm
 * @description TODO 业务定时任务
 * @date 2024/3/17 22:41
 */
@Component
@Slf4j
public class ServiceScheduled {
    @Resource
    private IConvertMicrosoftRecordService recordService;
    /**
     * 每天凌晨4点定时删除过期的转换文档
     * TODO 待测试！
     * */
    @Scheduled(cron ="0 0 4 * * ?")
//    @Scheduled(cron ="0/10 * * * * ?")
    public void delExpiredDocuments() {
        log.info("ServiceScheduled-delExpiredDocuments start");
        recordService.delExpiredConvertRecord();
        log.info("ServiceScheduled-delExpiredDocuments end");
    }
}
