package com.zm.ming.verify.interceptor;

import com.zm.ming.domain.constant.Constants;
import com.zm.ming.domain.constant.RedisCacheConstant;
import com.zm.ming.domain.constant.StatusConstants;
import com.zm.ming.domain.result.AjaxResult;
import com.zm.ming.domain.result.HttpConstant;
import com.zm.ming.domain.vo.login.LoginInfo;
import com.zm.ming.utils.*;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * @author PC
 * 非法拦截未登录访问
 * 继承HandlerInterceptorAdapter适配器,并重写具体的方法
 * 截器要生效需要一个配置类来配置相关要拦截的资源和不用拦截的资源
 */
public class NoLoginInterceptor extends HandlerInterceptorAdapter {
    @Resource
    private RedisUtil redisUtil;
    @Resource
    private MitchUtils mitchUtils;

    /**在执行目标方法（目标资源）之前要执行的方法，返回false代表不放行要执行的方法，返回true代表目标方法可被执行*/
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //对无需登录即可访问的接口都进行ip地址获取的操作，获取不到都不给操作
        String ipAddress = SecurityUtils.getIpAddress(request);
        //判断用户是否已经登录
        LoginInfo loginInfo = mitchUtils.getLoginInfo(request,RedisCacheConstant.USER_TOKEN);
        Assert.isTrue(loginInfo!=null,new AjaxResult(HttpConstant.UNAUTHORIZED,"用户未登录"));
        if (!ipAddress.equals(loginInfo.getLoginIp())){
            //ip地址发生改变时设置新的请求ip
            loginInfo.setLoginIp(ipAddress);
            mitchUtils.setLoginInfo(loginInfo,RedisCacheConstant.USER_TOKEN);
        }else {
            //请求的ip地址相同则无感刷新用户缓存过期时间
            if (redisUtil.getExpire(mitchUtils.getTokenKey(request,RedisCacheConstant.USER_TOKEN))<10*Constants.MINUTE){
                redisUtil.expire(mitchUtils.getTokenKey(request,RedisCacheConstant.USER_TOKEN),RedisCacheConstant.TOKEN_EXPIRES_IN);
            }
        }
        return true;
    }
}
