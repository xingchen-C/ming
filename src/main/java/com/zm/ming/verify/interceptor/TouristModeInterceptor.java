package com.zm.ming.verify.interceptor;

import com.zm.ming.domain.constant.Constants;
import com.zm.ming.domain.constant.RedisCacheConstant;
import com.zm.ming.domain.result.HttpConstant;
import com.zm.ming.domain.vo.login.LoginInfo;
import com.zm.ming.service.ITouristService;
import com.zm.ming.service.IUserService;
import com.zm.ming.utils.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * @author lzm
 * 游客模式拦截器，拦截非游客、非用户的空状态用户请求
 * 继承HandlerInterceptorAdapter适配器,并重写具体的方法
 * 截器要生效需要一个配置类来配置相关要拦截的资源和不用拦截的资源
 */
public class TouristModeInterceptor extends HandlerInterceptorAdapter {
    @Resource
    private RedisUtil redisUtil;
    @Resource
    private MitchUtils mitchUtils;
    @Resource
    private ITouristService touristService;
    @Resource
    private IUserService userService;

    /**在执行目标方法（目标资源）之前要执行的方法，返回false代表不放行要执行的方法，返回true代表目标方法可被执行*/
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //对无需登录即可访问的接口都进行ip地址获取的操作，获取不到都不给操作
        String ipAddress = SecurityUtils.getIpAddress(request);
        //先获取用户缓存信息，有则使用，无则获取游客缓存信息
        LoginInfo loginInfo = mitchUtils.getLoginInfo(request);
        if (loginInfo==null){
            //获取不到，检测数据库是否存在游客信息
            touristService.checkTourist(SecurityUtils.getTouristToken(request),ipAddress);
            return true;
        }
        String tokenType = mitchUtils.getTokenTypeByUserSourceType(loginInfo.getUserSourceType());
        if (!ipAddress.equals(loginInfo.getLoginIp())){
           //ip地址发生改变时设置新的请求ip
           loginInfo.setLoginIp(ipAddress);
           mitchUtils.setLoginInfo(loginInfo,tokenType);
       }else if (redisUtil.getExpire(mitchUtils.getTokenKey(request,tokenType))<10*Constants.MINUTE){
           //请求的ip地址相同则无感刷新用户缓存过期时间
           redisUtil.expire(mitchUtils.getTokenKey(request,tokenType),RedisCacheConstant.TOKEN_EXPIRES_IN);
       }
        return true;
    }
}
