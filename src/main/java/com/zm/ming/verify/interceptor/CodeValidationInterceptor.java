package com.zm.ming.verify.interceptor;

import com.alibaba.fastjson.JSONObject;
import com.zm.ming.domain.constant.RedisCacheConstant;
import com.zm.ming.utils.Assert;
import com.zm.ming.utils.RedisUtil;
import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import reactor.core.publisher.Flux;

import javax.annotation.Resource;
import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.nio.CharBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

//import org.apache.http.entity.ContentType;

/**
 * @author ZM
 * @date 2022/11/6 13:27
 * 两者的本质区别：拦截器（Interceptor）是基于Java的反射机制，而过滤器（Filter）是基于函数回调。
 * 从灵活性上说拦截器功能更强大些，Filter能做的事情，都能做，而且可以在请求前，请求后执行，比较灵活。
 * Filter主要是针对URL地址做一个编码的事情、过滤掉没用的参数、安全校验（比较泛的，比如登录不登录之类），太细的话，还是建议用interceptor。
 * 不过还是根据不同情况选择合适的。
 * 注意，这里可以使用@WebFilter注解或配置类的方式使得该过滤器生效
 * 而@WebFilter这个注解是Servlet3.0的规范，并不是Spring boot提供的。除了这个注解以外，我们还需在配置类中加另外一个注解：@ServletComponetScan，指定扫描的包。
 */
public class CodeValidationInterceptor extends HandlerInterceptorAdapter {
    @Resource
    private RedisUtil redisUtil;

    /**在执行目标方法（目标资源）之前要执行的方法，返回false代表不放行要执行的方法，返回true代表目标方法可被执行*/
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //获取放在请求头的随机验证码
//        String s = resolveBodyFromHttpServletRequest(request);
//        JSONObject jsonObject = JSONObject.parseObject(s);
        String uuid = request.getParameter("uuid");
        String putCode =request.getParameter("code");
        Assert.isTrue(!StringUtils.isAnyBlank(uuid,putCode),"请输入随机码！");
        String cacheCode = String.valueOf(redisUtil.get(RedisCacheConstant.GRAPHIC_CODE + uuid)).toLowerCase();
        //校验
        Assert.isTrue(cacheCode.equals(putCode.toLowerCase()) ,"随机码校验错误！");
        //通过删除该随机码并放行
        redisUtil.del(RedisCacheConstant.GRAPHIC_CODE + uuid);
        return true;
    }
    /**
     * 从请求体中获取body的表单内容
     * */
    private String resolveBodyFromServerHttpRequest(ServerHttpRequest serverHttpRequest)
    {
        // 获取请求体
        Flux<DataBuffer> body = serverHttpRequest.getBody();
        AtomicReference<String> bodyRef = new AtomicReference<>();
        body.subscribe(buffer -> {
            CharBuffer charBuffer = StandardCharsets.UTF_8.decode(buffer.asByteBuffer());
            DataBufferUtils.release(buffer);
            bodyRef.set(charBuffer.toString());
        });
        return bodyRef.get();
    }
    /**
     * 从请求体中获取body的表单内容
     * */
    @SneakyThrows(IOException.class)
    private String resolveBodyFromHttpServletRequest(HttpServletRequest request)
    {
        // 获取请求体
        StringBuffer sb = new StringBuffer();
        String line = null;
        BufferedReader reader = request.getReader();
        while ((line = reader.readLine()) != null){
            sb.append(line);
        }
        Assert.isTrue(!sb.isEmpty(),"获取不到提交的数据");
        //将空格和换行符替换掉避免使用反序列化工具解析对象时失败
        return sb.toString().replaceAll("\s","");
    }
}
