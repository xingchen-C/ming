package com.zm.ming.verify.interceptor;

import com.zm.ming.domain.constant.StatusConstants;
import com.zm.ming.domain.vo.login.LoginInfo;
import com.zm.ming.utils.*;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Lzm
 * @description TODO
 * @date 2022/12/10 16:24
 */
public class AdminInterceptor extends HandlerInterceptorAdapter {
    @Resource
    private MitchUtils mitchUtils;

    /**在执行目标方法（目标资源）之前要执行的方法，返回false代表不放行要执行的方法，返回true代表目标方法可被执行*/
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        LoginInfo user = mitchUtils.getLoginInfo(request);
        //判断用户是否有管理员权限
        Assert.isTrue(user!=null && StatusConstants.ROLE_SUPER.equals(user.getConvertRole()),"权限不足");
        return true;
    }
}
