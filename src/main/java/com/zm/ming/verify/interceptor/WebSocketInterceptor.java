package com.zm.ming.verify.interceptor;

import com.zm.ming.domain.constant.Constants;
import com.zm.ming.domain.constant.RedisCacheConstant;
import com.zm.ming.domain.result.AjaxResult;
import com.zm.ming.domain.result.HttpConstant;
import com.zm.ming.domain.vo.login.LoginInfo;
import com.zm.ming.service.ITouristService;
import com.zm.ming.utils.Assert;
import com.zm.ming.utils.MitchUtils;
import com.zm.ming.utils.RedisUtil;
import com.zm.ming.utils.SecurityUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.server.HandshakeInterceptor;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * @author PC
 * 非法拦截未登录访问
 * 继承HandshakeInterceptor适配器,并重写具体的方法
 */
public class WebSocketInterceptor implements HandshakeInterceptor {
    @Resource
    private MitchUtils mitchUtils;

    /**在执行目标方法（目标资源）之前要执行的方法，返回false代表不放行要执行的方法，返回true代表目标方法可被执行*/
    @Override
    public boolean beforeHandshake(ServerHttpRequest request, ServerHttpResponse serverHttpResponse, WebSocketHandler webSocketHandler, Map<String, Object> map) throws Exception {
        if (request instanceof ServletServerHttpRequest servletServerHttpRequest) {
            String token = servletServerHttpRequest.getServletRequest().getParameter("token");
            if (StringUtils.isNotBlank(token)){
                //先获取用户缓存信息，有则使用，无则获取游客缓存信息
                LoginInfo loginInfo = mitchUtils.getLoginInfo(token);
                //获取不到，检测数据库是否存在游客信息
                return loginInfo != null;
            }
        }
        return false;
    }

    @Override
    public void afterHandshake(ServerHttpRequest serverHttpRequest, ServerHttpResponse serverHttpResponse, WebSocketHandler webSocketHandler, Exception e) {

    }
}
