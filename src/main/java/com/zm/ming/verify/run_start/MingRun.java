package com.zm.ming.verify.run_start;

import com.zm.ming.domain.constant.RedisCacheConstant;
import com.zm.ming.domain.yml.convert.ConvertRedisStreamParams;
import com.zm.ming.service.IConvertMicrosoftRecordService;
import com.zm.ming.utils.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Arrays;

/**
 * @author Lzm
 * @description TODO 项目启动后会自动执行的方法
 * 一般是更新缓存配置
 * @date 2024/3/17 22:31
 */
@Component
@Slf4j
public class MingRun implements ApplicationRunner {
    @Resource
    private IConvertMicrosoftRecordService recordService;
    @Resource
    private RedisUtil redisUtil;
    @Resource
    private ConvertRedisStreamParams streamParams;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        log.info("ming app run myself method start ...");

        reloadConvertTypeVo();
        initConvertRedisStream();
    }
    private void reloadConvertTypeVo(){
        log.info("reloadConvertTypeVo start");
        redisUtil.del(RedisCacheConstant.CONVERT_TYPE_KEY);
        recordService.getConvertTypeVo();
        log.info("reloadConvertTypeVo end");
    }
    private void initConvertRedisStream() {
        log.info("initConvertRedisStream start..");
        /**
         * 这里必须先判空，重复创建组会报错，获取不存在的key的组也会报错
         * 所以需要先判断是否存在key，在判断是否存在组
         * 我这里只有一个组，如果需要创建多个组的话则需要改下逻辑
         */
        if (redisUtil.hasKey(streamParams.getConvert_stream())) {
            redisUtil.checkOrCreateStream(streamParams.getConvert_stream(),streamParams.getConvert_group());
        } else {
            redisUtil.createStream(streamParams.getConvert_stream(),streamParams.getConvert_group());
        }
        log.info("initConvertRedisStream end..");
    }

}
