package com.zm.ming.utils;

import cn.hutool.core.date.StopWatch;
import com.aspose.words.FixedPageSaveOptions;
import com.zm.ming.domain.bo.BasePageQuery;
import com.zm.ming.domain.constant.MicrosoftConstants;
import com.zm.ming.domain.constant.RedisCacheConstant;
import com.zm.ming.domain.constant.StatusConstants;
import com.zm.ming.domain.po.user.SysUser;
import com.zm.ming.domain.result.AjaxResult;
import com.zm.ming.domain.result.HttpConstant;
import com.zm.ming.domain.vo.login.LoginInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.*;

/**
 * @author Lzm
 * @description TODO
 * @date 2022/12/7 16:27
 */
@Component
public class MitchUtils {
    @Resource
    RedisUtil redisUtil;

    //获取缓存中的用户/游客信息，优先获取用户，其次获取游客
    public LoginInfo getLoginInfo(String token){
        LoginInfo loginInfo = (LoginInfo) redisUtil.get(getTokenKey(token,RedisCacheConstant.USER_TOKEN));
        if (loginInfo ==null){
            loginInfo=(LoginInfo) redisUtil.get(getTokenKey(token,RedisCacheConstant.TOURIST_TOKEN));
        }
        if (loginInfo != null){
            Assert.isTrue(StatusConstants.USER_NORMAL.equals(loginInfo.getStatus()),
                    AjaxResult.error(HttpConstant.UNAUTHORIZED,"用户状态异常，请联系管理员"));
        }
        return loginInfo;
    }
    public LoginInfo getLoginInfo(HttpServletRequest request){
        LoginInfo loginInfo = (LoginInfo) redisUtil.get(getTokenKey(request,RedisCacheConstant.USER_TOKEN));
        if (loginInfo ==null){
            loginInfo=(LoginInfo) redisUtil.get(getTokenKey(request,RedisCacheConstant.TOURIST_TOKEN));
        }
        if (loginInfo != null){
            Assert.isTrue(StatusConstants.USER_NORMAL.equals(loginInfo.getStatus()),
                    AjaxResult.error(HttpConstant.UNAUTHORIZED,"用户状态异常，请联系管理员"));
        }
        return loginInfo;
    }
    public LoginInfo getLoginInfo(HttpServletRequest request,String tokenType){
        String tokenKey = getTokenKey(request, tokenType);
        LoginInfo loginInfo = (LoginInfo) redisUtil.get(tokenKey);
        if (loginInfo != null){
            Assert.isTrue(StatusConstants.USER_NORMAL.equals(loginInfo.getStatus()),
                    AjaxResult.error(HttpConstant.UNAUTHORIZED,"用户状态异常，请联系管理员"));
        }
        return loginInfo;
    }
    public LoginInfo getLoginInfo(String token,String tokenType){
        String tokenKey = getTokenKey(token, tokenType);
        LoginInfo loginInfo = (LoginInfo) redisUtil.get(tokenKey);
        if (loginInfo != null){
            Assert.isTrue(StatusConstants.USER_NORMAL.equals(loginInfo.getStatus()),
                    AjaxResult.error(HttpConstant.UNAUTHORIZED,"用户状态异常，请联系管理员"));
        }
        return loginInfo;
    }

    public String getTokenKey(HttpServletRequest request,String tokenType){
        Assert.isTrue(RedisCacheConstant.TOKEN_TYPE.contains(tokenType),"用户缓存类型有误！");
        String tokenKey=null;
        if (RedisCacheConstant.USER_TOKEN.equals(tokenType)){
            tokenKey=getTokenKey(SecurityUtils.getToken(request),tokenType);
        }else if (RedisCacheConstant.TOURIST_TOKEN.equals(tokenType)){
            tokenKey=getTokenKey(SecurityUtils.getTouristToken(request),tokenType);
        }//..
        return tokenKey;
    }
    public String getTokenKey(String token,String tokenType){
        Assert.isTrue(RedisCacheConstant.TOKEN_TYPE.contains(tokenType),"用户缓存类型有误！");
        return tokenType+token;
    }
    public String getTokenTypeByUserSourceType(Integer userSourceType){
        String tokenType=null;
        if (StatusConstants.SOURCE_USER.equals(userSourceType)){
            tokenType=RedisCacheConstant.USER_TOKEN;
        }else if (StatusConstants.SOURCE_TOURIST.equals(userSourceType)){
            tokenType=RedisCacheConstant.TOURIST_TOKEN;
        }
        return tokenType;
    }
    /**
     * 设置登录用户信息缓存
     * */
    public LoginInfo setLoginInfo(SysUser loginCache){
        Assert.isTrue (StatusConstants.USER_NORMAL.equals(loginCache.getStatus()),"用户状态异常，请联系管理员");
        LoginInfo loginInfo = new LoginInfo();
        BeanUtils.copyProperties(loginCache, loginInfo);
        loginInfo.setRequestUid(String.valueOf(loginCache.getUserId()));
        loginInfo.setUserSourceType(StatusConstants.SOURCE_USER);
        return setLoginInfo(loginInfo,RedisCacheConstant.USER_TOKEN);
    }
    /**
     * 设置登录用户信息缓存
     * */
    public LoginInfo setTouristLoginInfo(String touristId,String ipAddress){
        LoginInfo loginInfo = new LoginInfo();
        loginInfo.setRequestUid(touristId);
        loginInfo.setLoginIp(ipAddress);
        loginInfo.setUserSourceType(StatusConstants.SOURCE_TOURIST);
        loginInfo.setConvertRole(StatusConstants.ROLE_TOURIST);
        loginInfo.setStatus(StatusConstants.USER_NORMAL);
        loginInfo.setFreeConvertNum(1);
        loginInfo.setSpendConvertNum(0);
        return setLoginInfo(loginInfo,RedisCacheConstant.TOURIST_TOKEN);
    }
    /**
     * 设置登录用户信息缓存
     * */
    public LoginInfo setLoginInfo(LoginInfo loginInfo,String tokenType){
        Assert.isTrue(RedisCacheConstant.TOKEN_TYPE.contains(tokenType),"用户缓存类型有误！");
        String key=null;
        if (RedisCacheConstant.TOURIST_TOKEN.equals(tokenType)){
            key=tokenType+loginInfo.getRequestUid();
        } else if (RedisCacheConstant.USER_TOKEN.equals(tokenType)) {
            key=tokenType+loginInfo.getLastLoginToken();
        }
        redisUtil.set(key,loginInfo,RedisCacheConstant.TOKEN_EXPIRES_IN);
        return loginInfo;
    }

    /**
     * 设置登录用户的转换次数缓存
     * */
    public boolean setConvertNumCache(String token,Integer freeNum,Integer spendNum){
        boolean result = false;
        LoginInfo loginInfo =getLoginInfo(token);
        Assert.notNull(loginInfo, AjaxResult.error(HttpConstant.UNAUTHORIZED,"用户登录信息已过期"));
        if (freeNum>=0){
            loginInfo.setFreeConvertNum(freeNum);
            result=true;
        }
        if (spendNum>=0){
            loginInfo.setSpendConvertNum(spendNum);
            result=true;
        }
        if (result){
//            setLoginInfo(loginInfo);
        }
        return result;
    }

    /**
     * 设置登录用户信息缓存
     * */
    public void checkEmailCode(String email,String emailCode){
        Assert.isTrue(!StringUtils.isAnyBlank(email,emailCode),"请输入邮箱或邮箱验证码~");
        Assert.isTrue(InfoUtils.isEmail(email), "邮箱格式不正确");
        String cacheEmailCode =String.valueOf(redisUtil.get(RedisCacheConstant.EMAIL_CODE+email)) ;
        Assert.isTrue(emailCode.equals(cacheEmailCode),"验证码不正确或已失效");
        //校验通过删除邮箱验证码缓存key
        redisUtil.del(RedisCacheConstant.EMAIL_CODE+email);
    }

    /**
     * 根据页数、条数获取redis指定前缀key的实体数据
     * */
    public <T> List<T> getByPrefixKey(String prefix, BasePageQuery query,Class<?> pojo){
        int pageNum = query.getPage();
        int limit = query.getLimit();
        Set<String> keys = redisUtil.scanGetKeysByPrefixKey(prefix, pageNum * limit);
        List<T> result=new ArrayList<>(keys.size());
        for (String key : keys) {
            result.add((T)redisUtil.get(key));
        }
        return result;
    }

    /**
     * 计时器，开始计时
     * */
    public static StopWatch  startStopWatch(){
        StopWatch consumptionTime = new StopWatch();
        consumptionTime.start();
        return consumptionTime;
    }
    /**
     * 计时器，结束计时并获取耗时s，这个必须与startStopWatch()成对使用
     * */
    public static double getSecond(StopWatch consumptionTime){
        consumptionTime.stop();
        return consumptionTime.getTotalTimeSeconds();
    }

}
