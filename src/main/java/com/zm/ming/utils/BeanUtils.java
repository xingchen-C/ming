package com.zm.ming.utils;

import cn.hutool.core.bean.BeanDesc;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.PropDesc;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.zm.ming.domain.bo.microsoft.ConvertFileBo;

import java.lang.reflect.Method;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Lzm
 * @description TODO
 * @date 2024/3/24 22:34
 */
public class BeanUtils extends BeanUtil{
    public static Map<String, String> beanToMap(Object bean) {
        Map<String, String> targetMap = new HashMap<>();
        if (bean == null) {
            return null;
        }
        //获取类的属性描述
        Collection<PropDesc> props = BeanUtil.getBeanDesc(bean.getClass()).getProps();
        String key;
        Method getter;
        Object value;
        for (PropDesc prop : props) {
            key = prop.getFieldName();
            // 过滤class属性
            // 得到property对应的getter方法
            getter = prop.getGetter();
            if (null != getter) {
                // 只读取有getter方法的属性
                try {
                    value = getter.invoke(bean);
                } catch (Exception ignore) {
                    continue;
                }
                //如果是集合特殊处理
                if (value != null && value instanceof Collection) {
                    listToMap(targetMap, (Collection<Object>) value, key);
                    continue;
                }
                if (value != null && false == value.equals(bean)) {
                    targetMap.put(key, null == value ? null : String.valueOf(value));
                }
            }
        }
        return targetMap;
    }

    private static Map<String, String> listToMap(Map<String, String> targetMap, Collection<Object> collection, String fieldName) {
        int i = 0;
        for (Object bean : collection) {
            Map<String, String> tempMap = beanToMap(bean);
            for (String key : tempMap.keySet()) {
                targetMap.put(fieldName + "[" + i + "]." + key, tempMap.get(key));
            }
            i++;
        }
        return targetMap;
    }
}
