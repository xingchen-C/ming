package com.zm.ming.utils.microsoft;


import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.ZipUtil;
import com.alibaba.fastjson.JSONObject;
import com.aspose.slides.*;
import com.zm.ming.domain.constant.MicrosoftConstants;
import com.zm.ming.domain.constant.WebsocketConstants;
import com.zm.ming.domain.result.WebsocketResult;
import com.zm.ming.handler.websocket.WebSocketImpl;
import com.zm.ming.utils.FileUtils;
import com.zm.ming.utils.UUID;
import com.zm.ming.utils.WebSocketUtil;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;

import static com.zm.ming.utils.microsoft.AsposeUtils.loadLicense;

/**
 * @author lzm
 *  ppt转文档工具类
 * */
@Component
@Slf4j
public class PptxToOther {
    private static PptxToOther instance;
    @PostConstruct
    public void init() {
        instance = this;
    }
    private static WebSocketImpl webSocket;
    /**
     *使用构造器注入websocket
     * */
    public PptxToOther(WebSocketImpl webSocket){
        PptxToOther.webSocket=webSocket;
    }
    /**转换完成后产生多个文件需要进行打包的有：\html\图片*/
    public static final List<String> isZipType=new ArrayList<>(3);
    public static final List<String> isImageType=new ArrayList<>(2);
    /**当前支持转的类型*/
    public static final List<String> allowedConvertType=new ArrayList<>(11);
    static {
        allowedConvertType.add(MicrosoftConstants.Odp);//todo 转Odp和Fodp、otp有时报错SolidFillColor is unavailable for FillType=Gradient
        allowedConvertType.add(MicrosoftConstants.Fodp);
        allowedConvertType.add(MicrosoftConstants.Otp);

        allowedConvertType.add(MicrosoftConstants.Pdf);
        allowedConvertType.add(MicrosoftConstants.Tiff);
        allowedConvertType.add(MicrosoftConstants.Gif);//转换时间太久，文件大
        allowedConvertType.add(MicrosoftConstants.Swf);
        allowedConvertType.add(MicrosoftConstants.Xps);
        //如果工具类新增加转换的类型，则在这里添加对应类型...
        allowedConvertType.add(MicrosoftConstants.Html);

        isImageType.add(MicrosoftConstants.Jpeg);
        isImageType.add(MicrosoftConstants.Png);
        //合并
        isZipType.addAll(isImageType);
        allowedConvertType.addAll(isZipType);
    }
    /**当前支持上传文件的类型*/
    public static final List<String> allowedSourceType=new ArrayList<>(9);
    static {
        allowedSourceType.add(MicrosoftConstants.Ppt);
        allowedSourceType.add(MicrosoftConstants.Pptx);
        allowedSourceType.add(MicrosoftConstants.Pps);
        allowedSourceType.add(MicrosoftConstants.Pot);
        allowedSourceType.add(MicrosoftConstants.Ppsx);
        allowedSourceType.add(MicrosoftConstants.Pptm);
        allowedSourceType.add(MicrosoftConstants.Ppsm);
        allowedSourceType.add(MicrosoftConstants.Potx);
        allowedSourceType.add(MicrosoftConstants.Potm);
//        allowedSourceType.add(MicrosoftConstants.Odp);
    }
    /**将文件后缀转为com.aspose.slides.SaveFormat对应的类型*/
    private static int getSaveFormatByType(String convertType){
        int i;
        switch (convertType){
            case MicrosoftConstants.Pdf: i = 1;break;
            case MicrosoftConstants.Xps: i = 2;break;
            case MicrosoftConstants.Tiff: i = 5;break;
            case MicrosoftConstants.Odp: i = 6;break;
            case MicrosoftConstants.Html: i = 13;break;
//            case MicrosoftConstants.Html5: i = 23;break;
            case MicrosoftConstants.Swf: i = 15;break;
            case MicrosoftConstants.Otp: i = 17;break;
            case MicrosoftConstants.Fodp: i = 21;break;
            case MicrosoftConstants.Gif: i = 22;break;

            default:i= SaveFormat.Pptx;
        }
        return i;
    }
    /**
     * 根据convertType类型将wordFile转其它文档的字节码
     * 此方法用于非保存本地
     * @param pptFile:    word文件MultipartFile格式
     * @param convertType: 需转成的文件类型
     * @return 生成的byte[]
     */
    @SneakyThrows(Exception.class)
    public static byte[] convertTypeBytes(MultipartFile pptFile,String convertType,String wsNoticeId) {
        //校验合法类型
        AsposeUtils.checkType(pptFile,allowedSourceType,convertType,allowedConvertType);
        //加载配置，否则有水印和页数限制
        loadLicense(MicrosoftConstants.PPTX_TO_OTHER);
        //linux下加载window字体
        AsposeUtils.loadLinuxFonts(MicrosoftConstants.PPTX_TO_OTHER);
        //构建文档对象
        Presentation document = new Presentation(pptFile.getInputStream());
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        byte[] resultByte = null;
        if (isImageType.contains(convertType)){
            String tempDir=MicrosoftConstants.TO_BYTE_TEMP_PATH+ UUID.fastUUID().toString(true)+File.separator;
            String fileName=FileUtils.getFilePrefix(pptFile);
            FileUtil.mkdir(tempDir);
            String convertFileSuffix="."+convertType;
            for (int i = 0; i < document.getSlides().size(); i++) {
                ISlide slide = document.getSlides().get_Item(i);
                BufferedImage image = slide.getThumbnail(1f, 1f);
                ImageIO.write(image,convertType,new File(String.format(tempDir+fileName+"_%d"+convertFileSuffix,i+1)));
            }
            File zip = ZipUtil.zip(tempDir);
            resultByte=FileUtil.readBytes(zip);
            //然后把原本的目录和压缩包文件删掉
            FileUtil.del(zip);
            FileUtil.del(tempDir);
        }else {
            SaveOptions saveOptions = customSaveOptions(convertType,wsNoticeId);
            if (saveOptions==null){
                document.save(outputStream, getSaveFormatByType(convertType));
            }else {
                document.save(outputStream, getSaveFormatByType(convertType),saveOptions);
            }
            resultByte=outputStream.toByteArray();
        }
        WebSocketUtil.sendConvertSuc(webSocket,wsNoticeId);
        // 返回生成的文件字节码
        return resultByte;
    }
    /**
     * 根据convertType类型将wordFile转其它文档
     * 此方法用于保存本地
     * @param pptFile:    ppt文件MultipartFile格式
     * @param systemPath 生成的文档所在的根目录，还需要拼接其它目录,savePath是最终文档的绝对路径
     * @param convertType 要转换成的文档类型
     * @return 生成的文件路径
     */
    @SneakyThrows(Exception.class)
    public static String convertType(MultipartFile pptFile, String systemPath,String convertType,String wsNoticeId) {
        //校验合法类型
        AsposeUtils.checkType(pptFile,allowedSourceType,convertType,allowedConvertType);
        //加载配置，否则有水印和页数限制
        loadLicense(MicrosoftConstants.PPTX_TO_OTHER);
        //构建文档对象
        Presentation document = new Presentation(pptFile.getInputStream());
        String fileName = FileUtils.getFilePrefix(pptFile);
        String namePath=FileUtils.dateFilePath(null);
        String zipDir=systemPath+convertType+namePath;
        String savePath=zipDir+fileName;
        //由于excel和ppt转换的保存需要savePath路径下的文件是存在的，所以这里创建一个
        FileUtil.mkdir(zipDir);
        String convertFileSuffix="."+convertType;
        //save保存的是savePath这个路径下包含的文件名称，如果只有路径没有文件名，就会导致在该路径下生成一个对应的文件但是没有后缀
        if (isImageType.contains(convertType)){
            //如果属于转图片，则转成缩略图
            for (int i = 0; i < document.getSlides().size(); i++) {
                ISlide slide = document.getSlides().get_Item(i);
                BufferedImage image = slide.getThumbnail(1f, 1f);
                ImageIO.write(image,convertType,new File(String.format(savePath+"_%d"+convertFileSuffix,i+1)));
            }
        }else {
            //其它类型来这里转
            savePath +=convertFileSuffix;
            SaveOptions saveOptions = customSaveOptions(convertType,wsNoticeId);
            if (saveOptions==null){
                document.save(savePath, getSaveFormatByType(convertType));
            }
            document.save(savePath, getSaveFormatByType(convertType),saveOptions);
        }
        //转成isZipType里面的都会产生多个文件则需要打成压缩包类型
        if (isZipType.contains(convertType)){
            File zip = ZipUtil.zip(zipDir);
            savePath=zip.getPath().replaceAll("\\\\", Matcher.quoteReplacement(File.separator));
            //然后把原本的目录删掉
            FileUtil.del(zipDir);
        }
        WebSocketUtil.sendConvertSuc(webSocket,wsNoticeId);
        return savePath.split(systemPath)[1];
    }
    private static SaveOptions customSaveOptions(String convertType,String wsNoticeId){
        SaveOptions saveOptions=null;
        switch (convertType){
            case MicrosoftConstants.Pdf: saveOptions = customPdfSaveOptions(wsNoticeId);break;
            case MicrosoftConstants.Xps: saveOptions = customXpsSaveOptions(wsNoticeId);break;
            case MicrosoftConstants.Tiff: saveOptions = customTiffSaveOptions(wsNoticeId);break;
            case MicrosoftConstants.Gif: saveOptions = customGifSaveOptions(wsNoticeId);break;
            case MicrosoftConstants.Html: saveOptions = customHtmlSaveOptions(wsNoticeId);break;
            case MicrosoftConstants.Swf: saveOptions = customSwfSaveOptions(wsNoticeId);break;
        }
        return saveOptions;
    }
    private static SaveOptions customPdfSaveOptions(String wsNoticeId) {
        PdfOptions options=new PdfOptions();
        options.setProgressCallback(getProgress(wsNoticeId));
        //指定生成的文档是否应包含隐藏的幻灯片。默认值为假。
//        options.setShowHiddenSlides(true);
        //指定要用于文档中的所有文本内容的压缩类型。读/写PdfTextCompression。默认值为PdfTextCompression.Flate
//        options.setTextCompression(PdfTextCompression.Flate);
        //指示是否必须自动为每个图像选择最有效的压缩（而不是默认压缩）。如果设置为 true，则将为演示文稿中的每个图像选择最合适的压缩算法，
        // 这将导致生成的 PDF 文档的尺寸更小。最佳图像压缩率选择在计算上是昂贵的，并且需要额外的RAM，默认情况下此选项为假。
//        options.setBestImagesCompressionRatio(true);
        //包含一组标志，指定在使用用户访问权限打开文档时应授予哪些访问权限。请参阅PdfAccessPermissions。
//        options.setAccessPermissions();
        /*默认值为true。PDF文档可以包含矢量图形和光栅图像。如果SaveMetafilesAsPng设置为true，
        则源图元文件图像将转换为Png格式并作为光栅图像保存到Pdf。如果SaveMetafilesAsPng设置为false，
        则源图元文件将转换为Pdf矢量图形。每种方法都有优点和缺点。例如，如果将图元文件转换为 PNG，
        则在生成的文档缩放过程中可能会有一些质量损失。如果将图元文件转换为 Pdf 矢量图形，则 Pdf 查看工具中可能会出现性能问题。*/
        options.setSaveMetafilesAsPng(true);
        // 设置PDF 文档中图像的分辨率。属性会影响文件大小、导出时间和图像质量。默认96
        options.setSufficientResolution(66);
        //如果为 true，则在每张幻灯片周围绘制黑框。读/写布尔值。默认false
//        options.setDrawSlidesFrame(true);
        //设置图像透明色。（颜色值）
//        options.setImageTransparentColor();
        //如果为 true，则对图像应用指定的透明色。
//        options.setApplyImageTransparent();
        return options;
    }
    private static SaveOptions customXpsSaveOptions(String wsNoticeId) {
        XpsOptions options=new XpsOptions();
        options.setProgressCallback(getProgress(wsNoticeId));
        return options;
    }
    private static SaveOptions customGifSaveOptions(String wsNoticeId) {
        GifOptions options=new GifOptions();
        options.setProgressCallback(getProgress(wsNoticeId));
        //获取或设置过渡 FPS [帧/秒] 默认值为 25。数值越大，gif在切换图片的时候就越慢
        options.setTransitionFps(15);
        //设置默认延迟时间 [ms]。如果未设置 （ISlideShowTransition.getAdvanceAfterTime/ISlideShowTransition.setAdvanceAfterTime（long）），则将使用此值。默认值为 1000。
//        options.setDefaultDelay();
        //确定是否导出隐藏的幻灯片。默认值为 false。
//        options.setExportHiddenSlides();
        //设置帧大小（尺寸值）
        Dimension dimension=new Dimension();
        dimension.setSize(700,350);
        options.setFrameSize(dimension);
        return options;
    }
    private static SaveOptions customTiffSaveOptions(String wsNoticeId) {
        TiffOptions options=new TiffOptions();
        options.setProgressCallback(getProgress(wsNoticeId));
        //指定生成的文档是否应包含隐藏的幻灯片。默认值为假。
//        options.setShowHiddenSlides();
        //指定生成的 TIFF 图像的大小。默认值为 0x0，这意味着生成的图像大小将根据演示幻灯片大小值计算
//        options.setImageSize();
        //以每英寸点数指定水平分辨率。默认96
//        options.setDpiX();
        //以每英寸点数指定垂直分辨率。默认96
//        options.setDpiY();
        //指定压缩类型。TiffCompressionTypes.xx
        /*CCITT3	指定 CCITT3 压缩方案。
          CCITT4	指定 CCITT4 压缩方案。
          LZW	指定 LZW 压缩方案（默认）。
          RLE	指定 RLE 压缩方案。*/
//        options.setCompressionType(TiffCompressionTypes.);
        //指定生成的图像的像素格式。ImagePixelFormat.xx
        /*Format1bppIndexed	每像素 1 位，已编入索引。
          Format4bppIndexed	每像素 4 位，已编入索引。
          Format8bppIndexed	每像素 8 位，已编入索引。（用这个转完比较小，像素也不差）
          Format24bppRgb	每像素 24 位，RGB。
          Format32bppRgb	每像素 32 位，ARGB。*/
        options.setPixelFormat(ImagePixelFormat.Format8bppIndexed);
        return options;
    }
    private static SaveOptions customHtmlSaveOptions(String wsNoticeId) {
        HtmlOptions options=new HtmlOptions();
        options.setProgressCallback(getProgress(wsNoticeId));
        return options;
    }
    private static SaveOptions customSwfSaveOptions(String wsNoticeId) {
        SwfOptions options=new SwfOptions();
        options.setProgressCallback(getProgress(wsNoticeId));
        return options;
    }
    //todo 目前只有转pdf有进度
    private static IProgressCallback getProgress(String wsNoticeId){
        WebsocketResult ws = new WebsocketResult();
        ws.setMsgType(WebsocketConstants.CONVERT_PROGRESS);
        return new IProgressCallback() {
            @Override
            public void reporting(double v) {
                ws.setMessage((int)v);
                webSocket.sendMessage(wsNoticeId,ws);
            }
        };
    }
}
