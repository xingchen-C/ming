package com.zm.ming.utils.microsoft;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.ZipUtil;
import com.aspose.pdf.*;
import com.aspose.pdf.devices.*;
import com.aspose.pdf.exceptions.FontNotFoundException;
import com.aspose.pdf.text.CustomFontSubstitutionBase;
import com.zm.ming.domain.constant.MicrosoftConstants;
import com.zm.ming.domain.constant.WebsocketConstants;
import com.zm.ming.domain.result.WebsocketResult;
import com.zm.ming.handler.websocket.WebSocketImpl;
import com.zm.ming.utils.FileUtils;
import com.zm.ming.utils.UUID;
import com.zm.ming.utils.WebSocketUtil;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import java.io.ByteArrayOutputStream;
import java.io.File;


import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;

import static com.zm.ming.utils.microsoft.AsposeUtils.loadLicense;


/**
 * @author lzm
 *  pdf转文档工具类
 *  已破解，转换前无需加载license
 * */
@Component
@Slf4j
public class PdfToOther {
    private static PdfToOther instance;
    @PostConstruct
    public void init() {
        instance = this;
    }
    /**
     *使用构造器注入websocket
     * */
    public PdfToOther(WebSocketImpl webSocket){
        PdfToOther.webSocket=webSocket;
    }

    private static WebSocketImpl webSocket;
    /**转换完成后产生多个文件需要进行打包的有：md\html\图片*/
    public static final List<String> isZipType=new ArrayList<>(8);
    /**当前支持转的类型。*/
    public static final List<String> isImageType=new ArrayList<>(5);
    /**当前支持转的类型。*/
    public static final List<String> allowedConvertType=new ArrayList<>(17);
    static {
        allowedConvertType.add(MicrosoftConstants.Doc);
        allowedConvertType.add(MicrosoftConstants.Xps);
        allowedConvertType.add(MicrosoftConstants.Svg);
        allowedConvertType.add(MicrosoftConstants.Xlsx);
        allowedConvertType.add(MicrosoftConstants.Epub);
        allowedConvertType.add(MicrosoftConstants.Plugin);
        allowedConvertType.add(MicrosoftConstants.Aps);
        allowedConvertType.add(MicrosoftConstants.PdfXml);
        allowedConvertType.add(MicrosoftConstants.Pptx);
        allowedConvertType.add(MicrosoftConstants.Docx);
        allowedConvertType.add(MicrosoftConstants.Html);
//        allowedConvertType.add(MicrosoftConstants.Tex);//todo Tex待解决：The required table 'head' was not found.
        allowedConvertType.add(MicrosoftConstants.MobiXml);//todo 不支持mobi保存为流，只能保存为file
        //如果工具类新增加转换的类型，则在这里添加对应类型...
        isImageType.add(MicrosoftConstants.Bmp);
        isImageType.add(MicrosoftConstants.Jpeg);
        isImageType.add(MicrosoftConstants.Gif);
        isImageType.add(MicrosoftConstants.Png);
        isImageType.add(MicrosoftConstants.Emf);
        //转换完会有多个文件的需要打成压缩包的类型有
        isZipType.addAll(isImageType);

        allowedConvertType.addAll(isZipType);
    }
    /**当前支持上传文件的类型*/
    public static final List<String> allowedSourceType=new ArrayList<>(1);
    static {
        allowedSourceType.add(MicrosoftConstants.Pdf);
        allowedSourceType.add(MicrosoftConstants.MarkDown);
        allowedSourceType.add(MicrosoftConstants.Ps);
        allowedSourceType.add(MicrosoftConstants.Epub);
        allowedSourceType.add(MicrosoftConstants.Svg);
        allowedSourceType.add(MicrosoftConstants.Xml);
    }

    /**
     * 根据convertType类型将wordFile转其它文档的字节码
     * 此方法用于非保存本地
     * @param pdfFile:    pdf文件MultipartFile格式
     * @param convertType: 需转成的文件类型
     * @return 生成的byte[]
     */
    @SneakyThrows(Exception.class)
    public static   byte[] convertTypeBytes(MultipartFile pdfFile,String convertType,String wsNoticeId) {
        //校验合法类型
        AsposeUtils.checkType(pdfFile,allowedSourceType,convertType,allowedConvertType);
        //加载配置，否则有水印和页数限制
        loadLicense(MicrosoftConstants.PDF_TO_OTHER);
        //linux下加载window字体
        AsposeUtils.loadLinuxFonts(MicrosoftConstants.PDF_TO_OTHER);
        //构建文档对象
        Document document = new Document(pdfFile.getInputStream());

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        byte[] resultByte = null;
        //创建临时文件夹->打成压缩包->转成bytes->删除压缩包
        String tempDir=MicrosoftConstants.TO_BYTE_TEMP_PATH+ UUID.fastUUID().toString(true)+File.separator;
        String fileName=FileUtils.getFilePrefix(pdfFile);
        FileUtil.mkdir(tempDir);
        if (isImageType.contains(convertType)){
            ImageDevice imageDevice = getImageDeviceByType(convertType);
            for (int pageCount = 1; pageCount <= document.getPages().size(); pageCount++) {
                Page page = document.getPages().get_Item(pageCount);
                imageDevice.process(page,String.format(tempDir+fileName+"_%d."+convertType,pageCount));
            }
        }else {
            //根据转换类型设置通用的保存选项
            SaveOptions saveOptions = customSaveOptions(convertType,wsNoticeId);
            document.save(outputStream, saveOptions);
            resultByte=outputStream.toByteArray();
        }

        if (isZipType.contains(convertType)){
            //打成压缩包
            File zip = ZipUtil.zip(tempDir);
            resultByte=FileUtil.readBytes(zip);
            //然后把原本的目录和压缩包文件删掉
            FileUtil.del(zip);
        }
        //删掉设置产生的临时空目录
        FileUtil.del(tempDir);
        WebSocketUtil.sendConvertSuc(webSocket,wsNoticeId);
        // 返回生成的`pdf`字节码
        return resultByte;
    }
    /**
     * 根据convertType类型将pdfFile转其它文档
     * 此方法用于保存本地
     * @param pdfFile 待转换的pdf文件MultipartFile格式
     * @param systemPath 生成的文档所在的根目录，还需要拼接其它目录,savePath是最终文档的绝对路径
     * @return 生成的文件路径
     * */
    @SneakyThrows(Exception.class)
    public static String convertType(MultipartFile pdfFile, String systemPath,String convertType,String wsNoticeId) {
        AsposeUtils.checkType(pdfFile,allowedSourceType,convertType,allowedConvertType);
        //加载配置，否则有水印和页数限制
        loadLicense(MicrosoftConstants.PDF_TO_OTHER);
        //构建文档对象
        Document document=new Document(pdfFile.getBytes());
        //设置转换后的文档路径
        String fileName = FileUtils.getFilePrefix(pdfFile); //xxx没有“.”
        String namePath=FileUtils.dateFilePath(null);// /2022/11/12/uuid/
        String zipDir=systemPath+convertType+namePath; // /project/files/convertType/2022/11/12/uuid/
        String savePath=zipDir+fileName+"."+convertType; // /project/files/convertType/2022/11/12/uuid/xxx.convertType
        //save保存的是savePath这个路径下包含的文件名称，如果只有路径没有文件名，就会导致在该路径下生成一个对应的文件但是没有后缀
        if (isImageType.contains(convertType)){
            ImageDevice imageDevice = getImageDeviceByType(convertType);
            for (int pageCount = 1; pageCount <= document.getPages().size(); pageCount++) {
                Page page = document.getPages().get_Item(pageCount);
                imageDevice.process(page,String.format(zipDir+fileName+"_%d."+convertType,pageCount));
            }
        }else {
            //根据转换类型设置通用的保存选项
            document.save(savePath,customSaveOptions(convertType,wsNoticeId));
        }
        if (isZipType.contains(convertType)){
            File zip = ZipUtil.zip(zipDir);
            //压缩以后把压缩包路径赋给返回的路径
            savePath=zip.getPath().replaceAll("\\\\", Matcher.quoteReplacement(File.separator) );
            //然后把原本的目录删掉
            FileUtil.del(zipDir);
        }
        WebSocketUtil.sendConvertSuc(webSocket,wsNoticeId);
        return savePath.substring(systemPath.length()-1);
    }

    /**将文件后缀转为com.aspose.pdf.SaveFormat对应的类型*/
    private static int getSaveFormatByType(String convertType){
        int i;
        switch (convertType){
            case MicrosoftConstants.Doc: i = DocSaveOptions.DocFormat.Doc;break;
            case MicrosoftConstants.Docx: i = DocSaveOptions.DocFormat.DocX;break;
            case MicrosoftConstants.Xps: i = SaveFormat.Xps.getValue();break;
            case MicrosoftConstants.Html: i = SaveFormat.Html.getValue();break;
            case MicrosoftConstants.Xml: i = SaveFormat.Xml.getValue();break;
            case MicrosoftConstants.Tex: i = SaveFormat.TeX.getValue();break;
            case MicrosoftConstants.Svg: i =  SaveFormat.Svg.getValue();break;
            case MicrosoftConstants.MobiXml: i = SaveFormat.MobiXml.getValue();break;
            case MicrosoftConstants.Xlsx: i = SaveFormat.Excel.getValue();break;
            case MicrosoftConstants.Epub: i = SaveFormat.Epub.getValue();break;
            case MicrosoftConstants.Plugin: i = SaveFormat.Plugin.getValue();break;
            case MicrosoftConstants.Pptx: i = SaveFormat.Pptx.getValue();break;
            case MicrosoftConstants.Aps: i = SaveFormat.Aps.getValue();break;
            case MicrosoftConstants.PdfXml: i = SaveFormat.PdfXml.getValue();break;
            default:i= SaveFormat.Pdf.getValue();
        }
        return i;
    }
    private static ImageDevice getImageDeviceByType(String convertImageType){
        ImageDevice device=null;
        switch (convertImageType){
            case MicrosoftConstants.Bmp -> device=new BmpDevice();
            case MicrosoftConstants.Jpeg -> device=new JpegDevice();
            case MicrosoftConstants.Gif -> device=new GifDevice();
            case MicrosoftConstants.Png -> device=new PngDevice();
            case MicrosoftConstants.Emf -> device=new EmfDevice();
        }
        return device;
    }
    //自定义SaveOptions
    private static SaveOptions customSaveOptions(String convertType,String wsNoticeId){
        SaveOptions saveOptions=null;
        switch (convertType){
            //先设置各自的保存选项参数(如果有)
            case MicrosoftConstants.Doc:
            case MicrosoftConstants.Docx:
                saveOptions=customDocSaveOptions(convertType,wsNoticeId);break;
            case MicrosoftConstants.Xps:saveOptions=customXpsSaveOptions();break;
            case MicrosoftConstants.Html:saveOptions=customHtmlSaveOptions(wsNoticeId);break;
            case MicrosoftConstants.Tex:saveOptions=customTexSaveOptions();break;
            case MicrosoftConstants.Svg:saveOptions=customSvgSaveOptions();break;
            case MicrosoftConstants.MobiXml:saveOptions=customMobiXmlSaveOptions();break;
            case MicrosoftConstants.Xlsx:saveOptions=customXlsxSaveOptions();break;
            case MicrosoftConstants.Epub:saveOptions=customEpubSaveOptions();break;
//            case MicrosoftConstants.Plugin:saveOptions=customPluginSaveOptions();break;
            case MicrosoftConstants.Pptx:saveOptions=customPptxSaveOptions(wsNoticeId);break;
            case MicrosoftConstants.Aps:saveOptions=customApsSaveOptions();break;
            case MicrosoftConstants.PdfXml:saveOptions=customPdfXmlSaveOptions();break;
            case MicrosoftConstants.Pdf:saveOptions=customPdfSaveOptions();break;
            default:
        }
        //再设置通用的保存选项参数
//        currencySaveOptions(saveOptions,convertType);
        return saveOptions;
    }

    private static SaveOptions customHtmlSaveOptions(String wsNoticeId) {
        HtmlSaveOptions options = new HtmlSaveOptions();
        //指示在保存期间是否将找到的 SVG 图形（如果有）压缩为 SVGZ 格式的标志
        options.setCompressSvgGraphicsIfAny(true);
        options.setHtmlMarkupGenerationMode(HtmlSaveOptions.HtmlMarkupGenerationModes.WriteAllHtml);
        options.setPartsEmbeddingMode(HtmlSaveOptions.PartsEmbeddingModes.EmbedAllIntoHtml);
        options.setRasterImagesSavingMode(HtmlSaveOptions.RasterImagesSavingModes.AsEmbeddedPartsOfPngPageBackground) ;

        options.setFontSavingMode( HtmlSaveOptions.FontSavingModes.SaveInAllFormats);
        //SplitOnPages=false，则代表所有输入PDF页面的整个HTML不会拆分为不同的HTML页面，而是会放入一个大结果HTML文件中。
        options.setSplitIntoPages(false) ;

        //设置转换进度
        options.setCustomProgressHandler(new UnifiedSaveOptions.ConversionProgressEventHandler() {
            @Override
            public void invoke(UnifiedSaveOptions.ProgressEventHandlerInfo progress) {
                showProgress(progress,wsNoticeId);
            }
        });
        //如果属性 ConvertMarkedContentToLayers 设置为 true，则 PDF 标记的内容（图层）中的所有元素都将放入 HTML div 中，其中“data-pdflayer”属性指定了图层名称。
        // 此图层名称将从 PDF 标记内容的可选属性中提取。如果此属性为 false（默认情况下），则不会从 PDF 标记的内容创建任何图层。
//        options.setConvertMarkedContentToLayers(true);
        //设置图像呈现的分辨率。
//        options.setImageResolution();
        //如果属性 RenderTextAsImage 设置为 true，则源中的文本将成为 HTML 中的图像。
        // 可能可用于使文本不可选择或 HTML 文本无法正确呈现。
//        options.setRenderTextAsImage(true);
        return options;
    }
    private static SaveOptions customPptxSaveOptions(String wsNoticeId) {
        PptxSaveOptions options = new PptxSaveOptions();
        //设置转换进度
        options.setCustomProgressHandler(new UnifiedSaveOptions.ConversionProgressEventHandler() {
            @Override
            public void invoke(UnifiedSaveOptions.ProgressEventHandlerInfo progress) {
                showProgress(progress,wsNoticeId);
            }
        });
        //设置图像分辨率 （dpi）
//        options.setImageResolution();
        //如果设置为 true，则图像与所有其他图形分离
//        options.setSeparateImages(true);
        //如果设置为 true，则所有内容都被识别为图像（每页一个）
//        options.setSlidesAsImages(true);
        return options;
    }
    private static SaveOptions customDocSaveOptions(String convertType,String wsNoticeId) {
        DocSaveOptions options = new DocSaveOptions();
        options.setFormat(getSaveFormatByType(convertType));
        //设置转换进度
        options.setCustomProgressHandler(new UnifiedSaveOptions.ConversionProgressEventHandler() {
            @Override
            public void invoke(UnifiedSaveOptions.ProgressEventHandlerInfo progress) {
                showProgress(progress,wsNoticeId);
            }
        });
        //DocSaveOptions.DocFormat.Doc
        //转换后的图像 X 分辨率。
//        options.setImageResolutionX()
        //设置图像分辨率Y（整数值）
//        options.setImageResolutionY();
        //最大文本行之间的距离（浮点值）此参数用于将文本行分组为段落。确定两个相对文本行之间的距离。以文本行高度的百分之百指定。
//        options.setMaxDistanceBetweenTextLines();
        //定义在内存保存模式下转换时保存临时数据的路径（文件名或目录名称）。
//        options.setMemorySaveModePath();
        return options;
    }

    private static SaveOptions customXpsSaveOptions() {
        XpsSaveOptions options = new XpsSaveOptions();
        //设置转换进度

//        options.setSaveTransparentTexts();
        return options;
    }
    private static SaveOptions customTexSaveOptions() {
        TeXSaveOptions options = new TeXSaveOptions();

        //在转换过程中保存临时图像的方法
//        options.setOutDirectoryPath();
        return options;
    }

    private static SaveOptions customSvgSaveOptions() {
        SvgSaveOptions options = new SvgSaveOptions();
        // Do  compress SVG image to Zip archive
        return options;
    }

    private static SaveOptions customMobiXmlSaveOptions() {
        MobiXmlSaveOptions options = new MobiXmlSaveOptions();

        return options;
    }

    private static SaveOptions customXlsxSaveOptions() {
        ExcelSaveOptions options = new ExcelSaveOptions();
        return options;
    }

    private static SaveOptions customEpubSaveOptions() {
        EpubSaveOptions options = new EpubSaveOptions();
        // Specify the layout for contents
        return options;
    }

    private static SaveOptions customApsSaveOptions() {
        ApsSaveOptions options = new ApsSaveOptions();
        //此属性打开了使用 OCR 子图层提取 PDF 文档的图像或文本的功能。默认值 == 假。值：真实文本将被提取到结果文档中;否则，假.
//        options.setExtractOcrSublayerOnly()
        //

        return options;
    }

    private static SaveOptions customPdfXmlSaveOptions() {
        PdfXmlSaveOptions options = new PdfXmlSaveOptions();

        return options;
    }

    private static SaveOptions customPdfSaveOptions() {
        PdfSaveOptions options = new PdfSaveOptions();

        return options;
    }
    /**
     *获取文档转换进度
     * */
    public static void showProgress(UnifiedSaveOptions.ProgressEventHandlerInfo eventInfo,String wsNoticeId){
        WebsocketResult ws = new WebsocketResult();
        ws.setMsgType(WebsocketConstants.CONVERT_PROGRESS);
        //EventType：0总转换进度，1、源页面分析进度，2、结果页面已创建进度，3、结果页面已保存进度
        switch (eventInfo.getEventType()){
            case 0 -> {
                ws.setMessage(eventInfo.getValue());
                webSocket.sendMessage(wsNoticeId,ws);
            }
//            case 1->
//                    webSocket.sendMessage(String.format("Source page %d of %d analyzed.", eventInfo.Value, eventInfo.MaxValue));
//            case 2->
//                    webSocket.sendMessage(String.format("Result page's %d of %d layout created.",  eventInfo.Value, eventInfo.MaxValue));
//            case 3->
//                    webSocket.sendMessage(String.format("Result page %d of %d exported.",eventInfo.Value, eventInfo.MaxValue));

        }
    }

public static void dealFonts(){
    CustomFontSubstitutionBase subst = new CustomFontSubstitutionBase() {
        public boolean trySubstitute(CustomFontSubstitutionBase.OriginalFontSpecification originalFontSpecification, /*out*/ com.aspose.pdf.Font[] substitutionFont) {

            System.out.println("Warning: Font " + originalFontSpecification.getOriginalFontName() + " is try to be substituted");

            if (!originalFontSpecification.isEmbedded()) {
                try {
                    Font fSrcInRepository = FontRepository.findFont(originalFontSpecification.getOriginalFontName());
                    if (fSrcInRepository==null){

                        System.out.println("font not found");
                        return false;
//                            substitutionFont[0] = FontRepository.openFont("/path_to_font/times.ttf");//or use default specified font
//                            System.out.println("Font " + originalFontSpecification.getOriginalFontName()
//                                    + " was substituted with another font -> " + substitutionFont[0].getFontName());
                    }else {
                        substitutionFont[0] = fSrcInRepository;
                    }
                } catch (FontNotFoundException e) {
                    System.out.println("font not found");
                    return false;
//                        substitutionFont[0] = FontRepository.openFont("/path_to_font/times.ttf");//or use default specified font
//                        System.out.println("Font " + originalFontSpecification.getOriginalFontName()
//                                + " was substituted with another font -> " + substitutionFont[0].getFontName());
                }
                return true;
            } else {
                return super.trySubstitute(originalFontSpecification, substitutionFont);
            }

        }};
    FontRepository.getSubstitutions().add(subst);
}
}
