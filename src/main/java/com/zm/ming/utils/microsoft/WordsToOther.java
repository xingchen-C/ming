package com.zm.ming.utils.microsoft;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.ZipUtil;
import com.aspose.slides.ISlide;
import com.aspose.words.*;
import com.zm.ming.domain.constant.MicrosoftConstants;
import com.zm.ming.handler.websocket.WebSocketImpl;
import com.zm.ming.utils.FileUtils;
import com.zm.ming.utils.UUID;
import com.zm.ming.utils.WebSocketUtil;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;

/**
 * @author lzm
 *  words转文档工具类
 *  已破解，转换前无需加载license
 * */

@Component
@Slf4j
public class WordsToOther {
    private static WordsToOther instance;
    @PostConstruct
    public void init() {
        instance = this;
    }
    private static WebSocketImpl webSocket;
    /**
     *使用构造器注入websocket
     * */
    public WordsToOther(WebSocketImpl webSocket){
        WordsToOther.webSocket=webSocket;
    }
    /**转换完成后产生多个文件需要进行打包的有：md\html\图片*/
    public static final List<String> isZipType=new ArrayList<>(8);
    /**当前支持转的类型*/
    public static final List<String> isImageType=new ArrayList<>(7);
    public static final List<String> allowedConvertType=new ArrayList<>(26);
    static {
        //文档
        allowedConvertType.add(MicrosoftConstants.Doc);
        allowedConvertType.add(MicrosoftConstants.Dot);
        allowedConvertType.add(MicrosoftConstants.Docx);
        allowedConvertType.add(MicrosoftConstants.Docm);
        allowedConvertType.add(MicrosoftConstants.Dotx);
        allowedConvertType.add(MicrosoftConstants.Dotm);
        allowedConvertType.add(MicrosoftConstants.Rtf);
        allowedConvertType.add(MicrosoftConstants.Pdf);
        allowedConvertType.add(MicrosoftConstants.Xps);
        allowedConvertType.add(MicrosoftConstants.OPEN_XPS);
        allowedConvertType.add(MicrosoftConstants.Ps);
        allowedConvertType.add(MicrosoftConstants.Pcl);
        allowedConvertType.add(MicrosoftConstants.Mhtml);
        allowedConvertType.add(MicrosoftConstants.Epub);
        allowedConvertType.add(MicrosoftConstants.Odt);
        allowedConvertType.add(MicrosoftConstants.Ott);
        allowedConvertType.add(MicrosoftConstants.Text);
        allowedConvertType.add(MicrosoftConstants.Html);
        //图形
        isImageType.add(MicrosoftConstants.Svg);
        isImageType.add(MicrosoftConstants.Tiff);
        isImageType.add(MicrosoftConstants.Png);
        isImageType.add(MicrosoftConstants.Bmp);
        isImageType.add(MicrosoftConstants.Jpeg);
        isImageType.add(MicrosoftConstants.Gif);
        isImageType.add(MicrosoftConstants.Emf);
        //如果工具类新增加转换的类型，则在这里添加对应类型...
        //需打包的类型(要打包是因为会生成多个文件)
        isZipType.addAll(isImageType);
        //如果转完html没有设置setExportImagesAsBase64把图片插入为base64的话，那么就需要打包。
//        isZipType.add(MicrosoftConstants.Html);
        isZipType.add(MicrosoftConstants.MarkDown);
        //合并
        allowedConvertType.addAll(isZipType);

    }
    /**当前支持上传文件的类型*/

    public static final List<String> allowedSourceType=new ArrayList<>(11);
    static {
        allowedSourceType.add(MicrosoftConstants.Doc);
        allowedSourceType.add(MicrosoftConstants.Docx);
        allowedSourceType.add(MicrosoftConstants.Docm);
        allowedSourceType.add(MicrosoftConstants.Dotm);
        allowedSourceType.add(MicrosoftConstants.Dotx);
        allowedSourceType.add(MicrosoftConstants.Dot);
        allowedSourceType.add(MicrosoftConstants.Odt);
        allowedSourceType.add(MicrosoftConstants.Ott);
        allowedSourceType.add(MicrosoftConstants.Text);
        allowedSourceType.add(MicrosoftConstants.Html);
        allowedSourceType.add(MicrosoftConstants.Mhtml);
        allowedSourceType.add(MicrosoftConstants.MarkDown);
    }

    /**
     * 根据convertType类型将wordFile转其它文档的字节码
     * 此方法用于非保存本地
     * @param wordFile:    word文件MultipartFile格式
     * @param convertType: 需转成的文件类型
     * @return 生成的byte[]
     */

    @SneakyThrows(Exception.class)
    public static byte[] convertTypeBytes(MultipartFile wordFile,String convertType,String wsNoticeId) {
        //校验合法类型
        AsposeUtils.checkType(wordFile,allowedSourceType,convertType,allowedConvertType);
        //linux下加载window字体
        AsposeUtils.loadLinuxFonts(MicrosoftConstants.WORDS_TO_OTHER);
        //构建文档对象
        Document document = new Document(wordFile.getInputStream());
        //根据转换类型设置通用的保存选项
        SaveOptions saveOptions = customSaveOptions(convertType);

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        byte[] resultByte = null;
        //创建临时文件夹保存为压缩包再转bytes
        String tempDir=MicrosoftConstants.TO_BYTE_TEMP_PATH+ UUID.fastUUID().toString(true)+File.separator;
        String fileName=FileUtils.getFilePrefix(wordFile);
        FileUtil.mkdir(tempDir);
        if (isImageType.contains(convertType)){
            //如果属于转图片，则转成缩略图
            for (int page = 0; page < document.getPageCount(); page++){
                Document extractedPage = document.extractPages(page, 1);
                extractedPage.save(String.format(tempDir+fileName+"_%d."+convertType, page + 1),saveOptions);
            }
        }else{
            //注意一点，不管是任何类型excel、words、ppt、pdf都好，对于使用转换保存到本地的方式会产生多个文件时，用来保存为流时是不对的，
            // 要么文件打不开要么报错，所以对于这种情况都要使用保存到本地的方式然后打成压缩包再转成流
            if (!isImageType.contains(convertType)&&isZipType.contains(convertType)){
                String filePath=tempDir+fileName+ "."+convertType;
                document.save(filePath,saveOptions);
            }else {
                document.save(outputStream,saveOptions);
                resultByte= outputStream.toByteArray();
            }
        }
        document.cleanup();
        // 返回生成文件的字节码
        if (isZipType.contains(convertType)){
            //打成压缩包
            File zip = ZipUtil.zip(tempDir);
            resultByte=FileUtil.readBytes(zip);
            //然后把原本的目录和压缩包文件删掉
            FileUtil.del(zip);
        }
        //最后删除临时文件夹
        FileUtil.del(tempDir);
        WebSocketUtil.sendConvertSuc(webSocket,wsNoticeId);
        return resultByte;
    }

    /**
     * 根据convertType类型将wordFile转其它文档
     * 此方法用于保存本地
     * @param wordFile:    word文件MultipartFile格式
     * @param systemPath 生成的文档所在的根目录，还需要拼接其它目录,savePath是最终文档的绝对路径
     * @param convertType 要转换成的文档类型
     * @return 生成的文件路径
     */
    @SneakyThrows(Exception.class)
    public static String convertType(MultipartFile wordFile, String systemPath,String convertType,String wsNoticeId) {
        //校验合法类型
        AsposeUtils.checkType(wordFile,allowedSourceType,convertType,allowedConvertType);
        //构建文档对象
        Document document = new Document(wordFile.getInputStream());
        String fileName = FileUtils.getFilePrefix(wordFile);
        String namePath=FileUtils.dateFilePath(null);
        String zipDir=systemPath+convertType+namePath;
        String savePath=zipDir+fileName;
        //根据转换类型设置通用的保存选项
        SaveOptions saveOptions = customSaveOptions(convertType);
        //todo 需优化：md转其它时，图片较小
        if (isImageType.contains(convertType)){
            //说明转的是图片(多张)
            for (int page = 0; page < document.getPageCount(); page++){
                Document extractedPage = document.extractPages(page, 1);
                extractedPage.save(String.format(savePath+"_%d."+convertType, page + 1),saveOptions);
            }
        }else {
            //save保存的是savePath这个路径下包含的文件名称，如果只有路径没有文件名，就会导致在该路径下生成一个对应的文件但是没有后缀
            savePath=savePath+ "."+convertType;
            document.save(savePath,saveOptions);
        }
        document.cleanup();
        //需要打包成zip的则进行打包(因为部分类型转完有多个文档，而这里是统一返回一个文档的路径的，所以采用打包的方式)
        if (isZipType.contains(convertType)){
            File zip = ZipUtil.zip(zipDir);
            savePath=zip.getPath().replaceAll("\\\\", Matcher.quoteReplacement(File.separator));
            //然后把原本的目录删掉
            FileUtil.del(zipDir);
        }
        WebSocketUtil.sendConvertSuc(webSocket,wsNoticeId);
        return savePath.split(systemPath)[1];
    }
    /**将文件后缀转为com.aspose.words.SaveFormat对应的类型(版本不同，可能部分数值会有点偏差！)*/
    private static   int getSaveFormatByType(String convertType){
        int i;
        switch (convertType){
            case MicrosoftConstants.Doc: i = 10;break;
            case MicrosoftConstants.Dot: i = 11;break;
            case MicrosoftConstants.Docx: i = 20;break;
            case MicrosoftConstants.Docm: i = 21;break;
            case MicrosoftConstants.Dotx: i = 22;break;
            case MicrosoftConstants.Dotm: i = 23;break;

            case MicrosoftConstants.Rtf: i = 30;break;
            case MicrosoftConstants.Pdf: i = 40;break;
            case MicrosoftConstants.Xps: i = 41;break;
            case MicrosoftConstants.Svg: i = 44;break;
            case MicrosoftConstants.OPEN_XPS: i = 46;break;
            case MicrosoftConstants.Ps: i = 47;break;
            case MicrosoftConstants.Pcl: i = 48;break;
            case MicrosoftConstants.Html: i = 50;break;
            case MicrosoftConstants.Mhtml: i = 51;break;
            case MicrosoftConstants.Epub: i = 52;break;
            case MicrosoftConstants.Odt: i = 60;break;
            case MicrosoftConstants.Ott: i = 61;break;
            case MicrosoftConstants.Text: i = 70;break;
            case MicrosoftConstants.MarkDown: i = 73;break;

            case MicrosoftConstants.Tiff: i = 100;break;
            case MicrosoftConstants.Png: i = 101;break;
            case MicrosoftConstants.Bmp: i = 102;break;
            case MicrosoftConstants.Emf: i = 103;break;
            case MicrosoftConstants.Jpeg: i = 104;break;
            case MicrosoftConstants.Gif: i = 105;break;
            default:i=SaveFormat.UNKNOWN;
        }
        return i;
    }

    //自定义SaveOptions
    private static SaveOptions customSaveOptions(String convertType){
        SaveOptions saveOptions=null;
        switch (convertType){
            //先设置各自的保存选项参数(如果有)
            case MicrosoftConstants.Html:saveOptions=customHtmlSaveOptions();break;
            default:saveOptions=FixedPageSaveOptions.createSaveOptions(getSaveFormatByType(convertType));
        }
        //再设置通用的保存选项参数
        currencySaveOptions(saveOptions,convertType);
        return saveOptions;
    }

    /**
     * 设置通用的保存选项参数
     * */
    private static SaveOptions currencySaveOptions(SaveOptions saveOptions,String convertType){
        //在文档中嵌入 TrueType 字体时是否允许嵌入带有 PostScript 轮廓的字体。默认值为false。
        saveOptions.setAllowEmbeddingPostScriptFonts(true);
        //确定如何呈现 3D 效果。默认值为Dml3DEffectsRenderingMode.BASIC。
//        saveOptions.setDml3DEffectsRenderingMode(Dml3DEffectsRenderingMode.ADVANCED);
//        //确定如何呈现 DrawingML 效果。默认值为DmlEffectsRenderingMode.SIMPLIFIED。
//        saveOptions.setDmlEffectsRenderingMode(DmlEffectsRenderingMode.FINE);
//        //确定如何呈现 DrawingML 形状。默认值为DmlRenderingMode.FALLBACK。
//        saveOptions.setDmlRenderingMode(DmlRenderingMode.DRAWING_ML);
        //如果为 true，则在适用的情况下使 HTML、MHTML、EPUB、WORDML、RTF、DOCX 和 ODT输出漂亮的格式。默认false
        saveOptions.setPrettyFormat(true);
        //确定是否使用高质量（保存为图像时）,默认false。
//        saveOptions.setUseHighQualityRendering(true);
        /*当Aspose.Words保存文档时，它需要创建临时的内部结构。默认情况下，这些内部结构是在内存中创建的，并且在保存文档时内存使用量会在短时间内达到峰值。保存完成后，垃圾回收器将释放并回收内存。
        如果要保存一个非常大的文档（数千页）和/或同时处理多个文档，则保存期间的内存峰值可能足以导致系统抛出java.lang.IndexOutOfBoundsException。
        使用 getTempFolder（）/setTempFolder（java.lang.String） 指定临时文件夹将导致 Aspose.Words 将内部结构保留在临时文件而不是内存中。它减少了保存期间的内存使用量，但会降低保存性能。
        该文件夹必须存在且可写，否则将引发异常。Aspose.Words 会在保存完成后自动删除所有临时文件*/
//        saveOptions.setTempFolder(FileUtil.createTempFile(convertType,false).getPath());
        //确定在保存文档之前是否应执行内存优化。此属性的默认值为false。true耗时长但是内存消耗小
        saveOptions.setMemoryOptimization(true);
        //设置使用抗锯齿，默认false，true是质量更高
//        saveOptions.setUseAntiAliasing(true);
        return saveOptions;
    }

    //自定义转html的保存参数
    private static   HtmlSaveOptions customHtmlSaveOptions(){
        HtmlSaveOptions saveOptions=new HtmlSaveOptions();
        //设置html里面的图片为base64，而非另创建的文件
        saveOptions.setExportImagesAsBase64(true);

        return saveOptions;
    }
//        private static DocSaveOptions saveDocOptions(){
//        DocSaveOptions saveOptions=new DocSaveOptions();
//        return saveOptions;
//    }
//    private static MarkdownSaveOptions saveMdOptions(){
//        MarkdownSaveOptions saveOptions=new MarkdownSaveOptions();
//        return saveOptions;
//    }
//    private static ImageSaveOptions saveImageOptions(int saveFormat){
//        ImageSaveOptions saveOptions=new ImageSaveOptions(saveFormat);
//        /*设置生成的图像的水平分辨率（以每英寸点数为单位）。默认值为 96。
//        此属性仅在保存为光栅图像格式时有效，并影响输出大小（以像素为单位）*/
//        saveOptions.setHorizontalResolution(105);
//        //设置生成的图像的缩放系数。默认值为 1.0。该值必须大于 0。
//        saveOptions.setScale(10);
//        return saveOptions;
//    }
//    private static PdfSaveOptions savePdfOptions(){
//        PdfSaveOptions saveOptions=new PdfSaveOptions();
//        return saveOptions;
//    }
//    private static TxtSaveOptions saveTxtOptions(){
//        TxtSaveOptions saveOptions=new TxtSaveOptions();
//        return saveOptions;
//    }
//    private static XpsSaveOptions saveXpsOptions(){
//        XpsSaveOptions saveOptions=new XpsSaveOptions();
//        return saveOptions;
//    }
//    private static SvgSaveOptions saveSvgOptions(){
//        SvgSaveOptions saveOptions=new SvgSaveOptions();
//        return saveOptions;
//    }
//    private static OdtSaveOptions saveOdtOptions(){
//        OdtSaveOptions saveOptions=new OdtSaveOptions();
//        return saveOptions;
//    }
//    private static RtfSaveOptions saveRtfOptions(){
//        RtfSaveOptions saveOptions=new RtfSaveOptions();
//        return saveOptions;
//    }
//    private static OoxmlSaveOptions saveOoxmlOptions(){
//        OoxmlSaveOptions saveOptions=new OoxmlSaveOptions();
//        return saveOptions;
//    }

}
