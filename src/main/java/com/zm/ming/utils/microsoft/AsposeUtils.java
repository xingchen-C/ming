package com.zm.ming.utils.microsoft;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.resource.ResourceUtil;
import com.aspose.cells.FontConfigs;
import com.aspose.pdf.*;

import com.aspose.slides.FontSources;
import com.aspose.slides.FontsLoader;
import com.aspose.slides.IFontSources;
import com.zm.ming.domain.bo.microsoft.ConvertFileBo;
import com.zm.ming.domain.bo.microsoft.ConvertResult;
import com.zm.ming.domain.constant.MicrosoftConstants;
import com.zm.ming.utils.Assert;
import com.zm.ming.utils.FileUtils;
import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import java.io.*;
import java.util.Arrays;
import java.util.List;

/**
 * @author Lzm
 * @description Aspose使用辅助工具类
 * @date 2022/11/12 12:10
 */
public class AsposeUtils {
    private static final Logger log= LoggerFactory.getLogger(AsposeUtils.class);
    public static final void checkType(MultipartFile sourceFile, List<String> sourceTypeList, String convertType, List<String> convertTypeList){
        String sourceFileType = FileUtils.getFileType(sourceFile);
        Assert.isTrue(!convertType.equals(sourceFileType),"上传文档与待转换的格式相同！");
        Assert.isTrue(!sourceFile.isEmpty()&&sourceTypeList.contains(sourceFileType),"待转换的文件类型不支持！");
        Assert.isTrue(convertTypeList.contains(convertType),"不支持转换为："+convertType+" 类型");
    }
    /**
     * 读取resources下的验证配置文件
     * */
    @SneakyThrows
    protected static void loadLicense(String who){
        InputStream fis = ResourceUtil.getStream("license.xml");
        if (MicrosoftConstants.PPTX_TO_OTHER.equals(who)){
            com.aspose.slides.License license=new com.aspose.slides.License();
            license.setLicense(fis);
        }else if (MicrosoftConstants.EXCEL_TO_OTHER.equals(who)){
            com.aspose.cells.License license=new com.aspose.cells.License();
            license.setLicense(fis);
        }else if (MicrosoftConstants.PDF_TO_OTHER.equals(who)){
            com.aspose.pdf.License license=new com.aspose.pdf.License();
            license.setLicense(fis);
        }else if (MicrosoftConstants.CAD_TO_OTHER.equals(who)){
            com.aspose.cad.License license=new com.aspose.cad.License();
            license.setLicense(fis);
        }
    }
    public static boolean isWindowsOs(){
        return System.getProperty("os.name").toLowerCase().contains("window");
    }

    /**
     * linux环境下加载字体，解决linux环境下文档转成pdf要么乱码要么报错： Cannot find any fonts installed on the system.
     * */
    public static void loadLinuxFonts(String who){
//        if (1==1){
//            return;
//        }
        //获取当前java运行环境
        String os = System.getProperty("os.name");
        log.info("当前操作系统环境："+os);
        if (StringUtils.isNotBlank(os)&& os.contains("Linux")) {
            //window字体存放到linux下所在的目录：
            String fontsPath = "/usr/share/fonts/winFonts/";
            //1、先将window环境下的C:\Windows\Fonts\中所有字体都上传到linux下在目录/usr/share/fonts/下（或随便建一个目录来存放也行）
            //2、上传之后在liunx系统下更改这些字体库的权限：sudo chmod 755 /usr/share/fonts/*
            //3、安装一下字体工具：yum install -y fontconfig mkfontscale
            //4、进入到你存放在linux字体的目录：cd /usr/share/fonts/
            //5、建立字体索引信息，更新字体缓存执行命令(如果提示找不到命令安装相应提醒安装即可重新执行该命令)： mkfontscale && mkfontdir && fc-cache -fv
            //最后一步，要在你的aspose转pdf工具类里，执行该方法
            if (MicrosoftConstants.PPTX_TO_OTHER.equals(who)) {
//                com.aspose.slides.FontSources fs=new FontSources();
//                fs.setFontFolders(new String[]{fontsPath});

                log.info("本地字体路径："+Arrays.toString( FontsLoader.getFontFolders()));
                FontsLoader.loadExternalFonts(new String[]{fontsPath});
                log.info("设置后本地字体路径："+Arrays.toString( FontsLoader.getFontFolders()));
            } else if (MicrosoftConstants.EXCEL_TO_OTHER.equals(who)) {
                log.info("excel设置前的 FontSources 路径："+Arrays.toString(FontConfigs.getFontSources()) );
                com.aspose.cells.FontConfigs.setFontFolder(fontsPath, true);
                log.info("excel设置后的 FontSources 路径："+Arrays.toString(FontConfigs.getFontSources()) );
            } else if (MicrosoftConstants.PDF_TO_OTHER.equals(who)) {
                log.info("添加前本地字体路径："+  FontRepository.getLocalFontPaths());
//                List<String> localFontPaths = FontRepository.getLocalFontPaths();
//                localFontPaths.add(fontsPath);
//                FontRepository.setLocalFontPaths(localFontPaths);
//                log.info("设置后本地字体路径："+  FontRepository.getLocalFontPaths());
//                FontRepository.addLocalFontPath(fontsPath);
//                log.info("添加后本地字体路径："+  FontRepository.getLocalFontPaths());
//                log.info("字体源存储配置的状态："+FontRepository.isThreadStaticConfigEnabled());

//                com.aspose.pdf.FolderFontSource fs=new FolderFontSource(fontsPath);
//                log.info("FolderFontSource获取字体目路径录："+fs.getFolderPath());

//                com.aspose.pdf.FileFontSource fs=new FileFontSource(fontsPath);
//                log.info("FileFontSource获取字体目路径录："+fs.getFilePath());
//                com.aspose.pdf.FontEmbeddingOptions fe=new FontEmbeddingOptions();
//                fe.setUseDefaultSubstitution();

            } else if (MicrosoftConstants.WORDS_TO_OTHER.equals(who)) {
//                com.aspose.words.FontSettings fontSettings = com.aspose.words.FontSettings.getDefaultInstance();
//                fontSettings.setFontsFolder(fontsPath, true);
            }
        }
    }
}
