package com.zm.ming.utils.microsoft;

/**
 * @author Lzm
 * @description TODO AsposePdfJarCrack
 * @date 2022/11/11 18:03
 */
import javassist.*;
import javassist.bytecode.MethodInfo;

import java.io.*;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.JarOutputStream;

/**
 * @date 2022-05-16
 * @user lzm
 * aspose-pdf的jar包破解类，用来生成破解后相同的jar包
 */
public class AsposePdfJarCrack {

    private static final String Desktop="C:\\Users\\PC\\Desktop\\";
    public static void main(String[] args) throws Exception {

        //====》破解aspose-pdf的jar，因为默认只能转4页还有水印。下面的jarPath是你aspose-pdf所在的本地maven库地址
        /*修改你本机的aspose-pdf-21.8.jar路径，然后运行主方法，破解成功后，
        会再同级文件夹下生成一个aspose-pdf-21.8.cracked.jar包，用这个包替换原来的aspose-pdf-21.8.jar包即可。*/
        //crackAsposePdfJar("C:\\Users\\PC\\.m2\\repository\\com\\aspose\\aspose-pdf\\21.8\\aspose-pdf-21.8.jar");

        //====》破解AsposeWords,添加license
//        crackAsposeWordsJarAddLicense2("D:\\外部jar包\\文档操作工具包\\aspose-words-21.1.0-jdk17.jar");
        //====》破解AsposeSlides21-8-jdk16,添加license
//        modifyPptJar("D:\\外部jar包\\文档操作工具包\\aspose-slides-21.8-jdk16.jar");
//        crackAsposeCells("D:\\外部jar包\\文档操作工具包\\aspose破解版\\需要引入license.xml在转之前加载这个xml\\aspose-cells-21.8.jar");
//        crackAsposeCells22_9V("D:\\home\\project\\ming\\lib\\aspose-cells-22.9.jar");
//        crackAsposeWords22_9V("D:\\home\\project\\ming\\lib\\aspose-words-22.9-jdk17.jar");
    }
    private static void viewSetLicenseCode()throws Exception{
        com.aspose.pdf.License pdfLicense=new com.aspose.pdf.License();
        com.aspose.words.License wordsLicense=new com.aspose.words.License();
        com.aspose.slides.License slidesLicense = new com.aspose.slides.License();
        com.aspose.cells.License cellsLicense = new com.aspose.cells.License();
        //请点击这里的setLicense方法进入查看源码，不同版本对words 的破解方法名不一样，但道理差不多
        pdfLicense.setLicense("");
        wordsLicense.setLicense("");
        slidesLicense.setLicense("");
        cellsLicense.setLicense("");
    }
    /**破解AsposeWords-21.11-jdk17版方法1这种修改license可以成功，但是在代码进行转换的时候要手动调用一次license
     * InputStream is = new FileInputStream(new File("license.xml"));
     * License license = new License();
     * license.setLicense(is);
     * 后续转换代码。。。。
     * */
    private static void crackAsposeWordsJarAddLicense1(String jarName){
        try {
            //这一步是完整的jar包路径,选择自己解压的jar目录
            ClassPool.getDefault().insertClassPath(jarName);
            //获取指定的class文件对象
            CtClass zzZJJClass = ClassPool.getDefault().getCtClass("com.aspose.words.zzXDb");
            //从class对象中解析获取指定的方法
            CtMethod[] methodA = zzZJJClass.getDeclaredMethods("zzY0J");
            //遍历重载的方法
            for (CtMethod ctMethod : methodA) {
                CtClass[] ps = ctMethod.getParameterTypes();
                if (ctMethod.getName().equals("zzY0J")) {
                    System.out.println("ps[0].getName==" + ps[0].getName());
                    //替换指定方法的方法体
                    ctMethod.setBody("{this.zzZ3l = new java.util.Date(Long.MAX_VALUE);this.zzWSL = com.aspose.words.zzYeQ.zzXgr;zzWiV = this;}");
                }
            }
            //这一步就是将破译完的代码放在桌面上
            zzZJJClass.writeFile(Desktop);

            //获取指定的class文件对象
            CtClass zzZJJClassB = ClassPool.getDefault().getCtClass("com.aspose.words.zzYKk");
            //从class对象中解析获取指定的方法
            CtMethod methodB = zzZJJClassB.getDeclaredMethod("zzWy3");
            //替换指定方法的方法体
            methodB.setBody("{return 256;}");
            //这一步就是将破译完的代码放在桌面上
            zzZJJClassB.writeFile(Desktop);
        } catch (Exception e) {
            System.out.println("错误==" + e);
        }
    }

    /**破解AsposeWords-21.1.0-jdk17方法2这种修改license可以成功，但是在代码进行转换的时候要手动调用一次license
     * InputStream is = new FileInputStream(new File("license.xml"));
     * License license = new License();
     * license.setLicense(is);
     * 后续转换代码。。。。
     * */
    private static void crackAsposeWordsJarAddLicense2(String jarName){
        try {
            //这一步是完整的jar包路径,选择自己解压的jar目录
            ClassPool.getDefault().insertClassPath(jarName);
            CtClass ctClass = ClassPool.getDefault().getCtClass("com.aspose.words.zzZE0");
            CtMethod zzZ4h = ctClass.getDeclaredMethod("zzZ4h");
            CtMethod zzZ4g = ctClass.getDeclaredMethod("zzZ4g");
            zzZ4h.setBody("{return 1;}");
            zzZ4g.setBody("{return 1;}");
            //反编译后的“zzZE0.class"保存目录[xxx/com/aspose/words/zzZE0.class]
            ctClass.writeFile(Desktop);
            File file = new File(jarName);
            ctClass.writeFile(file.getParent());
            disposeJar(jarName, file.getParent() + "/com/aspose/words/zzXDb.class");
        } catch (Exception e) {
            System.out.println("错误==" + e);
        }
    }

    /**破解asposeCells-21.8*/
    public static void crackAsposeCells(String JarPath) throws NotFoundException,CannotCompileException, IOException {
        // 这个是得到反编译的池
        ClassPool pool = ClassPool.getDefault();

        // 取得需要反编译的jar文件，设定路径
        pool.insertClassPath(JarPath);

        CtClass cc_License = pool.get("com.aspose.cells.License");

        CtMethod method_isLicenseSet = cc_License.getDeclaredMethod("isLicenseSet");
        method_isLicenseSet.setBody("return true;");

        CtClass cc_License2 = pool.get("com.aspose.cells.License");
        CtMethod method_setLicense = cc_License2.getDeclaredMethod("setLicense");
        method_setLicense.setBody("{    a = new com.aspose.cells.License();\n" +
                "    com.aspose.cells.zbkl.a();}");

        CtMethod methodL = cc_License.getDeclaredMethod("l");
        methodL.setBody("return new java.util.Date(Long.MAX_VALUE);");


        cc_License.writeFile(Desktop);
    }
    /**破解asposeCells-22.9*/
    public static void crackAsposeCells22_9V(String JarPath) throws NotFoundException,CannotCompileException, IOException {
        // 这个是得到反编译的池
        ClassPool pool = new ClassPool();
        // 取得需要反编译的jar文件，设定路径
        pool.insertClassPath(JarPath);
        //添加下面这句解决[source error] javassist.NotFoundException: org.w3c.dom.Document异常
        pool.appendClassPath(new LoaderClassPath(new ClassLoader() {
            @Override
            public Class<?> loadClass(String name) throws ClassNotFoundException {
                return super.loadClass(name);
            }
        })); // 追加
        //开始...
        CtClass cc_License = pool.get("com.aspose.cells.License");

        CtMethod method_isLicenseSet = cc_License.getDeclaredMethod("isLicenseSet");
        method_isLicenseSet.setBody("return true;");

        CtClass t2j = pool.get("com.aspose.cells.t2j");
        //从class对象中解析获取所有方法
        CtMethod[] methodA = t2j.getDeclaredMethods("a");
        methodA[1].setBody("{a = this;com.aspose.cells.y06.a();}");

        t2j.writeFile(Desktop);
        cc_License.writeFile(Desktop);
    }
    public static void crackAsposePdf22_9V(String jarPath) throws NotFoundException, CannotCompileException, IOException {
            // 这个是得到反编译的池
            ClassPool pool = new ClassPool();
            // 取得需要反编译的jar文件，设定路径
            pool.insertClassPath(jarPath);
            //添加下面这句解决[source error] javassist.NotFoundException: org.w3c.dom.Document异常
            pool.appendSystemPath(); // 追加
            pool.appendClassPath(new LoaderClassPath(new ClassLoader() {
                @Override
                public Class<?> loadClass(String name) throws ClassNotFoundException {
                    return super.loadClass(name);
                }
            })); // 追加
            //获取指定的class文件对象
            CtClass zzZJJClass = pool.get("com.aspose.pdf.l10n");
            //从class对象中解析获取所有方法
            CtMethod[] methodA = zzZJJClass.getDeclaredMethods("lI");
            methodA[1].setBody("{this.l0t = com.aspose.pdf.l10f.lf;com.aspose.pdf.internal.l132y.lf.lI();lI(this);lI = true;}");
            //这一步就是将破译完的代码放在桌面上
            zzZJJClass.writeFile(Desktop);
    }
    public static void crackAsposePpt22_9V(String jarPath) {
        try {
            // 这个是得到反编译的池
            ClassPool pool = new ClassPool();
            // 取得需要反编译的jar文件，设定路径
            pool.insertClassPath(jarPath);
            //添加下面这句解决[source error] javassist.NotFoundException: org.w3c.dom.Document异常
            pool.appendClassPath(new LoaderClassPath(new ClassLoader() {
                @Override
                public Class<?> loadClass(String name) throws ClassNotFoundException {
                    return super.loadClass(name);
                }
            })); // 追加
            CtClass zzZJJClass = pool.getCtClass("com.aspose.slides.internal.oh.return");
            CtMethod[] methodA = zzZJJClass.getDeclaredMethods();
            for (CtMethod ctMethod : methodA) {
                CtClass[] ps = ctMethod.getParameterTypes();
                if (ps.length == 3 && ctMethod.getName().equals("do")) {
                    System.out.println("ps[0].getName==" + ps[0].getName());
                    ctMethod.setBody("{}");
                }
            }
            //这一步就是将破译完的代码放在桌面上
            zzZJJClass.writeFile(Desktop);
        } catch (Exception e) {
            System.out.println("错误==" + e);
        }
    }
    public static void crackAsposeWords22_9V(String jarPath) {
        try {
            // 这个是得到反编译的池
            ClassPool pool = new ClassPool();
            // 取得需要反编译的jar文件，设定路径
            pool.insertClassPath(jarPath);
            //添加下面这句解决[source error] javassist.NotFoundException: org.w3c.dom.Document异常
            pool.appendClassPath(new LoaderClassPath(new ClassLoader() {
                @Override
                public Class<?> loadClass(String name) throws ClassNotFoundException {
                    return super.loadClass(name);
                }
            })); // 追加
            CtClass zzZJJClass = pool.getCtClass("com.aspose.words.zzWgI");
            CtMethod methodA = zzZJJClass.getDeclaredMethod("zzWz");
            methodA.setBody("{if (zzgh == 0L) {zzgh ^= zzWGQ;} return com.aspose.words.zzYRi.zzX7L;}");
            //这一步就是将破译完的代码放在桌面上
            zzZJJClass.writeFile(Desktop);
        } catch (Exception e) {
            System.out.println("错误==" + e);
        }
    }
    /**
     * 修改slides21.8版本破解校验
     */
    private static void modifyPptJar(String jarName) {
        try {
            //这一步是完整的jar包路径,选择自己解压的jar目录
            ClassPool.getDefault().insertClassPath(jarName);
            CtClass zzZJJClass = ClassPool.getDefault().getCtClass("com.aspose.slides.internal.of.public");
            CtMethod[] methodA = zzZJJClass.getDeclaredMethods();
            for (CtMethod ctMethod : methodA) {
                CtClass[] ps = ctMethod.getParameterTypes();
                if (ps.length == 3 && ctMethod.getName().equals("do")) {
                    System.out.println("ps[0].getName==" + ps[0].getName());
                    ctMethod.setBody("{}");
                }
            }
            //这一步就是将破译完的代码放在桌面上
            zzZJJClass.writeFile(Desktop);
        } catch (Exception e) {
            System.out.println("错误==" + e);
        }
    }



    /**破解AsposePdfJar-21.8版本*/
    private static void crackAsposePdfJar(String jarName) {
        try {
            ClassPool.getDefault().insertClassPath(jarName);
            CtClass ctClass = ClassPool.getDefault().getCtClass("com.aspose.pdf.ADocument");
            CtMethod[] declaredMethods = ctClass.getDeclaredMethods();
            int num = 0;
            for (int i = 0; i < declaredMethods.length; i++) {
                if (num == 2) {
                    break;
                }
                CtMethod method = declaredMethods[i];
                CtClass[] ps = method.getParameterTypes();
                if (ps.length == 2
                        && method.getName().equals("lI")
                        && ps[0].getName().equals("com.aspose.pdf.ADocument")
                        && ps[1].getName().equals("int")) {
                    //源码ADocument类的这个方法限制页数：
                    // static boolean lI(ADocument var0, int var1) {
                    //        return !lb() && !lj() && !var0.lt() && var1 > 4;
                    //    }
                    // 最多只能转换4页，处理返回false，无限制页数
                    System.out.println(method.getReturnType());
                    System.out.println(ps[1].getName());
                    method.setBody("{return false;}");
                    num = 1;
                }
                if (ps.length == 0 && method.getName().equals("lt")) {
                    // 水印处理
                    method.setBody("{return true;}");
                    num = 2;
                }
            }
            File file = new File(jarName);
            ctClass.writeFile(file.getParent());
            disposeJar(jarName, file.getParent() + "/com/aspose/pdf/ADocument.class");
        } catch (NotFoundException e) {
            e.printStackTrace();
        } catch (CannotCompileException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static void disposeJar(String jarName, String replaceFile) {
        List<String> deletes = new ArrayList<>();
        deletes.add("META-INF/37E3C32D.SF");
        deletes.add("META-INF/37E3C32D.RSA");
        File oriFile = new File(jarName);
        if (!oriFile.exists()) {
            System.out.println("######Not Find File:" + jarName);
            return;
        }
        //将文件名命名成备份文件
        String bakJarName = jarName.substring(0, jarName.length() - 3) + "cracked.jar";
        //   File bakFile=new File(bakJarName);
        try {
            //创建文件（根据备份文件并删除部分）
            JarFile jarFile = new JarFile(jarName);
            JarOutputStream jos = new JarOutputStream(new FileOutputStream(bakJarName));
            Enumeration entries = jarFile.entries();
            while (entries.hasMoreElements()) {
                JarEntry entry = (JarEntry) entries.nextElement();
                if (!deletes.contains(entry.getName())) {
//                    if (entry.getName().equals(replaceFile1)) {
                    if (entry.getName().equals("com/aspose/pdf/ADocument.class")) {
                        System.out.println("Replace:-------" + entry.getName());
                        JarEntry jarEntry = new JarEntry(entry.getName());
                        jos.putNextEntry(jarEntry);
                        FileInputStream fin = new FileInputStream(replaceFile);
                        byte[] bytes = readStream(fin);
                        jos.write(bytes, 0, bytes.length);
                    } else {
                        jos.putNextEntry(entry);
                        byte[] bytes = readStream(jarFile.getInputStream(entry));
                        jos.write(bytes, 0, bytes.length);
                    }
                } else {
                    System.out.println("Delete:-------" + entry.getName());
                }
            }
            jos.flush();
            jos.close();
            jarFile.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static byte[] readStream(InputStream inStream) throws Exception {
        ByteArrayOutputStream outSteam = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int len = -1;
        while ((len = inStream.read(buffer)) != -1) {
            outSteam.write(buffer, 0, len);
        }
        outSteam.close();
        inStream.close();
        return outSteam.toByteArray();
    }
}
