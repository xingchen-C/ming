package com.zm.ming.utils.microsoft;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.ZipUtil;
import com.aspose.cells.*;
import com.zm.ming.domain.constant.MicrosoftConstants;
import com.zm.ming.domain.constant.WebsocketConstants;
import com.zm.ming.domain.result.WebsocketResult;
import com.zm.ming.handler.websocket.WebSocketImpl;
import com.zm.ming.utils.FileUtils;
import com.zm.ming.utils.UUID;
import com.zm.ming.utils.WebSocketUtil;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import javax.imageio.ImageIO;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;

import static com.zm.ming.utils.microsoft.AsposeUtils.loadLicense;

/**
 * @author lzm
 *  ppt转文档工具类
 *  未完全破解，转换前需加载license
 * */
@Component
@Slf4j
public class ExcelToOther {
    public static void main(String[] args) {

    }
    private static ExcelToOther instance;
    @PostConstruct
    public void init() {
        instance = this;
    }
    private static WebSocketImpl webSocket;
    /**
     *使用构造器注入websocket
     * */
    public ExcelToOther(WebSocketImpl webSocket){
        ExcelToOther.webSocket=webSocket;
    }
    /**转换完成后产生多个文件需要进行打包的有：\html\图片*/
    public static final List<String> isZipType=new ArrayList<>(5);
    public static final List<String> isImageType=new ArrayList<>(3);
    /**当前支持转的类型*/
    public static final List<String> allowedConvertType=new ArrayList<>(21);
    static {//注释调的这些是没有saveoptions的，暂时不用
//        allowedConvertType.add(MicrosoftConstants.Csv);//todo 待解决：内容乱码
        allowedConvertType.add(MicrosoftConstants.Tsv);
        allowedConvertType.add(MicrosoftConstants.Xlsx);
//        allowedConvertType.add(MicrosoftConstants.Xlsm);
//        allowedConvertType.add(MicrosoftConstants.Xltx);
        allowedConvertType.add(MicrosoftConstants.Json);
        allowedConvertType.add(MicrosoftConstants.Sql_Script);
        allowedConvertType.add(MicrosoftConstants.Ods);
        allowedConvertType.add(MicrosoftConstants.Xls);
        allowedConvertType.add(MicrosoftConstants.Xlsb);
        allowedConvertType.add(MicrosoftConstants.Pdf);
        allowedConvertType.add(MicrosoftConstants.Xps);
        allowedConvertType.add(MicrosoftConstants.Dif);
        allowedConvertType.add(MicrosoftConstants.Fods);
        allowedConvertType.add(MicrosoftConstants.Sxc);
        allowedConvertType.add(MicrosoftConstants.Pptx);//todo 待解决：列太长转换后会折页
        allowedConvertType.add(MicrosoftConstants.Docx);
        allowedConvertType.add(MicrosoftConstants.MarkDown);//todo 一个sheet只能转一个md

        //如果工具类新增加转换的类型，则在这里添加对应类型...
        isZipType.add(MicrosoftConstants.Mhtml);
        isZipType.add(MicrosoftConstants.Html);

        isImageType.add(MicrosoftConstants.Emf);
        isImageType.add(MicrosoftConstants.Wmf);


        allowedConvertType.add(MicrosoftConstants.Svg);//todo 打开内容不正确
        allowedConvertType.add(MicrosoftConstants.Svm);
//        isImageType.add(MicrosoftConstants.Jpeg);
        isImageType.add(MicrosoftConstants.Png);
        isImageType.add(MicrosoftConstants.Bmp);
        isImageType.add(MicrosoftConstants.Gif);
//        isImageType.add(MicrosoftConstants.Tiff);//todo 报错 Please add Java Advanced Imaging (JAI) references to generate TIFF image for Java 8 and older!
        isZipType.addAll(isImageType);
        //合并
        allowedConvertType.addAll(isZipType);
    }
    /**当前支持上传文件的类型*/
    public static final List<String> allowedSourceType=new ArrayList<>(6);
    static {
        allowedSourceType.add(MicrosoftConstants.Xls);
        allowedSourceType.add(MicrosoftConstants.Xlsx);
        allowedSourceType.add(MicrosoftConstants.Xlsb);
        allowedSourceType.add(MicrosoftConstants.Xltx);
        allowedSourceType.add(MicrosoftConstants.Xltm);
        allowedSourceType.add(MicrosoftConstants.Xlsm);
    }
    /**
     * 根据convertType类型将excelFile转其它文档的字节码
     * 此方法用于非保存本地
     * @param excelFile:    excel文件MultipartFile格式
     * @param convertType: 需转成的文件类型
     * @return 生成的byte[]
     */
    @SneakyThrows(Exception.class)
    public static byte[]convertTypeBytes(MultipartFile excelFile,String convertType,String wsNoticeId) {
        //校验合法类型
        AsposeUtils.checkType(excelFile,allowedSourceType,convertType,allowedConvertType);
        //加载配置，否则有水印和页数限制
        loadLicense(MicrosoftConstants.EXCEL_TO_OTHER);
        //linux下加载window字体
        AsposeUtils.loadLinuxFonts(MicrosoftConstants.EXCEL_TO_OTHER);
        //构建文档对象
        Workbook workbook = new Workbook(excelFile.getInputStream());
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        byte[] resultByte = null;
        String tempDir=MicrosoftConstants.TO_BYTE_TEMP_PATH+ UUID.fastUUID().toString(true)+File.separator;
        String fileName=FileUtils.getFilePrefix(excelFile);
        FileUtil.mkdir(tempDir);
        if (isImageType.contains(convertType)){
            //创建临时文件夹->打成压缩包->转成bytes->删除压缩包
            int sheetNums = workbook.getWorksheets().getCount();
            for (int i = 0; i < sheetNums; i++) {
                Worksheet worksheet = workbook.getWorksheets().get(i);
                SheetRender sheetRender = new SheetRender(worksheet, customImageSaveOptions(convertType,wsNoticeId).getImageOrPrintOptions());
                for (int i1 = 0; i1 < sheetRender.getPageCount(); i1++) {
                    sheetRender.toImage(i1,String.format(tempDir+fileName+"_%d_%d."+convertType,i,i1));
                }
            }
        }else {
            //根据转换类型设置通用的保存选项
            SaveOptions saveOptions = customSaveOptions(convertType,wsNoticeId);
            //注意一点，不管是任何类型excel、words、ppt、pdf都好，对于使用转换保存到本地的方式会产生多个文件时，用来保存为流时是不对的，
            // 要么文件打不开要么报错，所以对于这种情况都要使用保存到本地的方式然后打成压缩包再转成流
            if (!isImageType.contains(convertType)&&isZipType.contains(convertType)){
                String filePath=tempDir+fileName+ "."+convertType;
                if (saveOptions!=null){
                    workbook.save(filePath,saveOptions);
                }else {
                    workbook.save(filePath,getSaveFormatByType(convertType));
                }
            }else {
                if (saveOptions!=null){
                    workbook.save(outputStream,saveOptions);
                }else {
                    workbook.save(outputStream,getSaveFormatByType(convertType));
                }
                resultByte=outputStream.toByteArray();
            }
        }
        // 返回生成文件的字节码
        if (isZipType.contains(convertType)){
            //打成压缩包
            File zip = ZipUtil.zip(tempDir);
            resultByte=FileUtil.readBytes(zip);
            //然后把原本的目录和压缩包文件删掉
            FileUtil.del(zip);
        }
        //删掉设置产生的临时空目录
        FileUtil.del(tempDir);
        WebSocketUtil.sendConvertSuc(webSocket,wsNoticeId);
        // 返回生成的文件字节码
        return resultByte;
    }

    /**
     * 根据convertType类型将excelFile转其它文档
     * 此方法用于保存本地
     * @param excelFile:    excel文件MultipartFile格式
     * @param systemPath 生成的文档所在的根目录，还需要拼接其它目录,savePath是最终文档的绝对路径
     * @param convertType 要转换成的文档类型
     * @return 生成的文件路径
     */
    @SneakyThrows(Exception.class)
    public static String convertType(MultipartFile excelFile, String systemPath,String convertType,String wsNoticeId) {
        //校验合法类型
        AsposeUtils.checkType(excelFile,allowedSourceType,convertType,allowedConvertType);
        //加载配置，否则有水印和页数限制
        loadLicense(MicrosoftConstants.EXCEL_TO_OTHER);
        //构建文档对象
        Workbook workbook = new Workbook(excelFile.getInputStream());
        String fileName = FileUtils.getFilePrefix(excelFile);
        String namePath=FileUtils.dateFilePath(null);
        String zipDir=systemPath+convertType+namePath;
        String savePath=zipDir+fileName;
        //由于excel和ppt转换的保存需要savePath路径下的文件是存在的，所以这里创建一个
        FileUtil.mkdir(zipDir);
        //判断转换的类型
        //todo 转svg、tiff、bmp、gif还不行；转md只有一页、转docx和pptx会折页
        //1、转图片的处理方式：
        if (isImageType.contains(convertType)){
            int sheetNums = workbook.getWorksheets().getCount();
            for (int i = 0; i < sheetNums; i++) {
                Worksheet worksheet = workbook.getWorksheets().get(i);
                SheetRender sheetRender = new SheetRender(worksheet, customImageSaveOptions(convertType,wsNoticeId).getImageOrPrintOptions());
                for (int i1 = 0; i1 < sheetRender.getPageCount(); i1++) {
                    sheetRender.toImage(i1,String.format(savePath+"_%d_%d."+convertType,i,i1));
                }
            }
        }else {
            //转其它的处理方式：
            //根据转换类型设置通用的保存选项
            SaveOptions saveOptions = customSaveOptions(convertType,wsNoticeId);
            savePath+="."+convertType;
            workbook.save(savePath, saveOptions);
        }
        if (isZipType.contains(convertType)){
            File zip = ZipUtil.zip(zipDir);
            savePath=zip.getPath().replaceAll("\\\\", Matcher.quoteReplacement(File.separator));
            //然后把原本的目录删掉
            FileUtil.del(zipDir);
        }
        WebSocketUtil.sendConvertSuc(webSocket,wsNoticeId);
        return savePath.split(systemPath)[1];
    }
    /**将文件后缀转为com.aspose.cells.SaveFormat对应的类型*/
    private static int getSaveFormatByType(String convertType){
        int i;
        switch (convertType){
            case MicrosoftConstants.Csv: i = 1;break;
            case MicrosoftConstants.Tsv: i = 11;break;

            case MicrosoftConstants.Xls: i = 5;break;
            case MicrosoftConstants.Xlsx: i = 6;break;
            case MicrosoftConstants.Xlsm: i = 7;break;
            case MicrosoftConstants.Xltx: i = 8;break;
            case MicrosoftConstants.Xltm: i = 9;break;
            case MicrosoftConstants.Xlam: i = 10;break;
            case MicrosoftConstants.Xlsb: i = 16;break;

            case MicrosoftConstants.Pdf: i = 13;break;

            case MicrosoftConstants.Html: i = 12;break;
            case MicrosoftConstants.Mhtml: i = 17;break;
            //其它
            case MicrosoftConstants.Ods: i = 14;break;
            case MicrosoftConstants.Xps: i = 20;break;

            case MicrosoftConstants.Emf: i = 258;break;
            case MicrosoftConstants.Tiff: i = 21;break;
            case MicrosoftConstants.Bmp: i = 263;break;
            case MicrosoftConstants.Gif: i = 322;break;

            case MicrosoftConstants.Svg: i = 28;break;
            case MicrosoftConstants.Dif: i = 30;break;
            case MicrosoftConstants.Ots: i = 31;break;
            case MicrosoftConstants.Xml: i = 51;break;

            case MicrosoftConstants.Json: i = 322;break;
            case MicrosoftConstants.Sql_Script: i = 514;break;
            case MicrosoftConstants.MarkDown: i = 57;break;
            case MicrosoftConstants.Fods: i = 59;break;
            case MicrosoftConstants.Sxc: i = 60;break;
            case MicrosoftConstants.Pptx: i = 61;break;
            case MicrosoftConstants.Docx: i = 62;break;
            default:i= SaveFormat.UNKNOWN;
        }
        return i;
    }
    /**将文件后缀转为com.aspose.cells.SaveFormat对应的类型*/
    private static int getImageTypeByType(String convertType){
        int i;
        switch (convertType){
            case MicrosoftConstants.Emf: i = 2;break;
            case MicrosoftConstants.Wmf: i = 3;break;
//            case MicrosoftConstants.Jpeg: i = 5;break;
            case MicrosoftConstants.Png: i = 6;break;
            case MicrosoftConstants.Bmp: i = 7;break;
            case MicrosoftConstants.Gif: i = 66;break;
            case MicrosoftConstants.Tiff: i = 67;break;
            case MicrosoftConstants.Svg: i = 68;break;
            case MicrosoftConstants.Svm: i = 69;break;
            default:i= ImageType.UNKNOWN;
        }
        return i;
    }
    //自定义SaveOptions
    private static SaveOptions customSaveOptions(String convertType,String wsNoticeId){
        SaveOptions saveOptions=null;
        switch (convertType){
            //先设置各自的保存选项参数(如果有)
            case MicrosoftConstants.Dif:saveOptions=customDifSaveOptions();break;
            case MicrosoftConstants.Docx:saveOptions=customDocxSaveOptions();break;
            case MicrosoftConstants.Html:saveOptions=customHtmlSaveOptions(convertType);break;
            case MicrosoftConstants.Mhtml:saveOptions=customHtmlSaveOptions(convertType);break;
            case MicrosoftConstants.Csv:saveOptions=customTxtSaveOptions();break;
            case MicrosoftConstants.Tsv:saveOptions=customTxtSaveOptions();break;
            case MicrosoftConstants.MarkDown:saveOptions=customMdSaveOptions();break;
            case MicrosoftConstants.Xps:saveOptions=customXpsSaveOptions();break;
            case MicrosoftConstants.Ods:saveOptions=customOdsSaveOptions();break;
            case MicrosoftConstants.Xlsb:saveOptions=customXlsbSaveOptions();break;
            case MicrosoftConstants.Xlsx:saveOptions=customOoxmlSaveOptions();break;
            case MicrosoftConstants.Xls:saveOptions=customXlsSaveOptions();break;
            case MicrosoftConstants.Pptx:saveOptions=customPptxSaveOptions();break;
            case MicrosoftConstants.Pdf:saveOptions=customPdfSaveOptions(wsNoticeId);break;
            case MicrosoftConstants.Json:saveOptions=customJsonSaveOptions();break;
            case MicrosoftConstants.Sql_Script:saveOptions=customSqlSaveOptions();break;

            case MicrosoftConstants.Emf:saveOptions=customImageSaveOptions(convertType,wsNoticeId);break;
            case MicrosoftConstants.Wmf:saveOptions=customImageSaveOptions(convertType,wsNoticeId);break;
            case MicrosoftConstants.Jpeg:saveOptions=customImageSaveOptions(convertType,wsNoticeId);break;
            case MicrosoftConstants.Png:saveOptions=customImageSaveOptions(convertType,wsNoticeId);break;
            case MicrosoftConstants.Bmp:saveOptions=customImageSaveOptions(convertType,wsNoticeId);break;
            case MicrosoftConstants.Gif:saveOptions=customImageSaveOptions(convertType,wsNoticeId);break;
            case MicrosoftConstants.Tiff:saveOptions=customImageSaveOptions(convertType,wsNoticeId);break;
            case MicrosoftConstants.Svg:saveOptions=customImageSaveOptions(convertType,wsNoticeId);break;
            case MicrosoftConstants.Svm:saveOptions=customImageSaveOptions(convertType,wsNoticeId);break;

            default:saveOptions=null;
        }
        //再设置通用的保存选项参数
        if (saveOptions!=null){
            currencySaveOptions(saveOptions,convertType);
        }
        return saveOptions;
    }



    /**
     * 设置通用的保存选项参数
     * */
    private static SaveOptions currencySaveOptions(SaveOptions saveOptions, String convertType){
        //缓存的文件夹用于存储一些大型数据。
//        saveOptions.setCachedFileFolder(FileUtil.createTempFile().getPath());
        //指示在保存文件之前是否合并条件格式和验证区域。
        saveOptions.setMergeAreas(true);
        //指示是否更新智能艺术设置。 默认值为 false。
        saveOptions.setUpdateSmartArt(true);
        //
        saveOptions.setValidateMergedAreas(true);
        return saveOptions;
    }
    //==================自定义各自的保存选项参数==================
    /**
     * 自定义html保存参数
     * */
    private static HtmlSaveOptions customHtmlSaveOptions(String convertType){

        HtmlSaveOptions saveOptions=new HtmlSaveOptions(getSaveFormatByType(convertType));
        //指示在无法完全显示数据时是否添加工具提示文本。 默认值为 false。
        saveOptions.setAddTooltipText(true);
        //附件将保存到的目录。 仅用于保存到 html 流。
//        saveOptions.setAttachedFilesDirectory();
        //	指定附加文件（如 html 文件中的图像）的 Url 前缀。 仅用于保存到 html 流。
//        saveOptions.setAttachedFilesUrlPrefix();
        //	指示是否排除未使用的样式。 对于生成的 html 文件，排除未使用的样式可以使文件大小更小 不影响视觉效果。因此，此属性的默认值为 true。
        //	如果用户需要保留工作簿中生成 html 的所有样式（例如该用户的场景 稍后需要从生成的 HTML 恢复工作簿），请将此属性设置为 false。
//        saveOptions.setExcludeUnusedStyles();
        //	指示是否导出虚假的底行数据。默认值为 true。如果要导入 html 或 mht 文件 要出类拔萃，请保留默认值。
//        saveOptions.setExportBogusRowData(false);
        //指示在将文件保存到 html 时是否导出非空白单元格的 Excel 坐标。默认值为 false。 如果要将输出的html导入Excel，请保留默认值。
//        saveOptions.setExportCellCoordinate(true);
        //指示是否导出文档属性。默认值为 true。如果要导入 HTML或MHT文件到Excel，请保留默认值。
//        saveOptions.setExportDocumentProperties(false);
        //指示当文本长度超过最大显示列时是否导出额外的标题。 默认值为 false。如果要将html文件导入Excel，请保留默认值。
//        saveOptions.setExportExtraHeadings(true);
        //	指示是否导出框架脚本和文档属性。默认值为 true。如果要导入 html 或 mht 文件 要出类拔萃，请保留默认值。\
//        saveOptions.setExportFrameScriptsAndProperties(false);
        //	指定图像是以 Base64 格式保存为 HTML、MHTML 还是 EPUB。true时不会有多个文件
        saveOptions.setExportImagesAsBase64(true);
        //指示在将文件保存到 html 时导出注释时，默认值为 false。
        saveOptions.setExportComments(true);
        //	指示 html 或 mht 文件是演示文稿首选项。 默认值为 false。 如果您想获得更漂亮的演示文稿，请将值设置为 true。
        saveOptions.setPresentationPreference(true);
        //	指示在将文件保存到 html 时是否通过工作表缩放级别放大或缩小 html，默认值为 false。
        saveOptions.setWorksheetScalable(true);

        return saveOptions;
    }
    private static PdfSaveOptions customPdfSaveOptions(String wsNoticeId){
        PdfSaveOptions saveOptions=new PdfSaveOptions();
        //设置保存进度
        saveOptions.setPageSavingCallback(customProcess(wsNoticeId));
        //展现转换进度
//        saveOptions.setPageSavingCallback(showProgress());
        //设置生成的 pdf 文档的生产者,版本较低没有这个方法
//        saveOptions.setProducer();
        //当 xls2pdf 结果中需要安全性时，请设置此选项。
//        saveOptions.setSecurityOptions();
        //设置一张纸的所有内容将输出到结果中的一页。 页面设置的纸张大小将无效，页面设置的其他设置 仍将生效。每个sheet一页，页面大小根据数据不定
//        saveOptions.setOnePagePerSheet(true);
        //设置一张工作表的所有列内容将输出到结果中的一页。 页面设置的纸张宽度将被忽略，页面设置的其他设置 仍将生效。每个sheet一页,页面长度固定，宽度根据数据大小不定
        saveOptions.setAllColumnsInOnePagePerSheet(true);
        //设置图像重新采样(每英寸所需的像素（即220高品质/150屏幕质量/96电子邮件质量）,0~100%的JPEG质量)
        saveOptions.setImageResample(96,70);
        //设置要保存的页数，默认是所有
//        saveOptions.setPageCount();

        return saveOptions;
    }
    private static SaveOptions customSqlSaveOptions() {
        SqlScriptSaveOptions saveOptions=new SqlScriptSaveOptions();

        return saveOptions;
    }

    private static SaveOptions customJsonSaveOptions() {
        JsonSaveOptions saveOptions=new JsonSaveOptions();

        return saveOptions;
    }

    private static SaveOptions customXlsbSaveOptions() {
        XlsbSaveOptions saveOptions=new XlsbSaveOptions();

        return saveOptions;
    }
    private static DocxSaveOptions customDocxSaveOptions(){
        DocxSaveOptions saveOptions=new DocxSaveOptions();
        return saveOptions;
    }
    private static ImageSaveOptions customImageSaveOptions(String convertType,String wsNoticeId){
        ImageSaveOptions saveOptions=new ImageSaveOptions();
        ImageOrPrintOptions imageOrPrintOptions = saveOptions.getImageOrPrintOptions();
        //设置生成的图像的水平分辨率（以每英寸点数为单位）。 应用生成图像方法，但 Emf 格式图像除外。默认96
//        imageOrPrintOptions.setHorizontalResolution(300);
        imageOrPrintOptions.setImageType(getImageTypeByType(convertType));
        //		指示单元格的宽度和高度是否按单元格值自动拟合。 默认值为 false
        imageOrPrintOptions.setCellAutoFit(true);
        imageOrPrintOptions.setPageSavingCallback(customProcess(wsNoticeId));
        //true则一张工作表的所有列内容将输出到结果中的一页。 页面设置的纸张宽度将无效，页面设置的其他设置 仍将生效。
        imageOrPrintOptions.setAllColumnsInOnePagePerSheet(true);
//        //	如果 OnePagePerSheet 为 true，则一张纸的所有内容将输出到结果中的一页。 页面设置的纸张大小将无效，页面设置的其他设置 仍将生效。
//        imageOrPrintOptions.setOnePagePerSheet(true);
//        //设置生成的图像的水平分辨率（以每英寸点数为单位）。 应用生成图像方法，但 Emf 格式图像除外。默认96
//        imageOrPrintOptions.setHorizontalResolution(300);
//        //设置生成的图像的垂直分辨率（以每英寸点数为单位）。 应用生成图像方法，但 Emf 格式图像除外。默认96
//        imageOrPrintOptions.setVerticalResolution(300);
        //如果此属性为 true，则生成的 SVG 将适合视图端口。
        imageOrPrintOptions.setSVGFitToViewPort(true);
//        //指示生成的图像的背景是否应为透明。
//        imageOrPrintOptions.setTransparent(true);
        return saveOptions;
    }
    private static MarkdownSaveOptions customMdSaveOptions(){
        MarkdownSaveOptions saveOptions=new MarkdownSaveOptions();

        return saveOptions;
    }
    private static PptxSaveOptions customPptxSaveOptions(){
        PptxSaveOptions saveOptions=new PptxSaveOptions();
        return saveOptions;
    }
    private static DifSaveOptions customDifSaveOptions(){
        DifSaveOptions saveOptions=new DifSaveOptions();
        return saveOptions;
    }
    private static OdsSaveOptions customOdsSaveOptions(){
        OdsSaveOptions saveOptions=new OdsSaveOptions();

        return saveOptions;
    }
    private static TxtSaveOptions customTxtSaveOptions(){
        TxtSaveOptions saveOptions=new TxtSaveOptions();
        //指示是否将所有工作表导出到文本文件。 如果为 false，则仅导出活动表，就像 MS Excel 一样。
        saveOptions.setExportAllSheets(true);
        //	获取和设置将单元格值导出为字符串时的格式策略。 该属性的值是CellValueFormatStrategy整数常量。
//        saveOptions.setFormatStrategy(CellValueFormatStrategy);
        //指示是否始终为每个字段添加“”如果为 true，则将引用所有值; 如果为 false，则值将仅在需要时引用（例如， 当值包含特殊字符（如“”、“\n”或分隔符）时）。 默认值为假。
//        saveOptions.setAlwaysQuoted();
        //指示是否应将单引号导出为一个单元格值的一部分 当Style.QuotePrefix为真时。默认值为假。
//        saveOptions.setExportQuotePrefix();
        return saveOptions;
    }
    private static OoxmlSaveOptions customOoxmlSaveOptions(){
        OoxmlSaveOptions saveOptions=new OoxmlSaveOptions();

        //编写 zip 存档时，即使没有必要，也始终使用 ZIP64 扩展名。
//        saveOptions.setEnableZip64();
        //指示是否将 OleObject 的 Ooxml 文件嵌入为 ole 对象。
        saveOptions.setEmbedOoxmlAsOleObject(true);
        //指示是否在保存文件之前更新比例因子 如果 PageSetup.FitToPagesWide 和 PageSetup.FitToPagesTall 属性控制工作表的缩放方式。
        saveOptions.setUpdateZoom(true);
        //指示是否将单元格名称导出到 Excel2007 .xlsx（.xlsm、.xltx、.xltm）文件。 如果输出文件可由 SQL Server DTS 访问，
        // 则此值必须为 true。 将值设置为 false 将大大提高性能并在创建大文件时减小文件大小。 默认值为 true。
//        saveOptions.setExportCellName(false);
        return saveOptions;
    }
    private static XpsSaveOptions customXpsSaveOptions(){
        XpsSaveOptions saveOptions=new XpsSaveOptions();
        saveOptions.setOnePagePerSheet(true);
        //设置转换的页数
//        saveOptions.setPageCount();
        return saveOptions;
    }
    private static XlsSaveOptions customXlsSaveOptions(){
        XlsSaveOptions saveOptions=new XlsSaveOptions();
        //设置模板
//        saveOptions.setTemplate();
        //	指示是否匹配字体颜色，因为标准调色板中有 56 种颜色。
        saveOptions.setMatchColor(true);
        return saveOptions;
    }
    //保存进度
    private static IPageSavingCallback customProcess(String wsNoticeId){
        WebsocketResult ws = new WebsocketResult();
        ws.setMsgType(WebsocketConstants.CONVERT_PROGRESS);
        return new IPageSavingCallback() {
            @Override
            public void pageStartSaving(PageStartSavingArgs info) {
            }
            @Override
            public void pageEndSaving(PageEndSavingArgs info) {
                ws.setMessage(((double) info.getPageIndex()/info.getPageCount())*100);
                webSocket.sendMessage(wsNoticeId,ws);
            }
        };
    }
}
