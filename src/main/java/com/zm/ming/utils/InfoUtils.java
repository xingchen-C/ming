package com.zm.ming.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author ZM
 * @date 2022/11/3 16:42
 */
public class InfoUtils {
    /**表情*/
    public static final Pattern EMOJI=Pattern.compile("^\\u0000-\\uFFFF");
    /**手机机身码IMEI*/
    public static final Pattern MOBILE_IMEI =Pattern.compile("^\\d{15,17}$");
    /**手机号严谨（2019年工信部公布的最新手机号段）*/
    public static final Pattern MOBILE_STRICT =Pattern.compile("^(?:(?:\\+|00)86)?1(?:(?:3[\\d])|(?:4[5-79])|(?:5[0-35-9])|(?:6[5-7])|(?:7[0-8])|(?:8[\\d])|(?:9[189]))\\d{8}$");
    /**手机号宽松（只要是13、14、15、16、17、18、19开头即可）*/
    public static final Pattern MOBILE_EASY =Pattern.compile("^(?:(?:\\+|00)86)?1[3-9]\\d{9}$");
    /**手机号最宽松（1开头，用来接收短信的手机号优先选这个）*/
    public static final Pattern MOBILE_RELAX =Pattern.compile("^(?:(?:\\+|00)86)?1\\d{10}$");
    /**固定电话*/
    public static final Pattern TELEPHONE=Pattern.compile("^0\\d{2,3}-?\\d{7,8}$");
    /**邮箱*/
    public static final Pattern EMAIL=Pattern.compile("^(\\w+([-.][A-Za-z0-9]+)*){3,18}@\\w+([-.][A-Za-z0-9]+)*\\.\\w+([-.][A-Za-z0-9]+)*$");
    public static boolean isEmail(String email){
       return EMAIL.matcher(email).matches();
    }
    public static boolean isMobileStrict(String mobile){
        return MOBILE_STRICT.matcher(mobile).matches();
    }
    public static boolean isMobileEasy(String mobile){
        return MOBILE_EASY.matcher(mobile).matches();
    }
    public static boolean isMobileRelax(String mobile){
        return MOBILE_RELAX.matcher(mobile).matches();
    }
    public static boolean isMobileImei(String mobileImei){
        return MOBILE_IMEI.matcher(mobileImei).matches();
    }
    public static boolean containsEmoji(String emoji){
        return EMOJI.matcher(emoji).matches();
    }
    public static boolean isTelephone(String tel){
        return TELEPHONE.matcher(tel).matches();
    }
}
