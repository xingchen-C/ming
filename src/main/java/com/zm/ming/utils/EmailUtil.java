package com.zm.ming.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author lzm
 * 邮箱发送工具
 */
@Component
public class EmailUtil{
    private static final Logger log = LoggerFactory.getLogger(EmailUtil.class);
    /**发送者*/
    @Value("${spring.mail.username}")
    private String sender;
    @Resource
    private JavaMailSenderImpl mailSender;
    public static final String CODE_SUBJECT="Mitch在线文档转换";
    public static final String CODE_MSG="您的邮箱有效验证码是：%s,5分钟内有效。如非本人操作，请忽略~";


    public void sendSimpleEmail(SimpleMailMessage email){
        //构建一个普通邮件
        //设置发送者
        email.setFrom(sender);
        //接受方To
        //标题Subject
        //内容Text
        log.info("开始发送邮件=====>，入参："+email.toString());
        mailSender.send(email);
        log.info("=====邮件发送结束====");
    }
}
