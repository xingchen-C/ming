package com.zm.ming.utils;

import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import com.zm.ming.domain.constant.RedisCacheConstant;
import com.zm.ming.domain.result.HttpConstant;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.DigestUtils;
import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.nio.charset.Charset;

/**
 * 权限获取工具类
 *
 * @author tecsun
 */
public class SecurityUtils {
    private static Logger logger = LoggerFactory.getLogger(SecurityUtils.class);
    private static final String IP_UTILS_FLAG = ",";
    private static final String UNKNOWN = "unknown";
    private static final String LOCALHOST_IP = "0:0:0:0:0:0:0:1";
    private static final String LOCALHOST_IP1 = "127.0.0.1";
    /**百度根据ip获取用户城市的url*/
    private static final String BAI_DU_GET_ADDRESSBYIP_URL="http://api.map.baidu.com/location/ip?ip=%s&ak=2VRC7aFcuR7QoyGaqC5Mg65ys1r3iogE";
    /**
     * 获取用户
     */
    public static String getByHeaderName(String headerName) {
        String headerParam = ServletUtils.getRequest().getHeader(headerName);
        return ServletUtils.urlDecode(headerParam);
    }


    /**
     * 根据request获取请求token
     */
    public static String getToken(HttpServletRequest request) {
        return ServletUtils.getRequest().getHeader(HttpConstant.AUTHORIZATION);
    }
    public static String getTouristToken(HttpServletRequest request) {
        return ServletUtils.getRequest().getHeader(HttpConstant.AUTHORIZATION_TOURIST);
    }

    /**随机盐生成器8位*/
    public static String getSalt(int digit){
        return  UUID.fastUUID().toString().substring(0, digit);
    }
    /**根据盐对密码进行md5加密*/
    public static String encryptPwdByMd5AndSalt(String pwd,String salt){
        String encrypt=""+salt.charAt(0)+salt.charAt(3)+pwd+salt.charAt(5);
        return encryptMd5(encrypt);
    }
    /**使用盐进行密码的二次md5加密
    * @param salt 盐
     * @param pwd 密码
     * @return 加密两次后的结果
    * */
    public static String encryptPwdFinal(String pwd,String salt){
        //进行第一次md5加密
        String first = encryptMd5(pwd);
        //返回第二次加密后的结果
        return encryptPwdByMd5AndSalt(first,salt);
    }

    /**
     * 根据salt对字符串进行Md5加密
     *
     * @param source 密码
     * @return 加密字符串
     */
    public static String encryptMd5(String source) {
        return DigestUtils.md5DigestAsHex(source.getBytes());
    }


    /**
     * 获取ip地址
     * 使用Nginx等反向代理软件， 则不能通过request.getRemoteAddr()获取IP地址
     * 如果使用了多级反向代理的话，X-Forwarded-For的值并不止一个，而是一串IP地址，X-Forwarded-For中第一个非unknown的有效IP字符串，则为真实IP地址
     */
    public static String getIpAddress(HttpServletRequest request) {
        String ip = null;
        try {
            //以下两个获取在k8s中，将真实的客户端IP，放到了x-Original-Forwarded-For。而将WAF的回源地址放到了 x-Forwarded-For了。
            ip = request.getHeader("X-Original-Forwarded-For");
            if (StringUtils.isEmpty(ip) || UNKNOWN.equalsIgnoreCase(ip)) {
                ip = request.getHeader("X-Forwarded-For");
            }
            //获取nginx等代理的ip
            if (StringUtils.isEmpty(ip) || UNKNOWN.equalsIgnoreCase(ip)) {
                ip = request.getHeader("x-forwarded-for");
            }
            if (StringUtils.isEmpty(ip) || UNKNOWN.equalsIgnoreCase(ip)) {
                ip = request.getHeader("Proxy-Client-IP");
            }
            if (StringUtils.isEmpty(ip) || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
                ip = request.getHeader("WL-Proxy-Client-IP");
            }
            if (StringUtils.isEmpty(ip) || UNKNOWN.equalsIgnoreCase(ip)) {
                ip = request.getHeader("HTTP_CLIENT_IP");
            }
            if (StringUtils.isEmpty(ip) || UNKNOWN.equalsIgnoreCase(ip)) {
                ip = request.getHeader("HTTP_X_FORWARDED_FOR");
            }
            //兼容k8s集群获取ip
            if (StringUtils.isEmpty(ip) || UNKNOWN.equalsIgnoreCase(ip)) {
                ip = request.getRemoteAddr();
                if (LOCALHOST_IP1.equalsIgnoreCase(ip) || LOCALHOST_IP.equalsIgnoreCase(ip)) {
                    //根据网卡取本机配置的IP
                    InetAddress iNet = null;
                    try {
                        iNet = InetAddress.getLocalHost();
                    } catch (UnknownHostException e) {
                        logger.error("获取客户端Ip地址失败： {}", e);
                    }
                    ip = iNet.getHostAddress();
                }
            }
        } catch (Exception e) {
            logger.error("IPUtils ERROR ", e);
        }
        Assert.isTrue(StringUtils.isNotBlank(ip),"当前网络环境异常，请更换网络再试一下");
        //使用代理，则获取第一个IP地址
        if (ip.indexOf(IP_UTILS_FLAG) > 0) {
            ip = ip.substring(0, ip.indexOf(IP_UTILS_FLAG));
        }
        return ip;
    }
    public static String getAddressByIp(String ip){
        String address=null;
        try {
            if (StringUtils.isNotBlank(ip)) {
                JSONObject jsonObject = readJsonFromUrl(String.format(BAI_DU_GET_ADDRESSBYIP_URL, ip));
                if (HttpConstant.SUCCESS ==(int)jsonObject.get("status")){
                    address=String.valueOf(((JSONObject)(jsonObject.get("content"))).get("address"));
                }
                logger.info("通过ip地址"+ip+"获取用户所在城市结果："+address);
            }
        } catch (IOException e) {
            logger.error("通过ip地址获取用户所在城市失败："+e.getMessage());
        }
        return address;
    }

    /**
     * 读取
     *
     * @param rd
     * @return
     * @throws IOException
     */
    private static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();

    }

    /**
     * 创建链接
     *
     * @param url
     * @return
     * @throws IOException
     * @throws JSONException
     */
    private static JSONObject readJsonFromUrl(String url) throws IOException, JSONException {
        InputStream is = new URL(url).openStream();
        try {
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            String jsonText = readAll(rd);
            JSONObject json = JSONObject.parseObject(jsonText);
            return json;
        } finally {
            is.close();
        }
    }
}
