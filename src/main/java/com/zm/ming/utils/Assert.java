package com.zm.ming.utils;


import com.zm.ming.domain.result.AjaxResult;
import com.zm.ming.exception.CustomException;
import org.springframework.lang.Nullable;

/**
 * @author ZM
 * @date 2022/11/2 16:44
 */

public class Assert extends org.springframework.util.Assert {
    /**
     * 当object不为null时校验通过,否则抛IllegalStateException
     * */
    public static void isTrue(boolean expression, AjaxResult errResult){
        if (!expression){
            throw new CustomException(errResult);
        }
    }
    public static void notNull(boolean expression, AjaxResult errResult){
        if (!expression){
            throw new CustomException(errResult);
        }
    }
    public static void notNull(@Nullable Object object, AjaxResult errResult) {
        if (object == null) {
            throw new CustomException(errResult);
        }
    }

}
