package com.zm.ming.utils;

import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.FastByteArrayOutputStream;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author ZM
 * @date 2022/11/8 14:39
 */
public class PictureUtils {
    private static Random random = new Random();
    private static int width = 120;
    private static int height = 50;
    /**
    * 图片拼接
     * @param paths 待拼接的图片路径集合
     * @param isHorizontal true横向拼接，false纵向拼接
     * @param targetFile 拼接后的文件路径
    * */
    @SneakyThrows
    public static String stitching(ArrayList<String> paths, boolean isHorizontal, String targetFile){
        // 图片
        ArrayList<BufferedImage> pics = new ArrayList<>();
        for (String path : paths) {
            pics.add(ImageIO.read(new File(path)));
        }

        // 计算宽高
        int newWidth = isHorizontal ? pics.stream().mapToInt(BufferedImage::getWidth).sum() : pics.get(0).getWidth();
        int newHeight = isHorizontal ? pics.get(0).getHeight() : pics.stream().mapToInt(BufferedImage::getHeight).sum();

        // 图片处理
        BufferedImage imageNew = new BufferedImage(newWidth, newHeight, BufferedImage.TYPE_INT_RGB);
        AtomicInteger pixelPointsWidth = new AtomicInteger();
        AtomicInteger pixelPointsHeight = new AtomicInteger();

        pics.forEach(v -> {
            int width = v.getWidth();
            int height = v.getHeight();

            int[] image = new int[width * height];
            image = v.getRGB(0, 0, width, height, image, 0, width);

            if (isHorizontal) {
                imageNew.setRGB(pixelPointsWidth.get(), 0, width, newHeight, image, 0, width);
                pixelPointsWidth.addAndGet(width);
            } else {
                imageNew.setRGB(0, pixelPointsHeight.get(), newWidth, height, image, 0, newWidth);
                pixelPointsHeight.addAndGet(height);
            }
        });
        // 图片生成
        ImageIO.write(imageNew, "jpg", new File(targetFile));
        return targetFile;
    }

    public static String createGraphicByChar(String code) throws IOException {
        Assert.isTrue(StringUtils.isNotBlank(code),"无法生成空的图形随机码");
        //在内存中创建图片
        BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        //在图片上画一个矩形当背景
        Graphics g = img.getGraphics();
        g.setColor(setColor(150, 230));
        g.fillRect(0, 0, width, height);

        //生成的字符串
        for (int i = 0; i <  code.length(); i++) {
            g.setColor(setColor(30, 140));
            g.setFont(new Font("黑体", Font.PLAIN, 40));
            g.drawString(String.valueOf(code.charAt(i)), 10 + i * 30, randomNum(height - 30, height));
        }
        //画随机线
        for (int i = 0; i < 25; i++) {
            g.setColor(setColor(50, 180));
            g.drawLine(randomNum(0, width), randomNum(0, height), randomNum(0, width), randomNum(0, height));
        }
        // 转换流信息写出
        FastByteArrayOutputStream os = new FastByteArrayOutputStream();
        ImageIO.write(img, "JPEG", os);
        //最后把流转成base64字符串
        return Base64.encode(os.toByteArray());
    }

    /**获取随机数*/
    private static int randomNum(int min, int max) {
        int num = 0;
        num = random.nextInt(max - min) + min;
        return num;
    }
    /**设置颜色*/
    private static Color setColor(int min, int max) {
        int r = min + random.nextInt(max - min);
        int g = min + random.nextInt(max - min);
        int b = min + random.nextInt(max - min);
        return new Color(r, g, b);
    }

}
