package com.zm.ming.utils;


import cn.hutool.core.io.FileUtil;
import com.aspose.cad.internal.p.S;
import com.zm.ming.domain.result.AjaxResult;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.http.entity.ContentType;
import org.apache.tomcat.util.http.fileupload.impl.FileSizeLimitExceededException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.Enumeration;
import java.util.zip.ZipFile;


/**
 * 文件上传工具类
 *
 * @author lzm
 */
public class FileUtils extends FileUtil {
    public static final Long BYTE=1L;
    public static final Long KB=BYTE*1024;
    public static final Long MB=KB*1024;
    public static final Long GB=MB*1024;


    /**
     * byte转mb
     * */
    public static Long b2mL(Long byteSize){
        return byteSize/MB;
    }
    public static String b2mS(Long byteSize){
        return String.valueOf(b2mL(byteSize))+"mb";
    }
    public static Long m2bL(Long mbSize){
        return mbSize*MB;
    }
    public static String m2bS(Long mbSize){
        return String.valueOf(m2bL(mbSize))+"byte";
    }

    /**===========特定文件类型集合===========*/
    private static final Logger log = LoggerFactory.getLogger(FileUtils.class);
    public static String multipartFileToFile(MultipartFile multipartFile,String tempDir) {
//        选择用缓冲区来实现这个转换即使用java 创建的临时文件 使用 MultipartFile.transferto()方法 。
        File file = null;
        String tempFileName=null;
        try {
            String suffix = multipartFile.getOriginalFilename().substring(multipartFile.getOriginalFilename().lastIndexOf("."));
            tempFileName=UUID.fastUUID().toString(true)+suffix;
            file=new File(tempDir+tempFileName);
            file.createNewFile();
//            multipartFile.transferTo(file);
            multipartFile.transferTo(Paths.get(file.getAbsolutePath()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        log.info("tempFileName is "+tempFileName);
        return tempFileName;
    }

    public static MultipartFile fileToMultipartFile(File file) {
        Assert.isTrue(file !=null && file.exists(),"input file is not null");
        FileInputStream is=null;
        MultipartFile multipartFile=null;
        try {
            is= new FileInputStream(file);
            multipartFile= new MockMultipartFile(file.getName(), file.getName(),
                    ContentType.APPLICATION_OCTET_STREAM.toString(), is);
        } catch (Exception e) {
            log.error("file to  multipartFile is error",e);
            Assert.isTrue(false,"to multipartFile is error");
        } finally {
            if (is!=null){
                try {
                    is.close();
                } catch (IOException e) {
                    log.error("fileToMultipartFile close file is error");
                    Assert.isTrue(false,"to multipartFile close is error");
                }
            }
        }
        return multipartFile;
    }
    public static MultipartFile fileToMultipartFile(String filePath) {
        File file=new File(filePath);
        Assert.isTrue(file.exists(),"input file is not null");
        FileInputStream is=null;
        MultipartFile multipartFile=null;
        try {
            is= new FileInputStream(file);
            multipartFile= new MockMultipartFile(file.getName(), file.getName(),
                    ContentType.APPLICATION_OCTET_STREAM.toString(), is);
        } catch (Exception e) {
            log.error("file to  multipartFile is error",e);
            Assert.isTrue(false,"to multipartFile is error");
        } finally {
            if (is!=null){
                try {
                    is.close();
                } catch (IOException e) {
                    log.error("fileToMultipartFile close file is error");
                    Assert.isTrue(false,"to multipartFile close is error");
                }
            }
        }
        return multipartFile;
    }
    /**
     * zip文件解压
     *
     * @param inputFile   待解压文件夹/文件
     * @param destDirPath 解压路径
     */
    public static void zipUncompress(String inputFile, String destDirPath) throws Exception {
        File srcFile = new File(inputFile);//获取当前压缩文件
        // 判断源文件是否存在
        if (!srcFile.exists()) {
            throw new Exception(srcFile.getPath() + "所指文件不存在");
        }
        ZipFile zipFile = new ZipFile(srcFile);//创建压缩文件对象
        //开始解压
        Enumeration<?> entries = zipFile.entries();
        while (entries.hasMoreElements()) {
            ZipEntry entry = (ZipEntry) entries.nextElement();
            // 如果是文件夹，就创建个文件夹
            if (entry.isDirectory()) {
                String dirPath = destDirPath + "/" + entry.getName();
                srcFile.mkdirs();
            } else {
                // 如果是文件，就先创建一个文件，然后用io流把内容copy过去
                File targetFile = new File(destDirPath + "/" + entry.getName());
                // 保证这个文件的父文件夹必须要存在
                if (!targetFile.getParentFile().exists()) {
                    targetFile.getParentFile().mkdirs();
                }
                targetFile.createNewFile();
                // 将压缩文件内容写入到这个文件中
                InputStream is = zipFile.getInputStream(entry);
                FileOutputStream fos = new FileOutputStream(targetFile);
                int len;
                byte[] buf = new byte[1024];
                while ((len = is.read(buf)) != -1) {
                    fos.write(buf, 0, len);
                }
                // 关流顺序，先打开的后关闭
                fos.close();
                is.close();
            }
        }
    }


    /**
     * zip文件解压指定
     *
     * @param inputFile   待解压文件夹/文件
     * @param destDirPath 解压路径
     */
    public static AjaxResult zipUnMkdir(String inputFile, String destDirPath) throws Exception {
        File srcFile = new File(inputFile);//获取当前压缩文件
        //创建目录，时间
        Long mkdir = System.currentTimeMillis();
        // 判断源文件是否存在
        if (!srcFile.exists()) {
            throw new Exception(srcFile.getPath() + "所指文件不存在");
        }
        ZipFile zipFile = new ZipFile(srcFile);//创建压缩文件对象
        //开始解压
        Enumeration<?> entries = zipFile.entries();
        while (entries.hasMoreElements()) {
            ZipEntry entry = (ZipEntry) entries.nextElement();
            // 如果是文件夹，就创建个文件夹
            if (entry.isDirectory()) {
                throw new Exception(srcFile.getPath() + "压缩包里面不要有文件夹");
//                String dirPath = destDirPath + "/" + entry.getName();
//                srcFile.mkdirs();
            } else {
                // 如果是文件，就先创建一个文件，然后用io流把内容copy过去
                File targetFile = new File(destDirPath + "/" + mkdir + "/" + entry.getName());
                // 保证这个文件的父文件夹必须要存在
                if (!targetFile.getParentFile().exists()) {
                    targetFile.getParentFile().mkdirs();
                }
                targetFile.createNewFile();
                // 将压缩文件内容写入到这个文件中
                InputStream is = zipFile.getInputStream(entry);
                FileOutputStream fos = new FileOutputStream(targetFile);
                int len;
                byte[] buf = new byte[1024];
                while ((len = is.read(buf)) != -1) {
                    fos.write(buf, 0, len);
                }
                // 关流顺序，先打开的后关闭
                fos.close();
                is.close();
                log.info(String.valueOf(mkdir));
            }
        }
        return AjaxResult.success("上传成功",String.valueOf(mkdir));
    }
    /**
     * 文件上传
     *
     * @param baseDir          相对应用的基目录
     * @param file             上传的文件
     * @param allowedExtension 上传文件类型
     * @return 返回上传成功的文件名
     * @throws IOException                          比如读写文件出错时
     */
    public static  String upload(String baseDir, MultipartFile file, List<String> allowedExtension)
            throws IOException {
        int fileNameLength = file.getOriginalFilename().length();
//        Assert.isTrue(fileNameLength <= FileUtils.DEFAULT_FILE_NAME_LENGTH,"文件名太长！");
        assertAllowed(file, allowedExtension);
        String fileName = extractFilename(file);
        File desc = getAbsoluteFile(baseDir, fileName);
        file.transferTo(desc);
        String pathFileName = getPathFileName(fileName);
        return pathFileName;
    }

    /**
     * 文件上传
     * @param file             上传的文件
     * @param fileUrl           文件路径
     *
     **/
    public static void uploadPath(MultipartFile file, String fileUrl){
        //判断该文件夹是否存在 不存在的话就创建
        //这个地方容易报错无操作权限，这个时候百度去查怎么开放权限就可以了，这里不做详情解答
        File dest = new File(fileUrl);
        if(!dest.getParentFile().exists()) {
            dest.getParentFile().mkdirs();
        }
        try {
            //保存文件
            BufferedOutputStream outputStream = new BufferedOutputStream(Files.newOutputStream(Paths.get(fileUrl)));
            outputStream.write(file.getBytes());
            outputStream.flush();
            outputStream.close();

        } catch (Exception e) {
            e.printStackTrace();
            log.info("文件上传错误: " + e.getMessage());
        }
    }



    /**
     * 编码文件名
     */
    public static  String extractFilename(MultipartFile file) {
        String extension = getFileType(file);
        //包含日期的路径
        return  UUID.fastUUID()+"." + extension;
    }

    private static  File getAbsoluteFile(String uploadDir, String fileName) throws IOException {
        File desc = new File(uploadDir + File.separator + fileName);

        if (!desc.exists()) {
            if (!desc.getParentFile().exists()) {
                desc.getParentFile().mkdirs();
            }
        }
        return desc.isAbsolute() ? desc : desc.getAbsoluteFile();
    }

    private static  String getPathFileName(String fileName) throws IOException {
        String pathFileName = "/" + fileName;
        return pathFileName;
    }

    /**
     * 文件大小校验
     *
     * @param file 上传的文件
     * @throws FileSizeLimitExceededException 如果超出最大大小
     */
    public static  void assertAllowed(MultipartFile file, List<String> allowedExtension){
        long size = file.getSize();
//        Assert.isTrue(DEFAULT_MAX_SIZE == -1||size <= DEFAULT_MAX_SIZE,"文件过大！");
        String fileType = getFileType(file);
        Assert.isTrue(allowedExtension.contains(fileType),"请上传指定类型的文件");
    }
    /**
     * 获取文件前缀
     * <p>
     * 例如: a.txt, 返回: a
     * @param file 文件
     * @return 前缀（不含".")
     */
    public static String getFilePrefix(MultipartFile file)
    {
        if (null == file)
        {
            return StringUtils.EMPTY;
        }
        return getFilePrefix(Objects.requireNonNull(file.getOriginalFilename()));
    }
    /**
     * 获取文件前缀
     * <p>
     * 例如: a.txt, 返回: a
     * @param fileName 文件名
     * @return 前缀（不含".")
     */
    public static String getFilePrefix(String fileName)
    {
        int separatorIndex = fileName.lastIndexOf(".");
        if (separatorIndex < 0)
        {
            return "";
        }
        return fileName.substring(0,separatorIndex).toLowerCase();
    }
    /**
     * 获取文件类型
     * <p>
     * 例如: a.txt, 返回: txt
     *
     * @param file 文件名
     * @return 后缀（不含".")
     */
    public static String getFileType(MultipartFile file)
    {
        if (null == file)
        {
            return StringUtils.EMPTY;
        }
        return getFileType(Objects.requireNonNull(file.getOriginalFilename()));
    }
    /**
     * 获取文件类型
     * <p>
     * 例如: a.txt, 返回: txt
     *
     * @param fileName 文件名
     * @return 后缀（不含".")
     */
    public static String getFileType(String fileName)
    {
        int separatorIndex = fileName.lastIndexOf(".");
        if (separatorIndex < 0)
        {
            return "";
        }
        return fileName.substring(separatorIndex + 1).toLowerCase();
    }

    /**
     * 根据目录下载文件
     * @param response
     * @param path
     * @return 后缀名
     */
    public static void downloadPhoto(String path, HttpServletResponse response){
        InputStream fis=null;
        OutputStream toClient=null;
        try {
            //获取下载的路径
            File file=new File(path);
            //获取文件名
            String filename=file.getName();
            String filename1 = URLEncoder.encode(filename, "utf-8");
            filename1 = new String(filename1.getBytes("utf-8"),"GBK");
            //取得文件的后缀名
            String ext=filename.substring(filename.lastIndexOf(".")+1).toUpperCase();
            //以流的形式下载文件
            fis=new BufferedInputStream(new FileInputStream(file));

            //创建一个和文件一样大小的缓存区
            byte[] buffer=new byte[fis.available()];
            //读取流
            fis.read(buffer);
            //清空首部空白行
            response.reset();
            //设置文件下载后的指定文件名
            response.setCharacterEncoding("UTF-8");
//            response.addHeader("Content-Disposition", "attachment;filename=" + new String(filename.getBytes("gb2312"),"ISO8859-1"));
            response.addHeader("Content-Disposition", "attachment;filename=" + filename1);
            response.addHeader("Content-Length", "" + file.length());
            //response.getOutputStream() 获得字节流，通过该字节流的write(byte[] bytes)可以向response缓冲区中写入字节，再由Tomcat服务器将字节内容组成Http响应返回给浏览器。
            toClient = new BufferedOutputStream(response.getOutputStream());
            response.setContentType("application/octet-stream");
            //将buffer 个字节从指定的 byte 数组写入此输出流。
            toClient.write(buffer);
            //刷新此缓冲的输出流。这迫使所有缓冲的输出字节被写出到底层输出流中。 把缓存区的数据全部写出
            toClient.flush();
        } catch (IOException e) {
            e.printStackTrace();

        } finally {
            try {
                //关闭流
                fis.close();
                //关闭缓冲输出流
                toClient.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }
    /**
     * 日期路径 即年/月/日 如2018/08/08
     */
    public static String datePath(int amount) {
        Date date=null;
        if (amount==0){
            date = new Date();
        }else {
            date=dealNowPath(amount);
        }
        return DateFormatUtils.format(date, "yyyy"+File.separator+"MM"+File.separator+"dd"+File.separator);
    }

    public static Date dealNowPath(int amount) {
        Calendar now = Calendar.getInstance();
        now.setTime(new Date());
        now.set(Calendar.DATE, now.get(Calendar.DATE) + amount);
        return now.getTime();
    }

    /**
     * 日期文件路径 即年/月/日/UUID.suffix 如2018/08/08/1bo41ub21j.suffix
     */
    public static String dateRandomPath(String suffix) {
        String datePath = datePath(0);
        return datePath+ UUID.fastUUID()+"."+suffix;
    }
    /**
     * 日期文件路径 即年/月/日/UUID.文件后缀名 如2018/08/08/1bo41ub21j.jpg
     */
    public static String datePath(MultipartFile file) {
        String datePath = datePath(0);
        return datePath+ UUID.fastUUID()+"."+getFileType(file);
    }
    /**
     * 日期文件路径 即/年/月/日/文件名. 如/2018/08/08/uuid/哈哈.jpg
     * 如果fileName为null，则返回/2018/08/08/uuid/
     */
    public static String dateFilePath(String fileName) {
        String datePath = File.separator+datePath(0)+UUID.fastUUID().toString(true)+File.separator;
        if (fileName!=null){
            datePath=datePath+fileName;
        }
        return datePath;
    }

    /**
     * 返回文件大小
     * */
    public static String getFileSize(MultipartFile file){
        String resultSize=null;
        if (file!=null && !file.isEmpty()){
            long size = file.getSize();
            if (size/1024<1024){
                resultSize=size/1024+"kb";
            }else if (size/1024/1024<1024){
                resultSize=size/1024/1024+"mb";
            }else {
                resultSize=size/1024/1024/1024+"g";
            }
        }
        return resultSize;
    }
    public static boolean isWindowSystem(){
        boolean res=false;
        String sys = System.getProperty("os.name");
        if (StringUtils.isNotBlank(sys) && sys.toLowerCase().startsWith("window")){
            res=true;
        }
        return res;

    }
    public static String lastDirName(String fullDirPath){
        String lastDirName=null;
        if (StringUtils.isNotBlank(fullDirPath)){
            lastDirName=fullDirPath.substring(fullDirPath.lastIndexOf(File.separator)+1);
        }
        return lastDirName;
    }
}