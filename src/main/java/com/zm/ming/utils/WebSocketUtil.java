package com.zm.ming.utils;

import com.zm.ming.domain.constant.WebsocketConstants;
import com.zm.ming.domain.result.WebsocketResult;
import com.zm.ming.handler.websocket.WebSocketInterface;

/**
 * @author Lzm
 * @description TODO
 * @date 2024/3/16 20:08
 */
public class WebSocketUtil {
    public static void sendConvertSuc(WebSocketInterface webSocket,String wsNoticeId){
        if (webSocket != null){
            WebsocketResult ws = new WebsocketResult();
            ws.setMsgType(WebsocketConstants.CONVERT_SUCCESS);
            ws.setMessage("转换成功！请移步 “我的-转换记录” 进行查看并下载文件~");
            ws.setSecond(10L);
            webSocket.sendMessage(wsNoticeId,ws);
        }
    }
    public static void sendConvertErr(WebSocketInterface webSocket, String errMsg, String wsNoticeId){
        if (webSocket != null){
            WebsocketResult ws = new WebsocketResult();
            ws.setMsgType(WebsocketConstants.CONVERT_ERROR);
            ws.setMessage("转换出错："+errMsg+"。若有疑问，请在 “我的-问题反馈” 进行反馈，感谢您的理解！");
            ws.setSecond(10L);
            webSocket.sendMessage(wsNoticeId,ws);
        }
    }
}
