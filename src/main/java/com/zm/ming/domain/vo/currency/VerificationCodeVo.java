package com.zm.ming.domain.vo.currency;

import lombok.Data;

/**
 * @author ZM
 * @date 2022/11/8 14:57
 * 验证码出参
 */
@Data
public class VerificationCodeVo {

    private String uuid;
    /**随机码*/
    private String code;
    /**图形base64*/
    private String graphic;
}
