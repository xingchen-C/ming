package com.zm.ming.domain.vo.microsoft;

import lombok.Data;

/**
 * @author Lzm
 * @description TODO
 * @date 2022/12/3 23:20
 * 单条转换数据统计对象
 */
@Data
public class OneStatistics {
    private Integer todayConvert;
    private Integer totalConvert;
    private String loveConvert;
}
