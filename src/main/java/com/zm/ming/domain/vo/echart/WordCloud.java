package com.zm.ming.domain.vo.echart;

import lombok.Data;

/**
 * @author Lzm
 * @description TODO
 * @date 2024/3/13 22:38
 */
@Data
public class WordCloud {
    private String name;
    private Integer value;

    public void incrementValue(){
        this.value++;
    }
}
