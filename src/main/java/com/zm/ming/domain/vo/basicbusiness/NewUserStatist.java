package com.zm.ming.domain.vo.basicbusiness;

import com.zm.ming.domain.vo.microsoft.OneStatistics;
import lombok.Data;

import java.util.List;

/**
 * @author Lzm
 * @description TODO访问量统计返回对象
 * @date 2022/12/11 10:04
 */
@Data
public class NewUserStatist {
    //人数统计
    private Integer onlineNum;
    private Integer todayNewNum;
    private Integer totalNum;
    //转换全量统计
    private OneStatistics statistics;
    //前7天用户新增量统计-折线图
    private List<Integer> sevenDay;
    private String sevenDayStr;
    //近6个月用户新增量统计-柱状图
    private List<Integer> sixMonth;
    private String sixMonthStr;
}
