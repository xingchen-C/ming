package com.zm.ming.domain.vo.microsoft;

import lombok.Data;

/**
 * @author Lzm
 * @description TODO
 * @date 2022/11/15 20:40
 */
@Data
public class ConvertVo {
    private byte[] convertFileByte;
    private String convertFileUrl;
    private long timeConsuming;
}
