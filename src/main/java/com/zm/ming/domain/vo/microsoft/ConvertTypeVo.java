package com.zm.ming.domain.vo.microsoft;

import com.zm.ming.domain.vo.echart.WordCloud;
import lombok.Data;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Lzm
 * @description TODO
 * @date 2022/11/26 17:33
 */
@Data
public class ConvertTypeVo {
    //所有允许上传的类型
    private Set<String> allUploadType;
    //可上传的文档类型，value是根据所有用户转换的次数来统计的，提供给前端生成词云提示
    //[{"name":"pdf","value":7}...]
    private List<WordCloud>countUploadTypeList;
    //下面为map<上传类型,对应类型的可转换列表>
    private Map<String,Set<String>> convertMap;

}
