package com.zm.ming.domain.vo.login;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * @author ZM
 * @date 2022/11/5 15:17
 * 用户缓存信息
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoginInfo implements Serializable{
    private static final long serialVersionUID = 1L;
    private Long userId;
    //请求者的用户id，登录下是userId，非登录下是connUid
    private String requestUid;
    //用户来源类型：0已登录的用户、1游客、
    private Integer userSourceType;
    private String userName;
    private String userSex;
    private String email;
    private String status;
//    private String openId;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date lastLoginTime;
    private String lastLoginToken;
    private String convertRole;
    private String loginIp;
    //免费转换次数
    private Integer freeConvertNum;
    //购买转换次数
    private Integer spendConvertNum;

}
