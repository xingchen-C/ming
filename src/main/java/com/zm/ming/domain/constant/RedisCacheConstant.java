package com.zm.ming.domain.constant;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ZM
 * @date 2022/11/5 15:19
 * 缓存在redis中的键
 */
public class RedisCacheConstant {
    //=====================微信相关===================
    /**微信access_token缓存key*/
    public static final String WX_ACCESS_TOKEN="we_chat_access_token";
    public static final int WX_ACCESS_TOKEN_EXPIRES_IN=3600;
    /**===========用户信息====================================*/
    /**密码输错锁定key*/
    public static final String PWD_ERR_LOCK ="pwd_err_lock~";
    /**密码允许试错次数key*/
    public static final String PWD_VALID_TRIAL ="pwd_valid_trial~";
    /**密码允许试错次数*/
    public static final Integer PWD_NUM_OF_VALID_TRIAL =5;
    /**密码错误锁定分钟数*/
    public static final Integer PWD_ERR_LOCK_TIME =15;

    public static final String USER_TOKEN ="User_Token_";
    public static final String TOURIST_TOKEN ="Tourist_Token_";
    public static final Long TOKEN_EXPIRES_IN=7* Constants.DAY;
    public static final List<String> TOKEN_TYPE=new ArrayList<>(2);
    static {
        TOKEN_TYPE.add(USER_TOKEN);
        TOKEN_TYPE.add(TOURIST_TOKEN);
    }
    //*_code~uuid:随机码值
    /**===========验证码校验（随机码、邮箱验证码）=================*/
    public static final String GRAPHIC_CODE="graphic_code_";
    public static final String EMAIL_CODE="email_code_";
    public static final String SEND_EMAIL_VALID_TRIAL ="send_email_valid_trial_";
    public static final String SEND_EMAIL_MAX_LOCK="send_email_lock_";
    public static final Integer SEND_EMAIL_MAX=10;
    public static final String MESSAGE_CODE="message_code_";

    /**======无法免费试用文档转换的Ip地址key=====*/
    public static final String ISN_FREE_CONVERT_IP_KEY="isn_convert_";
    /**======问题反馈=====*/
    public static final String ISN_COMMIT_PROBLEM_KEY="isn_problem_commit_";
    /**======转换数据集合=====*/
    public static final String CONVERT_TYPE_KEY="convert_type_k_all";
    public static final String CONVERT_TYPE_VU="convert_type_v_allUploadType";
    public static final String CONVERT_TYPE_VM="convert_type_v_convertMap";
    public static final String CONVERT_TYPE_VW="convert_type_v_convertTypeWordCloud";

    /**======转换缓存key=====*/
    public static final String CONVERT_QUEUE_KEY="convert_queue";
}
