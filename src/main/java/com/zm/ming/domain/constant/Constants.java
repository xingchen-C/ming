package com.zm.ming.domain.constant;

import java.util.HashMap;
import java.util.Map;

/**
 * 通用常量信息
 *
 * @author tecsun
 */
public class Constants
{
    /**
     * UTF-8 字符集
     */
    public static final String UTF8 = "UTF-8";

    /**
     * GBK 字符集
     */
    public static final String GBK = "GBK";

    /**
     * http请求
     */
    public static final String HTTP = "http://";

    /**
     * https请求
     */
    public static final String HTTPS = "https://";

    /**
     * 登录成功
     */
    public static final String LOGIN_SUCCESS = "Success";

    /**
     * 注销
     */
    public static final String LOGOUT = "Logout";

    /**
     * 注册
     */
    public static final String REGISTER = "Register";

    /**
     * 登录失败
     */
    public static final String LOGIN_FAIL = "Error";

    /**
     * 当前记录起始索引
     */
    public static final String PAGE_NUM = "pageNum";

    /**
     * 每页显示记录数
     */
    public static final String PAGE_SIZE = "pageSize";

    /**
     * 排序列
     */
    public static final String ORDER_BY_COLUMN = "orderByColumn";

    /**
     * 排序的方向 "desc" 或者 "asc".
     */
    public static final String IS_ASC = "isAsc";

    /**
     * 验证码 redis key
     */
    public static final String CAPTCHA_CODE_KEY = "captcha_codes:";

    /**
     * 验证码有效期（分钟）
     */
    public static final long CAPTCHA_EXPIRATION = 2;

    /**
     * 验证码类型，math数字加减，char字符随机
     */
    public static final String CAPTCHA_TYPE_MATH = "math";
    public static final String CAPTCHA_TYPE_CHAR = "char";

    /**
     * 令牌有效期（分钟）
     */
    public final static long TOKEN_EXPIRE = 720;

    /**
     * 参数管理 cache key
     */
    public static final String SYS_CONFIG_KEY = "sys_config:";

    /**
     * 字典管理 cache key
     */
    public static final String SYS_DICT_KEY = "sys_dict:";

    /**
     * 资源映射路径 前缀
     */
    public static final String RESOURCE_PREFIX = "/profile";

    /**天、时、分、毫秒转秒*/
    public static final Long MINUTE=60L;
    public static final Long HOURS=60*60L;
    public static final Long DAY=24*HOURS;
    public static final Long MILLISECONDS=1/1000L;

    /**盐长度*/
    public static final Integer SALT_PWD=8;
    public static final Integer SALT_RANDOM_CODE=4;
    public static final Integer SALT_EMAIL_CODE=4;

    //=====================文档转换相关===================
    /**文档转换使用的方式
     * 0 MultipartFile to Byte
     * 1 MultipartFile to Path
     * 2 Path to Path
     * */
    public static final Integer USE_CONVERT_M2B=0;
    public static final Integer USE_CONVERT_M2P=1;
    public static final Integer USE_CONVERT_P2P=2;


}
