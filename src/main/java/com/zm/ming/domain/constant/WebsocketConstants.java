package com.zm.ming.domain.constant;

/**
 * @author Lzm
 * @description TODO
 * @date 2022/12/10 14:15
 */
public class WebsocketConstants {
    /**=============消息类型============*/
    //连接成功
    public static final String CONN_SUC="conn-suc";
    //检查是否在线
    public static final String CONN_CHECK="conn-check";
    public static final String CONN_REPEAT="conn-repeat";
    //转换进度
    public static final String CONVERT_PROGRESS="convert-progress";
    public static final String CONVERT_SUCCESS="convert-success";
    public static final String CONVERT_ERROR="convert-error";
    //用户取消转换
    public static final String CONVERT_CHANNEL="convert-channel";
    //群公告
    public static final String GROUP_ANNOUNCEMENT="group-announcement";
    //个人消息弹框提醒
    public static final String PERSON_MSG="person-message";
}
