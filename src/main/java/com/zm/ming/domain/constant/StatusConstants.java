package com.zm.ming.domain.constant;

/**
 * @author ZM
 * @date 2022/11/8 16:34
 */
public class StatusConstants {
    /**用户来源类型（0用户，1游客）*/
    public static final Integer SOURCE_USER=0;
    public static final Integer SOURCE_TOURIST=1;
    /**用户状态（0正常，1已禁用，2已注销）*/
    public static final String USER_NORMAL="0";
    public static final String USER_DISABLE="1";
    public static final String USER_CANCELLATION="2";
    /**角色标志（普通用户，会员，管理员）*/
    public static final String ROLE_TOURIST="RT";
    public static final String ROLE_ORDINARY="RO";
    public static final String ROLE_VIP="RV";
    public static final String ROLE_SUPER="RS";

    /**转换状态（0初始，1完成，2失败，3过期，4删除失败，5转换失败重试）*/
    public static final Integer CONVERT_INIT=0;
    public static final Integer CONVERT_OK=1;
    public static final Integer CONVERT_FAIL=2;
    public static final Integer CONVERT_EXPIRED=3;
    public static final Integer CONVERT_DFAIL=4;
    public static final Integer CONVERT_RETRY=5;
}
