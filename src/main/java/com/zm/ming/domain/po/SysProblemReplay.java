package com.zm.ming.domain.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * @author Lzm
 * @description TODO问题反馈回复表
 * @date 2022/11/24 20:51
 */
@Data
@TableName("sys_problem_replay")
public class SysProblemReplay {
    private static final long serialVersionUID = 1L;
    /**回复的是哪个问题反馈表中的哪个问题*/
    @TableId(type = IdType.INPUT)
    private Long problemId;
    /**回复的内容*/
    @NotBlank(message = "请输入回复的内容~")
    @Size(min = 1,max = 140,message = "回复的内容在140个字符之间~")
    private String content;
    /**回复者*/
    private String replayerId;
    /**创建时间*/
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;
    /**是否已读*/
    private boolean isRead;
}
