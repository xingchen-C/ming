package com.zm.ming.domain.po.user;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.zm.ming.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author ZM
 * @date 2022/11/5 23:41
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("sys_user")
public class SysUser extends BaseEntity {
    @TableId(type = IdType.AUTO)
    private Long userId;
    private String passWord;
    private String userName;
    private String email;
    private String openId;
    private String userSex;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date lastLoginTime;
    private String lastLoginIp;
    private String lastLoginToken;
    private String status;
    private String salt;
    private String convertRole;
    private String loginIp;
    //免费转换次数
    private Integer freeConvertNum;
    //购买转换次数
    private Integer spendConvertNum;

}
