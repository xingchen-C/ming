package com.zm.ming.domain.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.zm.ming.domain.BaseEntity;
import com.zm.ming.domain.bo.BasePageQuery;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * @author Lzm
 * @description TODO问题反馈
 * @date 2022/11/24 20:30
 */
@Data
@TableName("sys_problem_feedback")
public class SysProblemFeedback {
    private static final long serialVersionUID = 1L;
    /**问题反馈表主键*/
    @TableId(type = IdType.AUTO)
    private Long problemId;
    /**问题描述*/
    @NotBlank(message = "请简要描述您遇到的问题")
    @Size(min = 1,max = 140,message = "文字描述不能超过140个字符")
    private String problemDescribe;
    /**是否匿名（true时则为匿名，无法回复）*/
    private boolean anonymous;
    /**提交问题的用户id/游客uid*/
    private String userId;
    /**创建时间*/
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;
    /**是否已读*/
    private boolean isRead;
}
