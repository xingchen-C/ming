package com.zm.ming.domain.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * @author Lzm
 * @description TODO公告对象
 * @date 2022/12/21 11:07
 */
@Data
@TableName("sys_notice")
public class SysNotice {
    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private Long noticeId;

    @NotBlank(message = "公告标题不能为空")
    @Size(min = 1,max = 100,message = "公告标题过长")
    private String noticeTitle;

    @NotBlank(message = "公告内容不能为空")
    @Size(min = 1,max = 500,message = "公告内容过长")
    private String noticeContent;

    @NotNull(message = "请选择是否启用")
    private boolean enabled;

    /**创建时间*/
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    /**有效截至时间时间*/
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date validEndTime;
}
