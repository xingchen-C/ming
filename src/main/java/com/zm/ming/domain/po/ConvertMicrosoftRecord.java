package com.zm.ming.domain.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * @author Lzm
 * @description TODO文档转换记录对象
 * @date 2022/11/14 11:04
 */
@Data
@TableName("convert_microsoft_record")
public class ConvertMicrosoftRecord {
    @TableId(type = IdType.AUTO)
    private Long convertId;

    /**用户来源类型：0已登录的用户、1游客、*/
    private Integer userSourceType;
    /**转换的用户id，登录时是user_id，未登录则是tourist_id*/
    private String convertUid;
    /**上传的文档名称*/
    private String uploadName;
    /**上传的文档类型*/
    private String uploadType;
    /**上传的文档大小MB*/
    private String uploadSize;
    /**要转换的文档类型*/
    private String convertType;
    /**转换后的路径*/
    private String convertUrl;
    /**创建时间*/
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;
    /**开始转换时间*/
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date convertTime;
    /**转换花费时间，秒*/
    private Double convertSecond;
    /**当前转换状态*/
    private Integer convertStatus;
    /**转换失败原因*/
    private String failReason;
}
