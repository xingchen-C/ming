package com.zm.ming.domain.bo.user;

import lombok.Data;

/**
 * @author ZM
 * @date 2022/11/9 14:38
 */
@Data
public class QueryUserBo {
    private Long userId;
    private String email;
    private String openId;
}
