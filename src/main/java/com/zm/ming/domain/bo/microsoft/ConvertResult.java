package com.zm.ming.domain.bo.microsoft;

import lombok.Data;

/**
 * @author Lzm
 * @description TODO
 * @date 2024/2/25 14:20
 */
@Data
public class ConvertResult {
    private byte[] bytesResult;
    private String pathResult;
    //转换所消耗的时间/秒
    private Double consumingSecond;
    private String wsNoticeId;
}
