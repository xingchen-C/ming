package com.zm.ming.domain.bo.user;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * @author Lzm
 * @description TODO
 * @date 2022/11/22 20:16
 */
@Data
public class PassWordBo {
    private String oldPassWord;
    @NotBlank(message = "密码不能为空")
    /**前端第一次进行MD5加密后密码会变成32位*/
    @Size(min = 32,max = 32,message = "密码非法")
    private String passWord;
    @NotBlank(message = "请查看确认密码是否有填写")
    private String againPwd;
    public boolean isSamePwd(){
        return passWord.equals(againPwd);
    }
    public boolean invalidSamePwd(){
        return passWord.equals(oldPassWord);
    }
}
