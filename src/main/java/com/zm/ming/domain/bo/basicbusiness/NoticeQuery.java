package com.zm.ming.domain.bo.basicbusiness;

import com.zm.ming.domain.bo.BasePageQuery;
import lombok.Data;

/**
 * @author Lzm
 * @description TODO
 * @date 2022/12/21 11:37
 */
@Data
public class NoticeQuery extends BasePageQuery {
    private Long noticeId;
    private boolean enabled;
    private String noticeTitle;
}
