package com.zm.ming.domain.bo.login;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author ZM
 * @date 2022/11/5 23:12
 * 用户登录入参
 */
@Data
public class LoginBo {
    /**登录时可以是微信用户的openid、邮箱*/
    @NotBlank(message = "请输入登录名！")
    private String loginId;

    @NotBlank(message = "请输入密码")
    private String passWord;

}
