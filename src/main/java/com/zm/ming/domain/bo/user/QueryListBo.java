package com.zm.ming.domain.bo.user;

import com.zm.ming.domain.bo.BasePageQuery;
import lombok.Data;

/**
 * @author Lzm
 * @description TODO查询用户列表入参
 * @date 2022/12/11 9:03
 */
@Data
public class QueryListBo extends BasePageQuery {
    //是否在线
    private boolean isOnline;
    //用户状态
    private String status;
    //用户昵称
    private String userName;
    //邮箱
    private String email;
}
