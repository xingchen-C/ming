package com.zm.ming.domain.bo;

import lombok.Data;

import java.util.Date;

@Data
public class BasePageQuery {
    //查询开始时间
    private Date startTime;
    //查询结束时间
    private Date endTime;
    //默认从第一页开始
    private Integer page=1;
    //默认每次显示10条数据
    private Integer limit=10;
}
