package com.zm.ming.domain.bo.user;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author Lzm
 * @description TODO管理用户入参
 * @date 2022/12/11 9:42
 */
@Data
public class ManageBo {
    //操作的用户id
    @NotNull(message = "待修改的用户id不能为空")
    private Long userId;
    //要修改成的状态
    @NotBlank(message = "用户状态不能为空")
    private String status;
    @NotBlank(message = "用户角色不能为空")
    private String convertRole;
    //是否强制下线
    private boolean doUnOnline;
    //如果强制下线为true，则token不能为空
    private String userToken;

}
