package com.zm.ming.domain.bo.user;

import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * @author ZM
 * @date 2022/11/8 16:10
 * 注册用户入参
 */
@Data
public class RegisterBo {

    @NotBlank(message = "请输入邮箱方以获取验证码")
    @Size(min = 6,max = 50,message = "邮箱太长拉我们装不下呢~")
    private String email;

    @NotBlank(message = "请输入邮箱获取的验证码")
    @Size(min = 4,max = 6,message = "验证码格式不正确")
    private String emailCode;

//    @NotBlank(message = "非法用户！")
//    @Size(min =10,max = 50,message = "非法用户！")
//    private String openId;

}
