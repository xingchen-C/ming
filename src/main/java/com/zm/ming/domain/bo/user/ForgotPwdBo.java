package com.zm.ming.domain.bo.user;

import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * @author Lzm
 * @description TODO
 * @date 2022/11/15 12:03
 */
@Data
public class ForgotPwdBo {
    @NotBlank(message = "请输入邮箱方以获取验证码")
    @Max(value = 50,message = "邮箱太长拉我们装不下呢~")
    private String email;
    @NotBlank(message = "密码不能为空")
    /**前端第一次进行MD5加密后密码会变成32位*/
    @Size(min = 32,max = 32,message = "密码非法")
    private String passWord;
    @NotBlank(message = "请查看确认密码是否有填写")
    private String againPwd;

    @NotBlank(message = "请输入邮箱获取的验证码")
    @Size(min = 6,max = 6,message = "验证码格式不正确")
    private String emailCode;
    public boolean isSamePwd(){
        return passWord.equals(againPwd);
    }
}
