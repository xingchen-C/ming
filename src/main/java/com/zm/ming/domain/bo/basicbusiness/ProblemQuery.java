package com.zm.ming.domain.bo.basicbusiness;

import com.zm.ming.domain.bo.BasePageQuery;
import lombok.Data;

/**
 * @author Lzm
 * @description TODO
 * @date 2022/12/5 23:59
 */
@Data
public class ProblemQuery extends BasePageQuery {

    /**问题反馈表主键*/
    private Long problemId;
    /**根据问题反馈内容模糊查询*/
    private String problemDescribe;
    /**提交问题的用户id/游客uid*/
    private String userId;
    /**是否已读*/
    private boolean isRead;
}
