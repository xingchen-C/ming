package com.zm.ming.domain.bo.user;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * @author Lzm
 * @description TODO修改用户信息入参
 * 不支持修改邮箱
 * @date 2022/11/14 19:20
 */
@Data
public class AlterInfoBo {
    @NotBlank(message = "给自己取个好听的名字把~")
    private String userName;

    private String email;

//    @NotBlank(message = "非法用户！")
//    @Size(min =10,max = 50,message = "非法用户！")
//    private String openId;

    @NotBlank(message = "请选择性别")
    @Size(min =1,max = 1,message = "非法性别")
    private String userSex;
    /**头像*/
    private MultipartFile avatarFile;
}
