package com.zm.ming.domain.bo.basicbusiness;

import com.zm.ming.domain.bo.BasePageQuery;
import lombok.Data;

/**
 * @author Lzm
 * @description TODO
 * @date 2022/12/5 23:59
 */
@Data
public class ConvertRecordQuery extends BasePageQuery {

    /**问题反馈表主键*/
    private Long convertId;
    /**转换的用户id，登录时是user_id，未登录则是tourist_id*/
    private String convertUid;
    /**上传的文档名称*/
    private String uploadName;
    /**上传的文档类型*/
    private String uploadType;
    /**要转换的文档类型*/
    private String convertType;
    /**当前转换状态*/
    private Integer convertStatus;
    /**转换失败原因*/
    private String failReason;
}
