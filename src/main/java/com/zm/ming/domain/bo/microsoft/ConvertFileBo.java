package com.zm.ming.domain.bo.microsoft;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Data
public class ConvertFileBo  implements Serializable {
    /**
     * 根据选择转换方式去选择要转换的方式(用来给对应责任链的工具类来选择)
     * 1、Words转换其它  2、pptx转换其它  3、pdf转换其它 4、excel转换其它
    * */
    @NotBlank(message = "上传文件的类型")
    @Size(min = 1,max = 10,message = "上传文件的类型！")
    private String uploadType;

    @NotNull(message = "请上传需要转换的文档")
    private MultipartFile convertFile;

    @NotBlank(message = "请选择要转换的类型")
    @Size(min = 1,max = 7,message = "转换的类型不正确")
    private String convertType;

    //对应转换记录的id
    private Long convertId;
    //未登录时转换者的uid，登录时此字段为空
    private String convertUid;
    //用户id
    private Long userId;
    //使用的转换方式
    private Integer useType;
    //当useType转换保存到本地时，此为要保存到本地的前缀路径
    private String preSavePath;
    //当useType，需要将用户上传的convertFile先保存到本地临时文件，待转换时使用tempLocalFilePath去转换
    //原因是入队列转换时，需要把ConvertFileBo暂存到redis，如果不使用路径的方式，那内存将极具飙升，非常不妥
    private String tempLocalFileName;
    //消费失败重试剩余次数
    private Integer failRetryNum;
}
