package com.zm.ming.domain.bo.basicbusiness;

import com.zm.ming.domain.bo.BasePageQuery;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author Lzm
 * @description TODO
 * @date 2022/12/5 23:59
 */
@Data
public class ProblemReplayQuery extends BasePageQuery {
    /**问题反馈表主键*/
    @NotNull(message = "对不起，我们不知道您要查看的是哪个问题")
    private Long problemId;
}
