package com.zm.ming.domain.result;

import com.alibaba.fastjson.JSON;

/**
 * http状态码
 * 
 * @author lzm
 */
public class HttpConstant
{
    /**====================状态码========================*/
    /**
     * 操作成功
     */
    public static final int SUCCESS = 200;

    /**
     * 对象创建成功
     */
    public static final int CREATED = 201;

    /**
     * 请求已经被接受
     */
    public static final int ACCEPTED = 202;

    /**
     * 操作已经执行成功，但是没有返回数据
     */
    public static final int NO_CONTENT = 204;

    /**
     * 资源已被移除
     */
    public static final int MOVED_PERM = 301;

    /**
     * 重定向
     */
    public static final int SEE_OTHER = 303;

    /**
     * 资源没有被修改
     */
    public static final int NOT_MODIFIED = 304;

    /**
     * 参数列表错误（缺少，格式不匹配）
     */
    public static final int BAD_REQUEST = 400;

    /**
     * 未授权
     */
    public static final int UNAUTHORIZED = 401;

    /**
     * 访问受限，授权过期
     */
    public static final int FORBIDDEN = 403;

    /**
     * 资源，服务未找到
     */
    public static final int NOT_FOUND = 404;

    /**
     * 不允许的http方法
     */
    public static final int BAD_METHOD = 405;

    /**
     * 资源冲突，或者资源被锁
     */
    public static final int CONFLICT = 409;

    /**
     * 不支持的数据，媒体类型
     */
    public static final int UNSUPPORTED_TYPE = 415;

    /**
     * 系统内部错误
     */
    public static final int ERROR = 500;

    /**
     * 接口未实现
     */
    public static final int NOT_IMPLEMENTED = 501;

    /**====================header请求头标识========================*/
    public static final String AUTHORIZATION = "authorization";
    public static final String AUTHORIZATION_TOURIST = "tauthorization";
    public static final String RANDOM_CODE = "random_code";

    /**====================contentType标识========================*/
    /** JSON格式*/
    public static final String APPLICATION_JSON = "application/json";
    /**XHTML格式*/
    public static final String APPLICATION_XHTML_XML = "application/xhtml+xml";
    /**XML数据格式*/
    public static final String APPLICATION_XML = "application/xml";
    /**Atom XML聚合格式*/
    public static final String APPLICATION_ATOM_XML = "application/atom+xml";
    /**pdf格式*/
    public static final String APPLICATION_PDF = "application/pdf";
    /**Word文档格式*/
    public static final String APPLICATION_MSWORD = "application/msword";
    /**二进制流数据（如常见的文件下载）*/
    public static final String APPLICATION_OCTET_STREAM = "application/octet-stream";
    /**<form encType=””>中默认的encType，form表单数据被编码为key/value格式发送到服务器（表单默认的提交数据的格式）*/
    public static final String X_WWW_FORM_URLENCODED = "application/x-www-form-urlencoded";
    /** 需要在表单中进行文件上传时，就需要使用该格式*/
    public static final String MULTIPART_FORM_DATA = "multipart/form-data";
    /** HTML格式*/
    public static final String TEXT_HTML = "text/html";
    /**纯文本格式*/
    public static final String TEXT_PLAIN = "text/plain";
    /** XML格式*/
    public static final String TEXT_XML = "text/xml";
    /**gif图片格式*/
    public static final String IMAGE_GIF = "image/gif";
    /**jpg图片格式*/
    public static final String IMAGE_JPEG = "image/jpeg ";
    /**png图片格式*/
    public static final String IMAGE_PNG = "image/png";


}
