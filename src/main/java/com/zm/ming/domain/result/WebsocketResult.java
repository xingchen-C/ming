package com.zm.ming.domain.result;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author Lzm
 * @description TODO
 * @date 2022/12/10 14:13
 */
@Data
public class WebsocketResult {
    /**消息的类型，见WebsocketConstants*/
    @NotBlank(message = "消息类型不能为空")
    private String msgType;
    /**消息内容*/
    @NotBlank(message ="消息内容不能为空")
    private Object message;
    /**消息有效期，默认一秒*/
    private Long second=5L;
}
