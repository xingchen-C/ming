package com.zm.ming.domain.we_chat;

import lombok.Data;

/**
 * @author Lzm
 * @description TODO获取微信access_token的返回体
 * @date 2022/11/20 14:36
 */
@Data
public class AccessTokenVo {
    private String access_token;
    private Integer expires_in;
    private String errcode;
    private String errmsg;
}
