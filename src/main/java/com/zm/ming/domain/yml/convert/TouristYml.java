package com.zm.ming.domain.yml.convert;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author Lzm
 * @description TODO 新游客每日创建数限制（对于请求ip而言）
 * @date 2024/3/4 22:56
 */
@Component
@ConfigurationProperties(prefix = "mitch.tourist")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TouristYml {
    private Integer newT_hit_day;
    private Integer newT_hit_num;
}
