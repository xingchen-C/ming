package com.zm.ming.domain.yml.convert;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author Lzm
 * @description TODO
 * @date 2024/3/24 23:40
 */
@Component
@ConfigurationProperties(prefix = "redis-stream.convert")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ConvertRedisStreamParams {
    private String convert_stream;
    private String convert_group;
    private String consume_name;
}
