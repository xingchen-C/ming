package com.zm.ming.domain.yml.convert;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author Lzm
 * @description TODO
 * @date 2024/3/4 22:56
 */
@Component
@ConfigurationProperties(prefix = "mitch.convert")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ConvertYml {
    private String save_path;
    private String temp_path;
    private Integer save_date_unit;
    private Integer save_date_num;
    private Boolean must_login;
    private Boolean must_spend;
    private Integer free_num;
    private Map<String,Long> role_size;
    private Integer fail_retry_num;
}
