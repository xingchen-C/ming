package com.zm.ming.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 邮件接受实体类
 * @author lzm
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SimpleEmailEntity{
    /**发送方，没有默认是项目组*/
    private String sender;
    /**邮件接受方，可以多人*/
    private String[]  receivers;
    /**主题*/
    private String theme;
    /**内容*/
    private String content;
}
